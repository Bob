package com.interrupt.callback;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

import com.interrupt.callback.CallbackEvent;

public class TestCallbackBroadcaster extends TestCase {
    
    public TestCallbackBroadcaster() {}
    public TestCallbackBroadcaster(String name) {
	super(name);
    }

    public static Test suite() {
	
	TestSuite suite = new TestSuite();
	//suite.addTest(new com.interrupt.callback.TestCallbackBroadcaster("test001"));
	//suite.addTest(new com.interrupt.callback.TestCallbackBroadcaster("testAddHandler"));
	//suite.addTest(new com.interrupt.callback.TestCallbackBroadcaster("testRemoveHandler"));
	//suite.addTest(new com.interrupt.callback.TestCallbackBroadcaster("testNotifyListeners"));
	
	return suite;
    }

    /*
    public void test001() {

	CallbackBroadcaster cbroadcaster = new BroadcasterImpl();
	assertNotNull("'CallbackBroadcaster' is NULL", cbroadcaster);

    }

    public void testAddHandler() {

	//com.interrupt.bob.DocumentHandler chandler1 = new HandlerImpl();
	//com.interrupt.bob.DocumentHandler chandler2 = new HandlerImpl();
	com.interrupt.bob.DocumentHandler chandler1 = new HandlerImpl();
	com.interrupt.bob.DocumentHandler chandler2 = new HandlerImpl();
	
	CallbackBroadcaster cbroadcaster = new BroadcasterImpl();
	cbroadcaster.addListener(chandler1);
	cbroadcaster.addListener(chandler2);
	
	List listeners = cbroadcaster.allListener();
	assertEquals("Incorrect number of Listeners", 2, listeners.size());

    }

    public void testRemoveHandler() {

	// add 2 handlers
	com.interrupt.bob.DocumentHandler chandler1 = new com.interrupt.bob.DocumentHandler() {
	    public void endHandle(CallbackEvent ce) {}
	    public int getThing() { 
		return 1;
	    }
	    public CallbackEvent getEvent() { return null; }
	};
	com.interrupt.bob.DocumentHandler chandler2 = new com.interrupt.bob.DocumentHandler() { 
	    public void endHandle(CallbackEvent ce) {}
	    public int getThing() { 
		return 2;
	    }
	    public CallbackEvent getEvent() { return null; }
	};
	
	CallbackBroadcaster cbroadcaster = new BroadcasterImpl();
	cbroadcaster.addListener(chandler1);
	cbroadcaster.addListener(chandler2);
	
	List listeners = cbroadcaster.allListener();
	assertEquals("Incorrect number of Listeners", 2, listeners.size());
	
	// add another handler; then remove another one
	com.interrupt.bob.DocumentHandler chandler3 = new com.interrupt.bob.DocumentHandler() {
	    public void endHandle(CallbackEvent ce) {}
	    public int getThing() { 
		return 3;
	    }
	    public CallbackEvent getEvent() { return null; }
	};
	cbroadcaster.addListener(chandler3);
	com.interrupt.bob.DocumentHandler removed = (com.interrupt.bob.DocumentHandler)cbroadcaster.removeListener(chandler1);

	List listeners2 = cbroadcaster.allListener();
	assertEquals("Listener size should be the same", 2, listeners2.size() );
	assertNotNull("Did not get the removed Listener", removed );
	assertSame("Removed listener is different from origianl", chandler1, removed );
	assertEquals("Verifying same", chandler1.getThing(), removed.getThing() );
    }
    
    
    public void testNotifyListeners() {
	
	// create broadcaster & add listeners
	com.interrupt.bob.DocumentHandler chandler1 = new HandlerImpl();
	com.interrupt.bob.DocumentHandler chandler2 = new HandlerImpl();
	//Object chandler1 = new HandlerImpl();
	//Object chandler2 = new HandlerImpl();
	
	CallbackBroadcaster cbroadcaster = new BroadcasterImpl();
	cbroadcaster.addListener(chandler1);
	cbroadcaster.addListener(chandler2);
	
	// notify listeners
	CallbackEvent cevent = new EventImpl();
	cbroadcaster.notifyListeners(cevent);
	
	
	assertNotNull("chandler1 should have recieved an event", chandler1.getEvent());
	assertNotNull("chandler2 should have recieved an event", chandler2.getEvent());
	
	assertSame("chandler1's event is different from original", cevent, chandler1.getEvent() );
	assertSame("chandler2's event is different from original", cevent, chandler2.getEvent() );
    }
    */
}


