
package com.interrupt.callback;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestCallbackEvent extends TestCase {
    
    
    public TestCallbackEvent() {}
    public TestCallbackEvent(String name) {
	super(name);
    }

    public static Test suite() {

	TestSuite suite = new TestSuite();
	//suite.addTest(new com.interrupt.callback.TestCallbackEvent("testMembers"));

	return suite;
    }
    
    /*public void testMessage() {

	Object message1 = new Object();

	CallbackEvent devent = new DefinitionEvent();
	devent.setMessage(message1);
	
	Object rmessage = devent.getMessage();
	
	assertNotNull("message returned was null", rmessage);
	assertSame("message returned is NOT SAME", message1, rmessage);
    }

    public void testMembers() {
	
	CallbackEvent cevent = new EventImpl();
	String ename = cevent.getEventName();
	String hname = cevent.getHandlerName();

	assertNotNull("CallbackEvent is NULL", cevent);
	assertEquals("WRONG generic event name","event", ename);
	assertEquals("WRONG generic handlerName", "onEvent", hname);
	
    }
    */

}


