
package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AllTests extends TestCase {
    
    public static Test suite() {
	
		TestSuite suite = new TestSuite();
		suite.addTest( com.interrupt.bob.TestBob.suite() );
		
		suite.addTest( com.interrupt.bob.TestDocumentProcessor.suite() );
		suite.addTest( com.interrupt.bob.TestBalloon.suite() );
		suite.addTest( com.interrupt.bob.TestFileProcessor.suite() );
		
		suite.addTest( com.interrupt.bob.TestFindFiles.suite() );
		suite.addTest( com.interrupt.bob.TestBob.suite() );
		suite.addTest( com.interrupt.bob.TestQueue.suite() );
		suite.addTest( com.interrupt.bob.TestQueues.suite() );
		suite.addTest( com.interrupt.bob.TestAttributes.suite() );
		//suite.addTest( com.interrupt.bob.TestXMLLoading.suite() );
		suite.addTest( com.interrupt.bob.TestBobSystem.suite() );
		//suite.addTest( com.interrupt.bob.TestXmlProcessor.suite() );
		suite.addTest( com.interrupt.bob.base.TestBobComparator.suite() );
		
		
		/* test generated code
		 */
		suite.addTest( com.interrupt.bob.gen.TestAccessors.suite() );
		suite.addTest( com.interrupt.bob.gen.TestAdder.suite() );
		suite.addTest( com.interrupt.bob.gen.TestAll.suite() );
		suite.addTest( com.interrupt.bob.gen.TestFinder.suite() );
		suite.addTest( com.interrupt.bob.gen.TestLister.suite() );
		suite.addTest( com.interrupt.bob.gen.TestRemover.suite() );
		suite.addTest( com.interrupt.bob.gen.TestRemoveAll.suite() );
		suite.addTest( com.interrupt.bob.gen.TestToXML.suite() );
		suite.addTest( com.interrupt.bob.gen.TestVisitor.suite() );
		
		
		//suite.addTest( com.interrupt.bob.TestDefinitionHandler.suite() );
		suite.addTest( com.interrupt.bob.TestDocumentHandler.suite() );
		suite.addTest( com.interrupt.bob.handler.TestXMLHandler.suite() );
		suite.addTest( com.interrupt.bob.handler.TestDefinitionHandler.suite() );
		suite.addTest( com.interrupt.bob.handler.TestTagMerger.suite() );
		suite.addTest( com.interrupt.callback.TestCallbackBroadcaster.suite() );
		
		
		return suite;
		
    }

}


