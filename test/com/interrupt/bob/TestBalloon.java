package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;


public class TestBalloon extends TestCase {
    
    
    public TestBalloon() {}
    public TestBalloon(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
    }

    public void tearDown() {
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestBalloon("testFlush") );
	
	return suite;
    }


    public void testFlush() {
		
		Balloon balloon = new Balloon();
		
		balloon.setAccessorsBalloon(new StringBuilder("aaa"));
		balloon.setAddersBalloon(new StringBuilder("bbb"));
		balloon.setAttributesBalloon(new StringBuilder("ccc"));
		balloon.setChildrenBalloon(new StringBuilder("ddd"));
		balloon.setClassCloseBalloon(new StringBuilder("eee"));
		balloon.setClassDeclBalloon(new StringBuilder("fff"));
		balloon.setFindersBalloon(new StringBuilder("ggg"));
		balloon.setFindersBalloon(new StringBuilder("ghi"));
		balloon.setPackageBalloon(new StringBuilder("hhh"));
		balloon.setRemoversBalloon(new StringBuilder("iii"));
	
	    balloon.flush();
	
		assertNull("Adders balloon should be NULL", balloon.getAddersBalloon());
		assertNull("Children balloon should be NULL", balloon.getChildrenBalloon());
		assertNull("Package balloon should be NULL", balloon.getPackageBalloon());
    }
    
}


