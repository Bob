
package com.interrupt.bob;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.IAncestor;
import com.interrupt.GAncestor;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;
//import com.interrupt.bob.core.XmlProcessor;
import com.interrupt.bob.core.IXmlProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;

public class TestXMLLoading extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestXMLLoading.class); 
	
	private String axml = "" +
	"<ancestor id='aid1' name='' address=''>" + 
		"<parent id='pid1' name='' address=''>" + 
		"<parent id='pid2' name='' address=''>" + 
		"<parent id='pid3' name='' address=''>" + 
		"<parent id='pid4' name='' address=''>" + 
	"</ancestor>";
	
	
	public TestXMLLoading() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TestXMLLoading(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	protected void setUp() {}
	protected void tearDown() {}
	
	
    public static Test suite() {

    	TestSuite suite = new TestSuite();
    	//suite.addTest( new TestXMLLoading("test001") );
    	
    	// 1. make sure BobSystem is loaded first (need xml definitions)
    	// 2. ensure namespace is correct
    	
    	return suite;
    }
    
    /*public void test001() {
    		
	List flist = new ArrayList();
	flist.add( "system/system.xml" );
	
	BobSystem instance = BobSystem.getInstance();
	instance.buildSystem( flist );
	IBob system = instance.getSystem();

	logger.debug( "============= TestXMLLoading::test001 =============" );
	system.toXML( System.out );
	
	
	assertNotNull( "XmlProcessor system is NULL", system );
	assertEquals( "list is not 'bob'", "bob", system.getTagName() );
	
	// set i) definition.files ii) gen.files
	List defFiles = new ArrayList();
	defFiles.add( "xml/test1.xml" );
	defFiles.add( "xml/test2.xml" );
	defFiles.add( "xml/test3.xml" );
	
	List genFiles = new ArrayList();
	genFiles.add("system/testBase.xml");
	
	
	Map memMap = new HashMap();
	memMap.put( "definition.files", defFiles );
	memMap.put( "gen.files", genFiles );
	memMap.put( "input.file", genFiles );
	
	// setup
	(System.getProperties()).setProperty( Util.GEN, "test/gen" );
	(System.getProperties()).setProperty( Util.BASE, "." );
	IXmlProcessor xmlp = null;
	
	try {
	    List allX = system.allChildren();
	    xmlp = (IXmlProcessor)allX.get( 0 );
	    ((XmlProcessor)xmlp).setMemory( memMap );
	    ((XmlProcessor)xmlp).process( "generate.java" );
	    ((XmlProcessor)xmlp).process( "generate.gen" );
	}
	catch(ProcessorException e) {
	    e.printStackTrace();
	}
	
	Map memory = ((XmlProcessor)xmlp).getMemory();
	IBob result = (IBob)memory.get( "bob.generated" );
	
	logger.debug();
	logger.debug( "============= bob.generated =============" );
	result.toXML( System.out );
	
    }
	*/
	
}



