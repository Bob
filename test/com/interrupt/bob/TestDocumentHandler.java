
package com.interrupt.bob;

import java.io.IOException;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.callback.CallbackEvent;

public class TestDocumentHandler extends TestCase {
    
    
    private XMLReader reader = null;
	
    public TestDocumentHandler() {}
    public TestDocumentHandler(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
	
	System.setProperty("org.xml.sax.driver", "com.bluecast.xml.Piccolo"); 
	try {
	    reader = XMLReaderFactory.createXMLReader();
	}
	catch(SAXException e) {
	    e.printStackTrace(System.out);
	}
    }

    public void tearDown() {
	reader = null;
    }

    public static Test suite() {

	TestSuite suite = new TestSuite();
	suite.addTest(new com.interrupt.bob.TestDocumentHandler("testForListeners"));
	suite.addTest(new com.interrupt.bob.TestDocumentHandler("testBroadcast"));
	suite.addTest(new com.interrupt.bob.TestDocumentHandler("testMethods"));

	return suite;
    }

    public void testForListeners() {
	
	// add a listener
	MockSubscriber subscriber = new MockSubscriber();
	
	DocumentHandler publisher = new DefinitionHandler();
	publisher.addListener(subscriber);
	
	List listenerList = publisher.allListener();
	
	assertNotNull("Listener list is NULL", listenerList);
	assertEquals("1 > WRONG amount of listeners in List", 1, listenerList.size());
	
	//remove a listener
	MockSubscriber subscriber2 = new MockSubscriber();
	publisher.addListener(subscriber2);
	publisher.removeListener(subscriber);

	List listenerList2 = publisher.allListener();
	assertEquals("2 > WRONG amount of listeners in the list", 1, listenerList2.size());
	assertSame("INCORRECT listener was removed", subscriber2, listenerList2.get(0));
    }

    /* test 'CallbackBroadcaster' methods
     */
    public void testBroadcast() {
	
	MockSubscriber subscriber1 = new MockSubscriber();
	MockSubscriber subscriber2 = new MockSubscriber();
	
	DocumentHandler publisher = new DefinitionHandler();
	publisher.addListener(subscriber1);
	publisher.addListener(subscriber2);
	
	List listenerList = publisher.allListener();
	
	// check size of listeners
	assertNotNull("Listener list is NULL", listenerList);
	assertEquals("WRONG amount of listeners in List", 2, listenerList.size());
	
	// publisher recieves 'endDocument' event
	try {
	    publisher.endDocument();
	    publisher.endDocument();
	}
	catch(org.xml.sax.SAXException e) {
	    e.printStackTrace();
	}
	
	// 1. ensure 'endDefinition' method was called on both subscribers
	assertTrue("'endDocument' WAS NOT called for 'subscriber1'", subscriber1.endDocCount > 0);
	assertTrue("'endDocument' WAS NOT called for 'subscriber2'", subscriber2.endDocCount > 0);
	
	// 2. ensure 'endDefinition' method was called twice for each subscriber
	assertEquals("subscriber1 > 'endDocument' should have been called 2 times", 2, subscriber1.endDocCount);
	assertEquals("subscriber2 > 'endDocument' should have been called 2 times", 2, subscriber1.endDocCount);

    }

   
    /* 'test' METHODS
     */
    public void testMethods() {
	
	ConcreteHandler chandler = new ConcreteHandler();
	
	assertNotNull("'ConcreteHandler is NULL", chandler);
	assertNotNull("'XMLReader is NULL", reader);
	try {
	    reader.setProperty("http://xml.org/sax/properties/lexical-handler", chandler); 
	    reader.setProperty("http://xml.org/sax/properties/declaration-handler", chandler); 
	    reader.setContentHandler(chandler);
	    //reader.parse("/Users/timothyw/projects/software/Bob/BOBJava/xml/test3.xml");
	    reader.parse("xml/test3.xml");
	}
	catch(SAXException e) {
	    e.printStackTrace();
	}
	catch(IOException e) {
	    e.printStackTrace();
	}
	
	assertTrue("'startDocument' NOT called", chandler.startDocCalled);
	assertTrue("'endDocCalled' NOT called", chandler.endDocCalled);
	assertTrue("'startElement' NOT called", chandler.startECalled);
	assertTrue("'endElement' NOT called", chandler.endECalled);
    }
    
    
    // mock objects for testing purposes
    public class ConcreteHandler extends DocumentHandler {
	
	public boolean startDocCalled = false;
	public boolean endDocCalled = false;
	public boolean startECalled = false;
	public boolean endECalled = false;
	
	public void startDocument () throws SAXException{
	    startDocCalled = true;
	}

	public void endDocument() throws SAXException{
	    endDocCalled = true;
	}
	
	public void startElement (String namespaceURI, String localName,
	    String qName, Attributes atts) throws SAXException{
	    startECalled = true;
	}

	public void endElement (String namespaceURI, String localName, 
	    String qName) throws SAXException{
	    endECalled = true;
	}
    
    }
    
    
    public class MockSubscriber implements com.interrupt.bob.DocumentHandler {

	private int endDocCount = 0;

	public void endHandle(CallbackEvent ce) {
	    endDocCount++;
	}
	public void setLast(boolean last) {}
    }
    
}


