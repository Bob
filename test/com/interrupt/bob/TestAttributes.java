
package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.bob.Attributes;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.Bob;

public class TestAttributes extends TestCase {
    
    
    public TestAttributes() {}
    public TestAttributes(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestAttributes("testAttributeType") );
	suite.addTest( new TestAttributes("testGenJavaName") );
	suite.addTest( new TestAttributes("testMapping") );
	
	return suite;
    }
    
    public void testAttributeType() {
	
	Attributes interruptAttrs = new Attributes();
	assertTrue("Attributes is NOT an 'org.xml.sax.Attributes'", interruptAttrs instanceof org.xml.sax.Attributes);
    }

    public void testGenJavaName() {
	
	Attributes interruptAttrs = new Attributes();
	String aname = "tim-tim";
	String jvalue = interruptAttrs.genJavaName(aname);

	String ename = "timtim";
	assertEquals("Expected name was false", ename, jvalue);
    }
    
    public void testMapping() {
	
	Attributes interruptAttrs = new Attributes();
	
	String aname = "tim-tim";
	String jvalue = interruptAttrs.genJavaName(aname);
	interruptAttrs.setJNameMapping(jvalue, aname);
	
	String resultAtt = interruptAttrs.getJNameMapping(jvalue);
	assertEquals("INCORRECT returned attribute name", "tim-tim", resultAtt);
    }

}


