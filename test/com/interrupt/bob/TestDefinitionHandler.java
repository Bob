
package com.interrupt.bob;

import java.io.IOException;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.callback.CallbackBroadcaster;

public class TestDefinitionHandler extends TestCase {
    
    
    private XMLReader reader = null;
	
    public TestDefinitionHandler() {}
    public TestDefinitionHandler(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
	
	System.setProperty("org.xml.sax.driver", "com.bluecast.xml.Piccolo"); 
	try {
	    reader = XMLReaderFactory.createXMLReader();
	}
	catch(SAXException e) {
	    e.printStackTrace(System.out);
	}
    }

    public void tearDown() {
	reader = null;
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestDefinitionHandler("testHandlerCallback") );
	//suite.addTest( new TestDefinitionHandler("testBroadcast") );
	
	return suite;
    }

     /* each 'DocumentHandler' should notify any listeners when it has 
     * finished handling an XMLReader callback
     */
    public void testHandlerCallback() {
	
	// test 'CallbackBroadcaster' interface
	DefinitionHandler definitionHandler = new DefinitionHandler();
	assertTrue("DefinitionHandler should be a 'CallbackBroadcaster'", 
		definitionHandler instanceof CallbackBroadcaster);
	
    }
    
    
    
    
    /* test 'sax' methods
     *
    public void testMethods() {
	
	ConcreteHandler chandler = new ConcreteHandler();
	
	assertNotNull("'ConcreteHandler is NULL", chandler);
	assertNotNull("'XMLReader is NULL", reader);
	try {
	    reader.setProperty("http://xml.org/sax/properties/lexical-handler", chandler); 
	    reader.setProperty("http://xml.org/sax/properties/declaration-handler", chandler); 
	    reader.setContentHandler(chandler);
	    reader.parse("/Users/timothyw/projects/software/Bob/BOBJava/xml/test3.xml");
	}
	catch(SAXException e) {
	    e.printStackTrace();
	}
	catch(IOException e) {
	    e.printStackTrace();
	}
	
	assertTrue("'startDocument' NOT called", chandler.startDocCalled);
	assertTrue("'endDocument' NOT called", chandler.endDocCalled);
	assertTrue("'startElement' NOT called", chandler.startECalled);
	assertTrue("'endElement' NOT called", chandler.endECalled);
    }*/
    

}


