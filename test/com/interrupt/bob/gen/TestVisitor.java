
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class TestVisitor extends TestCase {
    
	
	private Logger logger = Logger.getLogger(TestVisitor.class); 
	
	public TestVisitor() {}
    public TestVisitor(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestVisitor("testVisit1") );
	
	return suite;
    }
    
    
    public void testVisit1() {
	
	// leaves
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setId("01");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setId("02");
	
	// middle
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	aone1.setName("one");
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	aone2.setName("two");
	
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	aone3.setName("three");
	aone3.addParent(parent);
	aone3.addParent(parent2);
	
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	aone4.setName("four");
	
	// root
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	
	TVisitor tvisitor = new TVisitor();
	ancestor.accept(tvisitor);

	logger.debug(tvisitor.getResult());
	String expected = "anotherOneanotherOneparentparentanotherOneanotherOneancestor";
	String result = tvisitor.getResult();
	
	assertNotNull("expected result is NULL", expected);
	assertEquals("expected is DIFFERENT from result", expected, result);
    }

    class TVisitor implements IVisitor {
	
	private String result = "";
	public String getResult() {
	    return result;
	}
	
	public void visit(IBob bob) {
	    result += bob.getTagName();
	}
    }
}


