
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

public class TestAll extends TestCase {
    
    public TestAll() {}
    public TestAll(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestAll("testAll1") );
	suite.addTest( new TestAll("testAll2") );
	
	return suite;
    }
    
    
    /* kids in 'ancestor' all defined in 1 place
     */
    public void testAll1() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	com.interrupt.ISomethingElse selse = new com.interrupt.GSomethingElse();
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	ancestor.addParent(parent);
	ancestor.addParent(parent2);
	ancestor.addSomethingElse(selse);
	
	List allKids = ancestor.allChildren();
	List aokids = ancestor.allAnotherOne();
	List pkids = ancestor.allParent();
	List sekids = ancestor.allSomethingElse();
	
	assertEquals("child list should be 7", 7, allKids.size());
	assertEquals("amount of anotherOne SHOULD be 4", 4, aokids.size());
	assertEquals("amount of parent SHOULD be 2", 2, pkids.size());
	assertEquals("amount of somethingElse SHOULD be 1", 1, sekids.size());
    }

    /* 'anotherOne' has kids defined in different locations
     */
    public void testAll2() {
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	com.interrupt.two.IInterrupt interrupt = new com.interrupt.two.GInterrupt();
	
	com.interrupt.IAnotherOne aone = new com.interrupt.GAnotherOne();
	aone.addParent(parent);
	aone.addInterrupt(interrupt);

	List kids = aone.allChildren();
	List pz = aone.allParent();
	List iz = aone.allInterrupt();
	
	assertEquals("WRONG number of kids", 2, kids.size());
	assertEquals("WRONG number of parents", 1, pz.size());
	assertEquals("WRONG number of interrupts", 1, iz.size());
    }
    
}



