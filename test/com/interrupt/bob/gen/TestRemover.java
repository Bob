
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;

public class TestRemover extends TestCase {
    
	
	private Logger logger = Logger.getLogger(TestRemover.class); 
	
	public TestRemover() {}
    public TestRemover(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestRemover("testRemover1") );
	suite.addTest( new TestRemover("testRemover2") );
	
	return suite;
    }
    
    
    /* kids in 'ancestor' all defined in 1 place
     */
    public void testRemover1() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	aone1.setName("one");
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	aone2.setName("two");
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	aone3.setName("three");
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	aone4.setName("four");
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setId("01");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setId("02");
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	ancestor.addParent(parent);
	ancestor.addParent(parent2);
	
	com.interrupt.IAnotherOne removedAone = ancestor.removeAnotherOneByName("three");
	com.interrupt.IParent removedParent = ancestor.removeParentById("01");
	com.interrupt.IAnotherOne resultAone = ancestor.findAnotherOneByName("three");
	com.interrupt.IParent resultParent = ancestor.findParentById("01");
	
	assertNotNull("'removedAone' is NULL", removedAone);
	assertNotNull("'removedParent' is NULL", removedParent);
	assertNull("'anotherOne' SHOULD be NULL", resultAone);
	assertNull("'parent' SHOULD be NULL", resultParent);
    }

    /* 'anotherOne' has kids defined in different locations
     */
    public void testRemover2() {
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setAddress("123 Main St.");
	com.interrupt.IParent parent1 = new com.interrupt.GParent();
	parent1.setAddress("141 Grand River Blvd.");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setAddress("81 Isabella St.");
	
	
	com.interrupt.IAnotherOne aone = new com.interrupt.GAnotherOne();
	aone.addParent(parent);
	aone.addParent(parent1);
	aone.addParent(parent2);
	
	com.interrupt.IParent fresult = aone.findParentByAddress("141 Grand River Blvd.");
	com.interrupt.IParent rresult = aone.removeParentByAddress("141 Grand River Blvd.");
	assertSame("found and removed objects are not the same", fresult, rresult);
	assertEquals("child list should now be 2", 2, (aone.allParent()).size());
	
	com.interrupt.IParent newresult = aone.findParentByAddress("141 Grand River Blvd.");
	//logger.debug("WHYYYYY >> ["+newresult+"] ["+newresult.getAddress()+"]");
	assertNull("parent result SHOULD be null", newresult);
    }
    
}



