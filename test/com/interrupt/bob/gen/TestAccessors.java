
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestAccessors extends TestCase {
    
    public TestAccessors() {}
    public TestAccessors(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestAccessors("testAncestorAccessors") );
	suite.addTest( new TestAccessors("testAnotherOneAccessors") );
	suite.addTest( new TestAccessors("testReset") );
	
	return suite;
    }
    
    
    /* 'ancestor' attributes generated in 1 location; 
     * test your basic attributes
     */
    public void testAncestorAccessors() {
	
	String tid = "testid";
	String tname = "testname";
	String taddress = "testaddress";
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.setId(tid);
	ancestor.setTagName(tname);
	ancestor.setAddress(taddress);

	String rid = ancestor.getId();
	String rname = ancestor.getTagName();
	String raddress = ancestor.getAddress();

	assertNotNull("id is NULL", rid);
	assertNotNull("name is NULL", rname);
	assertNotNull("address is NULL", raddress);

	assertEquals("id has BAD value", tid, rid);
	assertEquals("name has BAD value", tname, rname);
	assertEquals("address has BAD value", taddress, raddress);
    }

    /* 'anotherOne' attributes generated in different locations
     */
    public void testAnotherOneAccessors() {
	
	String tnattr = "testnattr";
	
	com.interrupt.IAnotherOne aone = new com.interrupt.GAnotherOne();
	aone.setNattribute(tnattr);
	
	String rnattr = aone.getNattribute();
	
	assertNotNull("nattribute is NULL", rnattr);
	assertEquals("nattribute has a BAD value", tnattr, rnattr);
    }
    
    public void testReset() {
	
	String tid = "testid";
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.setId("someValue");
	ancestor.setId(tid);
	
	String rid = ancestor.getId();
	
	assertNotNull("id is NULL", rid);
	assertEquals("id has BAD value", tid, rid);
    }
}


