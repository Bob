
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

public class TestLister extends TestCase {
    
    public TestLister() {}
    public TestLister(String name) {
    	super(name);
    }
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
		
		TestSuite suite = new TestSuite();	
		suite.addTest( new TestLister("testLister1") );
		
		return suite;
    }
    
    
    /* kids in 'ancestor' all defined in 1 place
     */
    public void testLister1() {
		
		com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
		aone1.setName("two");
		com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
		aone2.setName("two");
		com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
		aone3.setName("two");
		com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
		aone4.setName("two");
		
		com.interrupt.IParent parent = new com.interrupt.GParent();
		parent.setId("01");
		com.interrupt.IParent parent2 = new com.interrupt.GParent();
		parent2.setId("01");
		
		com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
		ancestor.addAnotherOne(aone1);
		ancestor.addAnotherOne(aone2);
		ancestor.addAnotherOne(aone3);
		ancestor.addAnotherOne(aone4);
		ancestor.addParent(parent);
		ancestor.addParent(parent2);
		
		java.util.List resultAoneList = ancestor.listAnotherOneByName("two");
		java.util.List resultParentList = ancestor.listParentById("01");
		
		assertNotNull("'anotherOne' is NULL", resultAoneList); 
		assertNotNull("'parent' is NULL", resultParentList); 
		
		assertEquals("List size of 'anotherOne' SHOULD be 4", 4, resultAoneList.size() );
		assertEquals("List size of 'anotherOne' SHOULD be 2", 2, resultParentList.size() );
		
    }
    
}



