
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

public class TestAdder extends TestCase {
    
    public TestAdder() {}
    public TestAdder(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestAdder("testAdder1") );
	
	return suite;
    }
    
    
    public void testAdder1() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	com.interrupt.ISomethingElse selse = new com.interrupt.GSomethingElse();
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addParent(parent);
	ancestor.addSomethingElse(selse);
	
	List allKids = ancestor.allChildren();
	List aokids = ancestor.allAnotherOne();
	List pkids = ancestor.allParent();
	List sekids = ancestor.allSomethingElse();
	
	assertEquals("child list should be 4", 4, allKids.size());
	assertEquals("amount of anotherOne SHOULD be 2", 2, aokids.size());
	assertEquals("amount of parent SHOULD be 1", 1, pkids.size());
	assertEquals("amount of somethingElse SHOULD be 1", 1, sekids.size());
    }

}



