
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

public class TestFinder extends TestCase {
    
    public TestFinder() {}
    public TestFinder(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestFinder("testFinder1") );
	suite.addTest( new TestFinder("testFinder2") );
	
	return suite;
    }
    
    
    /* kids in 'ancestor' all defined in 1 place
     */
    public void testFinder1() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	aone1.setName("one");
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	aone2.setName("two");
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	aone3.setName("three");
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	aone4.setName("four");
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setId("01");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setId("02");
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	ancestor.addParent(parent);
	ancestor.addParent(parent2);
	
	com.interrupt.IAnotherOne resultAone = ancestor.findAnotherOneByName("three");
	com.interrupt.IParent resultParent = ancestor.findParentById("01");
	
	assertNotNull("'anotherOne' is NULL", resultAone);
	assertNotNull("'parent' is NULL", resultParent);
	
	assertEquals("'anotherOne' has INCORRECT name value", "three", resultAone.getName());
	assertEquals("amount of anotherOne SHOULD be 4", resultAone, aone3);
    }

    /* 'anotherOne' has kids defined in different locations
     */
    public void testFinder2() {
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setAddress("123 Main St.");
	com.interrupt.IParent parent1 = new com.interrupt.GParent();
	parent.setAddress("141 Grand River Blvd.");
	
	com.interrupt.two.IInterrupt interrupt = new com.interrupt.two.GInterrupt();
	interrupt.setNothing("00-1");
	com.interrupt.two.IInterrupt interrupt1 = new com.interrupt.two.GInterrupt();
	interrupt1.setNothing("00-2");
	
	com.interrupt.IAnotherOne aone = new com.interrupt.GAnotherOne();
	aone.addParent(parent);
	aone.addParent(parent1);
	aone.addInterrupt(interrupt);
	aone.addInterrupt(interrupt1);
	
	com.interrupt.IParent resultp = aone.findParentByAddress("141 Grand River Blvd.");
	com.interrupt.two.IInterrupt resulti = aone.findInterruptByNothing("00-1");
	
	assertNotNull("result parent is NULL", resultp);
	assertNotNull("result interrupt is NULL", resulti);
	
	assertEquals("result parent has bad 'address'", "141 Grand River Blvd.", resultp.getAddress());
	assertEquals("result interrupt has bad 'nothing'", "00-1", resulti.getNothing());
    }
    
}



