
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;

public class TestToXML extends TestCase {
    
	
	private Logger logger = Logger.getLogger(TestToXML.class); 
	
	public TestToXML() {}
    public TestToXML(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
		
    	// TODO - re-implement these tests 
		TestSuite suite = new TestSuite();	
		//suite.addTest( new TestToXML("testToXML") );
		
		return suite;
    }
    
    
    public void testToXML() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	aone1.setName("one");
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	aone2.setName("two");
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	aone3.setName("three");
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	aone4.setName("four");
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setId("01");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setId("02");
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	ancestor.addParent(parent);
	ancestor.addParent(parent2);
	
	String rxml = ancestor.toXML();
	logger.debug(rxml);
	
	String expectedXML = "<ancestor id='' name='' address='' ><anotherOne id='' name='one' nattribute='' /><anotherOne id='' name='two' nattribute='' /><anotherOne id='' name='three' nattribute='' /><anotherOne id='' name='four' nattribute='' /><parent id='01' name='' address='' tim='' sam='' /><parent id='02' name='' address='' tim='' sam='' /></ancestor>";
	
	
	logger.debug( "testToXML source XML: "+ expectedXML ); 
	logger.debug( "testToXML result XML: "+ rxml ); 
	
	
	assertNotNull("XML is NULL", rxml);
	assertEquals("XML is different from expected", expectedXML, rxml);
	
    }
}


