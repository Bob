
package com.interrupt.bob.gen;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

public class TestRemoveAll extends TestCase {
    
    public TestRemoveAll() {}
    public TestRemoveAll(String name) {
	super(name);
    }

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}

    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestRemoveAll("testRemoveAll1") );
	
	return suite;
    }
    
    
    public void testRemoveAll1() {
	
	com.interrupt.IAnotherOne aone1 = new com.interrupt.GAnotherOne();
	aone1.setTagName("one");
	com.interrupt.IAnotherOne aone2 = new com.interrupt.GAnotherOne();
	aone2.setTagName("two");
	com.interrupt.IAnotherOne aone3 = new com.interrupt.GAnotherOne();
	aone3.setTagName("three");
	com.interrupt.IAnotherOne aone4 = new com.interrupt.GAnotherOne();
	aone4.setTagName("four");
	
	com.interrupt.IParent parent = new com.interrupt.GParent();
	parent.setId("01");
	com.interrupt.IParent parent2 = new com.interrupt.GParent();
	parent2.setId("02");
	
	com.interrupt.IAncestor ancestor = new com.interrupt.GAncestor();
	ancestor.addAnotherOne(aone1);
	ancestor.addAnotherOne(aone2);
	ancestor.addAnotherOne(aone3);
	ancestor.addAnotherOne(aone4);
	ancestor.addParent(parent);
	ancestor.addParent(parent2);
	
	assertEquals("children list SHOULD be 6", 6, (ancestor.allChildren()).size());
	
	boolean removed = ancestor.removeAllAnotherOne();
	
	assertEquals("children list SHOULD be 2", 2, (ancestor.allChildren()).size());
	
    }
}


