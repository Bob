package com.interrupt.bob;

import java.util.List;
import java.util.ArrayList;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.FileProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;

public class TestFileProcessor extends TestCase {
    
    
    public TestFileProcessor() {}
    public TestFileProcessor(String test) {
	super(test);
    }

    public void setUp() {}
    public void tearDown() {
	System.setProperty(Util.HOME, "");
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestFileProcessor("testReadFilesDirectory") );
	
	return suite;
    }
    
    public void testReadFilesDirectory() {
	
	// test DIRECTORY of files
	FileProcessor dprocessor = new FileProcessor();
	//System.setProperty(Util.HOME, "/Users/timothyw/projects/software/Bob/BOBJava");
	System.setProperty(Util.HOME, ".");
	ProcessorException pe = null;
	
	List fileList = new ArrayList();
	try {
	    dprocessor.getFileList( fileList, "xml/" );
	}
	catch(ProcessorException e) {
	    pe = e;
	}
	String message = null;
	if(pe != null) {
	    message = pe.getMessage();
	}
	assertNull( "'ProcessorException' should be null; message["+ message +"]", pe );
	assertTrue( "fileList is empty", fileList.size() > 0 );
	
    }

}


