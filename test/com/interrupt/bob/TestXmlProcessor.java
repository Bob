package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.core.IXmlProcessor;
//import com.interrupt.bob.core.XmlProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;


public class TestXmlProcessor extends TestCase {
    
    
	private Logger logger = Logger.getLogger(TestXmlProcessor.class); 
	
	public TestXmlProcessor() {}
    public TestXmlProcessor(String name) {
	super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {
    }

    public void tearDown() {
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestXmlProcessor("testLoad") );
	
	return suite;
    }
    
    
    public void testLoad() {
		
		List flist = new ArrayList();
		flist.add( "system/system.xml" );
		
		BobSystem instance = BobSystem.getInstance();
		instance.buildSystem( flist );
		IBob system = instance.getSystem();
	
		logger.debug( "============= XmlProcessor =============" );
		system.toXML( System.out );
		
		
		assertNotNull( "XmlProcessor system is NULL", system );
		assertEquals( "list is not 'bob'", "bob", system.getTagName() );
		
		// set i) definition.files ii) gen.files
		List defFiles = new ArrayList();
		defFiles.add( "xml/test1.xml" );
		defFiles.add( "xml/test2.xml" );
		defFiles.add( "xml/test3.xml" );
		
		List genFiles = new ArrayList();
		genFiles.add("system/testBase.xml");
		
		Map memMap = new HashMap();
		memMap.put( "definition.files", defFiles );
		memMap.put( "gen.files", genFiles );
		
		// setup
		(System.getProperties()).setProperty( Util.GEN, "test/gen" );
		(System.getProperties()).setProperty( Util.BASE, "." );
		
		/*try {
		    List allX = system.allChildren();
		    IXmlProcessor xmlp = (IXmlProcessor)allX.get( 0 );
		    ((XmlProcessor)xmlp).setMemory( memMap );
		    ((XmlProcessor)xmlp).process( "generate.java" );
		}
		catch(ProcessorException e) {
		    e.printStackTrace();
		}
		*/
		
    }

}


