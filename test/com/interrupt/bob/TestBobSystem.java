package com.interrupt.bob;

import java.util.ArrayList;
import java.util.List;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.ProcessorException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestBobSystem extends TestCase {

	public TestBobSystem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TestBobSystem(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
    public static Test suite() {

	// 1. load many documents
	// 2. create 'Bob' from xml
	// 3. load 'Bob' from xml
    	TestSuite suite = new TestSuite();
    	suite.addTest( new TestBobSystem("testCreatedDefinitions") );
    	return suite;
    }
    
    public void testCreatedDefinitions() {
    	
		// process <xml/> and directory/ recursively
		String sarray[] = {"xml/test1.xml","xml/test2.xml","xml/test3.xml"};
		FindFiles ffiles = new FindFiles();
		//ffiles.setBase("/Users/timothyw/projects/software/Bob/BOBJava");
		ffiles.setBase(".");
		ffiles.setSuffix(".xml");
		List plist = ffiles.find(sarray);
		
		BobSystem bsystem = BobSystem.getInstance();
		bsystem.buildSystem( plist );
		
		List definitions = bsystem.getDefinitions();
		IBob system = bsystem.getSystem();
		
		assertNotNull( "definitions is NULL", definitions );
		assertEquals( "there SHOULD be 10 definitions", 10, definitions.size() );
		
		assertNotNull( "bob system is NULL", system );
		assertEquals( "IBob does not have the 'bob' tagname", "bob", system.getTagName() );
		
    }

}


