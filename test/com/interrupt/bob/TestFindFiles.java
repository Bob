package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.util.Util;

public class TestFindFiles extends TestCase {


	private Logger logger = Logger.getLogger(TestFindFiles.class); 
	
	public TestFindFiles() {}
    public TestFindFiles(String name) {
	super(name);
    }


    /* SETUP / TEARDOWN
     */
    public void setUp() {
	//System.setProperty(Util.BASE,"/Users/timothyw/projects/software/Bob/BOBJava/test/xml");
	System.setProperty(Util.BASE,"test/xml");
	System.setProperty(Util.END,".xml");
    }

    public void tearDown() {
	System.setProperty(Util.BASE,"");
	System.setProperty(Util.END,"");
    }

    public static Test suite() {
    	
		TestSuite suite = new TestSuite();
		suite.addTest( new TestFindFiles("testFindArgs") );
		suite.addTest( new TestFindFiles("testFindSuffix") );
		suite.addTest( new TestFindFiles("testFindFile") );
		suite.addTest( new TestFindFiles("testFindDir") );
		suite.addTest( new TestFindFiles("testEmptyList") );
		suite.addTest( new TestFindFiles("testBaseField") );
		suite.addTest( new TestFindFiles("testFileCheck") );
		
		return suite;
		
    }


    public void testFindArgs() {

	//String[] args = {"thing.xml","test1.xml","test2.xml","/Users/timothyw/projects/software/Bob/BOBJava/test/xml/test3.xml"};
	String[] args = {"thing.xml","test1.xml","test2.xml","test/xml/test3.xml"};

	FindFiles ff = new FindFiles();
    //ff.setBase("/Users/timothyw/projects/software/Bob/BOBJava/test/xml");
    ff.setBase("test/xml");
	List foundFiles = ff.find(args);
	
	assertEquals("there SHOULD be 4 files in this list", 4, foundFiles.size());
	
    }
    
    public void testFindSuffix() {

	String[] args = {"xml/"};
	String suffix = "gen";

	FindFiles ffiles = new FindFiles();
    //ffiles.setBase("/Users/timothyw/projects/software/Bob/BOBJava/test/xml");
    ffiles.setBase("test/xml");
	
	List fileList = null;
	RuntimeException re = null;
	try {
	    fileList = ffiles.find();
	}
	catch(RuntimeException e) {
	    re = e;
	}
	assertNotNull("exception should have been thrown b/c 'suffix not set'", re);
	
	ffiles.setSuffix(".xml");
	List fileList1 = ffiles.find();
	assertEquals("'fileList' SHOULD BE empty b/c bad 'suffix'", 5, fileList1.size());
	
    }

    public void testFindFile() {
		
		//String dirString = "/Users/timothyw/projects/software/Bob/BOBJava/test/xml";
		//String fileString = "/Users/timothyw/projects/software/Bob/BOBJava/test/xml/test1.xml";
		
		String dirString = "test/xml";
		String fileString = "test/xml/test1.xml";
		
		FindFiles ff = new FindFiles();
		ff.setBase("");
		List emptyList = ff.find("");
		assertNotNull("List should be empty, not NULL", emptyList);
		
		List resultList = ff.find(fileString);
		assertEquals("resultList SHOULD be 1", 1, resultList.size());
		
		ff.setSuffix(".xml");
		ff.setBase(".");
		List resultList1 = ff.find(dirString);
		
		assertEquals("resultList1 SHOULD be 5", 5, resultList1.size());
    }

    public void testFindDir() {
	
	//String baseString = "/Users/timothyw/projects/software/Bob/BOBJava/test";
	String baseString = "test";
	String dirString = "xml/";

	FindFiles ffiles = new FindFiles();
	ffiles.setBase(baseString);
	ffiles.setSuffix(".xml");
	List resultList = ffiles.find(dirString);

	assertEquals("resultList SHOULD be 5", 5, resultList.size());
	
	String baseString1 = ".";
	ffiles.setBase(baseString1);
	List resultList1 = ffiles.find(dirString);
	
	logger.debug("MAIN >> " + resultList1.toString());
	assertEquals("resultList1 SHOULD be 4", 4, resultList1.size());
    }
    
    public void testEmptyList() {

	String[] args = {};

	FindFiles ffiles = new FindFiles();
	ffiles.setBase(System.getProperty("user.dir"));
	List fileList = ffiles.find(args);

	assertNotNull("fileList should NOT be null", fileList);
	assertEquals("'fileList' should be empty", 0, fileList.size());
    }

    public void testBaseField() {

	String[] args = {"thing.xml"};
	//String baseString = "/Users/timothyw/projects/software/Bob/BOBJava/test/xml";
	String baseString = "test/xml";
	
	//logger.debug("testBaseField >>>>>>>>>>>");
	// ** fix broken test here
	FindFiles ffiles = new FindFiles();
	ffiles.setBase(System.getProperty("java.home"));
	List fileList = ffiles.find(args);
	//assertEquals("'fileList' SHOULD BE empty b/c wrong 'base'", 0, fileList.size());
	
	ffiles.setBase(baseString);
	List newList = ffiles.find(args);
	assertEquals("'fileList' SHOULD be 1", 1, newList.size());

	// all file names should be FULL path
	String fileName = (String)newList.get(0);
	assertTrue("File name should be absolute", fileName.startsWith(baseString));
    }
    
    public void testFileCheck() {
	
	// check a BAD list
	RuntimeException re = null;
	List flist = new ArrayList();
	flist.add("/aa/bb/cc");
	flist.add("/aa/bb/dd");
	flist.add("/aa/bb/ee");
	
	try {
	    FindFiles.listCheck(flist);
	}
	catch(RuntimeException e) {
	    re = e;
	}
	assertNotNull("a RuntimeException should be thrown for a bad file list", re);
	
	// check a GOOD list
	FindFiles ff = new FindFiles();
	ff.setBase(System.getProperty(Util.BASE));
	ff.setSuffix(System.getProperty(Util.END));
	List flist1 = ff.find();
	RuntimeException re1 = null;
	
	try {
	    FindFiles.listCheck(flist1);
	}
	catch(RuntimeException e) {
	    re1 = e;
	}
	assertNull("a RuntimeException SHOULD be null", re1);
    }
}


