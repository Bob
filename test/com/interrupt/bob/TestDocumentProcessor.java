
package com.interrupt.bob;

import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.handler.StubHandler;
import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;

public class TestDocumentProcessor extends TestCase {


	private Logger logger = Logger.getLogger(TestDocumentProcessor.class); 
	
	public TestDocumentProcessor() {}
    public TestDocumentProcessor(String test) {
	super(test);
    }

    public void setUp() {
	//System.setProperty(Util.BASE,"/Users/timothyw/projects/software/Bob/BOBJava/test/xml");
	System.setProperty(Util.BASE,"test/xml");
	System.setProperty(Util.END,".xml");
	System.setProperty(Util.GEN,"gen/");
    }
    public void tearDown() {
	System.setProperty(Util.BASE,"");
	System.setProperty(Util.END,"");
	System.setProperty(Util.GEN,"");
    }

    public static Test suite() {

	TestSuite suite = new TestSuite();
	//suite.addTest( new TestDocumentProcessor("testBadList") );
	
	//suite.addTest( new TestDocumentProcessor("testProcessList") );
	
	//suite.addTest( new TestDocumentProcessor("testReadFilesList") );
	//suite.addTest( new TestDocumentProcessor("testReadFilesDirectory") );

	return suite;
    }
    
    /*
    public void testBadList() {
		
		DocumentProcessor dprocessor = new DocumentProcessor();
		
		List badList = new ArrayList();
		badList.add("/aa/bb/cc");
		badList.add("/aa/bb/dd");
		badList.add("/aa/bb/ee");
	
		ProcessorException pe = null;
		try {
		    dprocessor.process(badList);
		}
		catch(ProcessorException e) {
		    pe = e;
		}
		assertNotNull("Expected a 'ProcessorException' ", pe);
		
    }
    */
    
    /*public void testProcessList() {
    	
		FindFiles ffiles = new FindFiles();
		ffiles.setBase(System.getProperty(Util.BASE));
		ffiles.setSuffix(System.getProperty(Util.END));
		List flist = ffiles.find();
		
		StubHandler handler1 = new StubHandler();
		StubHandler handler2 = new StubHandler();
		StubHandler handler3 = new StubHandler();
		StubHandler handler4 = new StubHandler();
	
		DocumentProcessor dprocessor = new DocumentProcessor();
		dprocessor.addHandler(handler1);
		dprocessor.addHandler(handler2);
		dprocessor.addHandler(handler3);
		dprocessor.addHandler(handler4);
		try {
		    dprocessor.process();
		}
		catch(ProcessorException e) {
		}
		List callbackList = dprocessor.allCallback();
	
		// 4 handlers * 5 files in 'test/xml'
		assertEquals("'callbackList' should be 20", 20, callbackList.size());
		
    }
    
    public void testReadFilesList() {

	// test ARRAY of files
	DocumentProcessor dprocessor = new DocumentProcessor();
	//System.setProperty(Util.HOME, "/Users/timothyw/projects/software/Bob/BOBJava.now");
	System.setProperty(Util.HOME, "BOBJava.now");
	ProcessorException pe = null;

	logger.debug();
	logger.debug();
	logger.debug();
	logger.debug();
	logger.debug("-------------- testReadFilesList ----------------");
	try {
	    List list = new ArrayList();
	    list.add("xml/test1.xml");
	    list.add("xml/test2.xml");
	    dprocessor.process(list);
	}
	catch(ProcessorException e) {
	    pe = e;
	}
	String message = null;
	if(pe != null) {
	    message = pe.getMessage();
	}
	assertNull("'ProcessorException' should be null; message["+ message +"]", pe);

    }


    public void testReadFilesDirectory() {

	// test DIRECTORY of files
	DocumentProcessor dprocessor = new DocumentProcessor();
	//System.setProperty(Util.HOME, "/Users/timothyw/projects/software/Bob/BOBJava.now");
	System.setProperty(Util.HOME, "BOBJava.now");
	ProcessorException pe = null;

	try {
	    //dprocessor.process("xml/");
	    dprocessor.process(new ArrayList());
	}
	catch(ProcessorException e) {
	    pe = e;
	}
	String message = null;
	if(pe != null) {
	    message = pe.getMessage();
	}
	assertNull("'ProcessorException' should be null; message["+ message +"]", pe);

    }
	*/
    
    
}



