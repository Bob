
package com.interrupt.bob.handler;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.handler.TagMerger;

public class TestTagMerger extends TestCase {
    
    
    public TestTagMerger() {}
    public TestTagMerger(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestTagMerger("testMergeChildren") );
	suite.addTest( new TestTagMerger("testMergeAttributes") );
	
	return suite;
    }
    
    public void testMergeChildren() {
	
	IBob achild = new Bob();
	achild.setTagName("achild");
	achild.setNamespace("com/one");
	IBob prebob = new Bob();
	prebob.addGenChild(achild);
	
	IBob bchild = new Bob();
	bchild.setTagName("bchild");
	bchild.setNamespace("com/two");
	IBob postbob = new Bob();
	postbob.addGenChild(bchild);

	TagMerger merger = new TagMerger();
	merger.mergeChildren(prebob,postbob);
	
	IBob r1 = (IBob)(prebob.allGenChildren()).get(0);
	IBob r2 = (IBob)(prebob.allGenChildren()).get(1);
	assertEquals("1 >> children did not merge properly", 2, (prebob.allGenChildren()).size());
	assertEquals("2 >> WRONG first tag", "com/one:achild", r1.getQName());
	assertEquals("3 >> WRONG second tag", "com/two:bchild", r2.getQName());
    }

    public void testMergeAttributes() {
	
	AttributesImpl attrpre = new AttributesImpl();
	attrpre.addAttribute("","something","something","","tim");
	attrpre.addAttribute("","more","more","","thing");
	//attrpre.setLocalName(0,"something");
	//attrpre.setValue(0,"tim");
	//attrpre.setLocalName(1,"more");
	//attrpre.setValue(1,"thing");
	
	AttributesImpl attrpost = new AttributesImpl();
	attrpost.addAttribute("","else","else","","sam");
	//attrpost.setLocalName(0,"else");
	//attrpost.setValue(0,"sam");
	
	IBob prebob = new Bob();
	prebob.setAttributes(attrpre);
	
	IBob postbob = new Bob();
	postbob.setAttributes(attrpost);
	
	TagMerger tmerger = new TagMerger();
	tmerger.mergeAttributes(prebob, postbob);
	
	Attributes aimpl = prebob.getAttributes();
	String v1 = aimpl.getValue("something");
	String v2 = aimpl.getValue("more");
	String v3 = aimpl.getValue("else");
	
	assertEquals("prebob has INCORRECT attribute length", 3, aimpl.getLength());
	assertEquals("1 >> WRONG attribute value", "tim", v1);
	assertEquals("2 >> WRONG attribute value", "thing", v2);
	assertEquals("3 >> WRONG attribute value", "sam", v3);
    }
    
}


