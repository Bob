
package com.interrupt.bob.handler;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;
import java.io.File;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.Balloon;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.util.Util;


public class TestXMLHandler extends TestCase {
    
    
    private String _basedir = null;
    private String _gendir = null;
    
    public TestXMLHandler() {}
    public TestXMLHandler(String name) {
	super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {
    	
		//_basedir = "/Users/timothyw/projects/software/Bob/BOBJava";
		_basedir = ".";
		_gendir = "gen/test";
		(System.getProperties()).setProperty(Util.BASE, _basedir);
		(System.getProperties()).setProperty(Util.GEN, _gendir);
		
    }

    public void tearDown() {
		
		//(System.getProperties()).setProperty(Util.BASE, "");
		//(System.getProperties()).setProperty(Util.GEN, "");
		
		// ensure all previous files are deleted
		//File deleteme = new File("/Users/timothyw/projects/software/Bob/BOBJava/gen/test/com/interrupt/test/TestFileWrite.java");
		File deleteme = new File("gen/test/com/interrupt/test/TestFileWrite.java");
		deleteme.delete();
		
		//File deleteInterface = new File("/Users/timothyw/projects/software/Bob/BOBJava/gen/test/com/interrupt/test/IFileWrite.java");
		File deleteInterface = new File("gen/test/com/interrupt/test/IFileWrite.java");
		deleteInterface.delete();
		
		//File deleteGen = new File("/Users/timothyw/projects/software/Bob/BOBJava/gen/test/com/interrupt/test/GFileWrite.java");
		File deleteGen = new File("gen/test/com/interrupt/test/GFileWrite.java");
		deleteGen.delete();
		
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	
	//suite.addTest( new TestXMLHandler("testEndElement") );
	suite.addTest( new TestXMLHandler("testWriteToFile") );
	suite.addTest( new TestXMLHandler("testMakeInterface") );
	suite.addTest( new TestXMLHandler("testMakeClass") );
	suite.addTest( new TestXMLHandler("testGenClassName") );
	suite.addTest( new TestXMLHandler("testGenInterfaceName") );
	return suite;
    }
    
    /** implement test for: 
     * 1) stack semantics and 
     * 2) make interf/class calling coherence
     */
    public void testEndElement() {
	
    }
    
    public void testWriteToFile() {
	
	XMLHandler xhandler = new XMLHandler();
	xhandler.writeToFile("com/interrupt/test", "TestFileWrite.java", "package com.interrupt.test; public class TestFileWrite{ }");
	
	File createdf = new File(_gendir,"com/interrupt/test/TestFileWrite.java");
	assertEquals("'TestWriteFile' DOES NOT exist", true, createdf.exists());
    }
    
    public void testMakeInterface() {
	
	Balloon balloon = new Balloon();
	String nsURI = "com/interrupt/test";
	Attributes attrs = new AttributesImpl();
	
	IBob tagdef = new Bob();
	tagdef.setNamespace("com/interrupt/test/");
	tagdef.setTagName("fileWrite");
	tagdef.setAttributes(attrs);
	
	XMLHandler xhandler = new XMLHandler();
	xhandler.makeInterface(tagdef, balloon, nsURI);

	File createdf = new File(_gendir,"com/interrupt/test/IFileWrite.java");
	assertEquals("'IFileWrite' DOES NOT exist", true, createdf.exists());
    }
    
    public void testMakeClass() {

	//logger.debug("testMakeClass");
	
	String nsURI = "com/interrupt/test";
	
	Balloon balloon = new Balloon();
	IBob tagdef = new Bob();
	tagdef.setTagName("fileWrite");
	tagdef.setNamespace(nsURI);
	
	XMLHandler xhandler = new XMLHandler();
	xhandler.makeClass(tagdef,balloon,nsURI);

	File createdf = new File(_gendir,"com/interrupt/test/GFileWrite.java");
	assertTrue("'com/interrupt/test/GFileWrite.java' file does not exist", createdf.exists());
    }

    public void testGenClassName() {
	
	String tname = "interrupt";
	
	XMLHandler xhandler = new XMLHandler();
	String cname = xhandler.genClassNameFromTag("");
	assertNotNull("generated class name (from empty String) is NULL", cname);

	String cname1 = xhandler.genClassNameFromTag(tname);
	assertNotNull("generated class name is NULL", cname1);
	assertEquals("WRONG generated class name", "GInterrupt", cname1);
    }
    public void testGenInterfaceName() {
	
	String tname = "interrupt";
	
	XMLHandler xhandler = new XMLHandler();
	String cname = xhandler.genInterfaceNameFromTag("");
	assertNotNull("generated class name (from empty String) is NULL", cname);

	String cname1 = xhandler.genInterfaceNameFromTag(tname);
	assertNotNull("generated class name is NULL", cname1);
	assertEquals("WRONG generated class name", "IInterrupt", cname1);
    }
    
}


