
package com.interrupt.bob.base;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobComparator;
import com.interrupt.bob.util.Util;

import com.interrupt.one.IThing;


public class TestBobComparator extends TestCase {
    
    
	private Logger logger = Logger.getLogger(TestBobComparator.class); 
	
	public TestBobComparator() {}
    public TestBobComparator(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
    	
    	System.getProperties().setProperty(Util.BASE, "."); 
		System.getProperties().setProperty(Util.END, ".xml"); 
		System.getProperties().setProperty(Util.DEF, "xml"); 
		
    }

    public void tearDown() {
    }
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();	
	suite.addTest( new TestBobComparator("testCompare") );
	suite.addTest( new TestBobComparator("testCompareComplex") );
	suite.addTest( new TestBobComparator("testEquals") );
	
	return suite;
    }


    public void testCompare() {
	
	IBob bob1 = new Bob();
	//bob1.setQName("com/interrupt/bob:tag1");
	bob1.setNamespace("com/interrupt/bob");
	bob1.setTagName("tag1");
	
	IBob bob2 = new Bob();
	//bob2.setQName("com/interrupt/bob:tag2");
	bob2.setNamespace("com/interrupt/bob");
	bob2.setTagName("tag2");
	
	IBob bob3 = new Bob();
	//bob3.setQName("com/interrupt/bob:tag1");
	bob3.setNamespace("com/interrupt/bob");
	bob3.setTagName("tag1");

	BobComparator bcomparator = new BobComparator();
	int result1 = bcomparator.compare(bob1,bob2);
	assertEquals("These tags SHOULD NOT compare", -1, result1);

	int result2 = bcomparator.compare(bob1,bob3);
	assertEquals("These tags SHOULD be the same", 0, result2);
    }

	public void testCompareComplex() { 
		
		IBob bob = new Bob();
		IThing thing1 = (IThing)bob.load("<thing xmlns='com/interrupt/one' address='thingaddr' ><interrupt xmlns='com/interrupt/two' id='interid' /></thing>"); 
		
		IThing thing2 = (IThing)bob.load("<thing xmlns='com/interrupt/one' address='thingaddr' ><interrupt xmlns='com/interrupt/two' id='interid' /></thing>"); 
		
		logger.debug( "thing1["+thing1+"]" ); 
		logger.debug( "thing2["+thing2+"]" ); 
		
		
		boolean equals1 = thing1.equals(thing2); 
		assertTrue( "These 2 'things' SHOULD be equal", equals1 ); 
		
		
		IThing thing3 = (IThing)bob.load("<thing xmlns='com/interrupt/one' address='' ><interrupt xmlns='com/interrupt/two' id='interid' /></thing>"); 
		logger.debug( "thing3["+thing3+"]" ); 
		
		boolean equals3 = thing1.equals(thing3);
		assertFalse( "These 2 'things' SHOULD NOT be equal", equals3 ); 
		
		IThing thing4 = (IThing)bob.load("<thing xmlns='com/interrupt/one' address='thingaddr' />"); 
		boolean equals4 = thing1.equals(thing4);
		logger.debug( "thing4["+thing4+"]" ); 
		
		assertFalse( "These 2 'things' SHOULD NOT be equal", equals4 ); 

		
	}
	
	
	
    public void testEquals() {

	BobComparator bcomparator1 = new BobComparator();
	BobComparator bcomparator2 = new BobComparator();
	Object obj1 = new Object();

	assertTrue("comparators SHOULD be equal", bcomparator1.equals(bcomparator2));
	assertFalse("comparators SHOULD NOT be equal", bcomparator1.equals(obj1));
    }
    
}


