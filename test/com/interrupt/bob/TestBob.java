package com.interrupt.bob;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.GAncestor;
import com.interrupt.IAncestor;
import com.interrupt.GWee;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.core.IXmlProcessor;
import com.interrupt.bob.core.ISystem;
import com.interrupt.bob.util.Util;
import com.interrupt.one.GThing;


public class TestBob extends TestCase {
    
	
	private Logger logger = Logger.getLogger(Bob.class); 
	
	public TestBob() {}
    public TestBob(String name) {
	super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {
    	
		System.getProperties().setProperty(Util.BASE, "."); 
		System.getProperties().setProperty(Util.END, ".xml"); 
		System.getProperties().setProperty(Util.DEF, "xml"); 
		
    }

    public void tearDown() {
    }
    
    public static Test suite() {
		
    	
    	// TODO - re-implement these tests 
		
		TestSuite suite = new TestSuite();	
		//suite.addTest( new TestBob("testGeneralChildren") );
		
		//suite.addTest( new TestBob("testToXML") );
		suite.addTest( new TestBob("testToXMLDepth") ); 
		//suite.addTest( new TestBob("testToXMLEmpty") );
		//suite.addTest( new TestBob("testMake") );
		
		suite.addTest( new TestBob("testMake001") );
		//suite.addTest( new TestBob("testLoad") );
		suite.addTest( new TestBob("testRemoveAllChildren") );
		suite.addTest( new TestBob("testRemoveChild") );
		//suite.addTest( new TestBob("testRemoveGenChild") );
		
		suite.addTest( new TestBob("testToXMLPrefix001") ); 
		
		
		suite.addTest( new TestBob("testToXMLPrefix002") ); 
		
		return suite; 
		
    }
    
    
    public void testMake() {
		
		IBob bobby = new Bob();
		IBob made = bobby.make();
	
		assertNotNull("MADE object is null", made);
	
		// namespace, namespaces, tagname, attributes
		assertNotNull("'namespace' was NULL", made.getNamespace());
		assertNotNull("'namespaces' was NULL", made.getNamespaces());
		assertNotNull("'tagname' was NULL", made.getTagName());
		//assertNotNull("'attributes' was NULL", made.getAttributes());
		
    }
    
    public void testMake001() { 
    	
    	
    	GThing thing = (GThing)Bob.make( "com.interrupt.one.GThing" ); 
    	
    	assertNotNull("THING object is null", thing);
    	
		// namespace, namespaces, tagname, attributes
		assertNotNull("'namespace' was NULL", thing.getNamespace());
		assertNotNull("'namespaces' was NULL", thing.getNamespaces());
		assertNotNull("'tagname' was NULL", thing.getTagName());
		
    	
    }
    
    
    /*public void testGeneralChildren() {
	
	IBob child1 = new Bob();
	child1.setTagName("child1");
	child1.setNamespace("one");
	
	IBob child2 = new Bob();
	child2.setTagName("child2");
	child2.setNamespace("two");
	
	IBob bob = new Bob();
	bob.addChild(child1);
	bob.addChild(child2);
	
	List kids = bob.allChildren();
	assertTrue("child list SHOULD NOT be empty", ( kids.size() > 0 ) );
	assertEquals("WRONG number of children in the list", 2, kids.size());
    }
    */
    
    public void testToXML() {
		
		AttributesImpl attrsParent = new AttributesImpl();
		attrsParent.addAttribute("","att1","","","123");
		
		AttributesImpl attrsChild = new AttributesImpl();
		attrsChild.addAttribute("","att2","","","456");
		
		IBob child1 = new Bob();
		child1.setNamespace("");
		child1.addNamespace("yy","yy");
		child1.setTagName("child");
		child1.setAttributes(attrsChild);
		
		IBob bob = new Bob();
		bob.setNamespace("aa/bb");
		bob.addNamespace("xx","xx");
		bob.setTagName("parent");
		bob.setAttributes(attrsParent);
		bob.addChild(child1);
	
	        /*      
		<parent xmlns="aa/bb" att1="123" >
		    <child att2="456" >
		</parent>
		 */
		//String expected = "<parent xmlns='aa/bb' att1='123' ><child att2='456' /></parent>";
		String expected = "<parent xmlns='aa/bb' xmlns:xx='xx' att1='123' ><child xmlns:yy='yy' att2='456' /></parent>";
		String actual = bob.toXML();
	
		logger.debug(">> Actual >> " + actual);
		assertEquals("xml string is BAD ", expected, actual);
		
    }
    
    public void testToXMLDepth() { 
	    
	    IBob bobby = new Bob(); 
	    
	    IAncestor ancestor = (IAncestor)bobby.load(new File("test/xml/test1.xml"),"test/xml/test1.xml"); 
	    ancestor.setProperty(IBob.XML_DEPTH, "-1"); 
	    String xmlString = ancestor.toXML(); 
	    
	    logger.debug("toXML depth / [\n"+xmlString+"]"); 
	    
	    
	    //** expected string should look something like this
	    String expectedXML = "<ancestor id='ancestorid' name='' address=''" + 
	        "xmlns='com/interrupt/'" + 
	        "xmlns:one='com/interrupt/one'" + 
	        "xmlns:two='com/interrupt/two'>" + 
	        "<parent id='parentid' name='pname' address='paddress'>" + 
	        "</parent>" + 
	        "<anotherOne id='anothoneid' name='aname'>" + 
	        "</anotherOne>" + 
	        "<somethingElse id='somethingElseid' name='sname' address='saddress'>" + 
	        "</somethingElse>" + 
	    "</ancestor>"; 
	    
	    
    }
    
    public void testToXMLEmpty() {
		
		AttributesImpl attrsParent = new AttributesImpl();
		attrsParent.addAttribute("","att1","","","123");
		
		IBob bob = new Bob();
		bob.setNamespace("aa/bb");
		bob.setTagName("parent");
		bob.setAttributes(attrsParent);
	
	        /*      
		<parent xmlns="aa/bb" att1="123" />
		 */
		String expected = "<parent xmlns='aa/bb' att1='123' />";
		String actual = bob.toXML();
		logger.debug(">> Actual >> " + actual);
		assertEquals("xml string is BAD ", expected, actual);
		
    }

	public void testLoad() {
		
		
		// load a Bob tree from an xml string
		String xml_source = "<system xmlns='com/interrupt/bob/core' ><xmlProcessor name='' /></system>";
		IBob fromXml = new Bob();
		IBob resultBob = fromXml.load( xml_source, "xml/system.xml" );
		String xml_result = resultBob.toXML(); 
		
		logger.debug("XML Source: " + xml_source );
		logger.debug("XML Result: " + xml_result.trim() );
		
		
		assertNotNull( "'From XML' xml is NULL", xml_result ); 
		assertEquals( "'From XML' xml does not match input XML", xml_source, xml_result );
		
		// TODO
		// problem 1. Bob.load() method finishes before XML callback has finished processing
		// problem 2. by default, BobHandler will create all possible children for a definition even if 
		//				those children weren't specified in the input XML
		
		
	}
	
	/*public void testToXMLPrefix001() { 
		
		
		IBob bobby = new Bob(); 
		IAncestor ancestor = (IAncestor)bobby.load(new File("xml/test1.xml"), "xml/test1.xml"); 
		//ancestor.toXML(System.out);
		
		
		// 1. find <wee id="weeid"/>
		IBob weeBob = ancestor.search("wee", "id", "weeid"); 
		//weeBob.toXML(System.out);
		
		
		// 2. is namespace visible?  ( it's not )
		boolean nsvisible = weeBob.isNSVisible(); 
		assertFalse("the namespace SHOULD NOT be visible", nsvisible); 
		
		
		// 3. ensure namespace 'com/interrupt/' 
		String NS = ((GWee)weeBob).getNamespace(); 
		assertEquals("the namespace SHOULD be 'com/interrupt/'", "com/interrupt/", NS);
		
		
		// 4. ensure namespace mappings are 0 
		HashMap nsMap = weeBob.getPrefixMappings(); 
		assertEquals("the namespace map SHOULD be empty", 0, nsMap.size()); 
		
		
		// 5. ensure parent namespace is 'com/interrupt/one' 
		IBob parent = weeBob.getParent(); 
		String parentNS = ((GThing)parent).getNamespace();
		

		logger.debug("NAMESPACE: "+ NS); 
		logger.debug("PARENT NAMESPACE: "+ parentNS); 
		
		assertEquals("the parent namespace SHOULD be 'com/interrupt/one'", "com/interrupt/one", parentNS);
		
	}
	*/
	

	/*public void testToXMLPrefix002() { 
		
		/*
		<one:thing id="thingid" name="tname" xmlns:deux="com/interrupt/deux">
        </one:thing>
        
        	- is prefixed 
			- namespace mapping comes from ancestor mappings 
		* / 
		
		IBob bobby = new Bob(); 
		IAncestor ancestor = (IAncestor)bobby.load(new File("xml/test1.xml"), "xml/test1.xml"); 
		ancestor.toXML(System.out);
		
		// 1. find <one:thing id="thingid" ... >
		IBob thingBob = ancestor.search("thing", "id", "thingid"); 
		thingBob.toXML(System.out);
		
		
		// 2. is namespace visible?  ( it is, from prefix )
		boolean nsvisible = thingBob.isNSVisible(); 
		assertTrue("the namespace SHOULD be visible", nsvisible); 
		
		
		// 3. ensure namespace 'com/interrupt/one' 
		String NS = ((GThing)thingBob).getNamespace(); 
		assertEquals("the namespace SHOULD be 'com/interrupt/one'", "com/interrupt/one", NS);
		
		
		// 4. ensure namespace mappings are 1 & correct 
		HashMap nsMap = thingBob.getPrefixMappings(); 
		assertEquals("the namespace map SHOULD be 1", 1, nsMap.size()); 
		
		String mappedNS = (String)nsMap.get("deux"); 
		assertEquals("the mapped namespace SHOULD be 'com/interrupt/deux'", "com/interrupt/deux", mappedNS); 
		
		
		// 5. ensure parent namespace is 'com/interrupt/' 
		IBob parent = thingBob.getParent().getParent(); 
		String parentNS = ((GAncestor)parent).getNamespace();
		
		logger.debug("NAMESPACE: "+ NS); 
		logger.debug("PARENT NAMESPACE: "+ parentNS); 
		
		assertEquals("the parent namespace SHOULD be 'com/interrupt/'", "com/interrupt/", parentNS);
		
	}
	*/
	
	/* currently working code, but include the tests 
	 * 
	 * TODO - write the XML output with namespace prefix mappings 
	 * TODO - child nodes with prefixes that don't have a mappings should break 
	 */
	public void testToXMLPrefixOutput() { 
		
		//... 
		
	}
	
	
	public void testRemoveAllChildren() { 
		
		IBob fromXml = new Bob();
		
		String xml_source = "<system xmlns='com/interrupt/bob/core' ><xmlProcessor name='p1' /><xmlProcessor name='p2' /></system>";
		IBob resultBob = fromXml.load( xml_source, "xml/system.xml" );
		
		assertEquals("there should be 2 children in list", 2, resultBob.allChildren().size()); 

		resultBob.removeAllChildren(); 
		assertEquals("there should be NO children in list", 0, resultBob.allChildren().size()); 
		
	}
	
	public void testRemoveChild() { 
		
		
		IBob fromXml = new Bob();
		
		String xml_source = "<system xmlns='com/interrupt/bob/core' ><xmlProcessor name='p1' /><xmlProcessor name='p2' /></system>";
		IBob resultBob = fromXml.load( xml_source, "xml/system.xml" );
		
		String xml_source2 = "<xmlProcessor name='p1' xmlns='com/interrupt/bob/core' />";
		IBob resultBob2 = fromXml.load( xml_source2, "xml/system.xml" ); 
		
		boolean remove_result = resultBob.removeChild(resultBob2); 
		assertTrue("not getting a 'true' result when removing <xmlProcessor name='p1' />", remove_result); 
		assertEquals("WRONG child size", 1, resultBob.allChildren().size()); 

		IXmlProcessor xprocessor = ((ISystem)resultBob).findXmlProcessorByName("p2");
		assertNotNull("WRONG child was removed", xprocessor);
		
		
	}
	
	/* TODO - to finish this, refactor the BobComparator
	 */
	public void testRemoveGenChild() { 
		

		IBob fromXml = new Bob();
		
		String xml_source = "<system xmlns='com/interrupt/bob/core' />";
		IBob resultBob = fromXml.load( xml_source, "xml/system.xml" );
		
		String xml_source1 = "<xmlProcessor name='p1' xmlns='com/interrupt/bob/core' />";
		IBob resultBob1 = fromXml.load( xml_source1, "xml/system.xml" ); 
		
		String xml_source2 = "<xmlProcessor name='p2' xmlns='com/interrupt/bob/core' />";
		IBob resultBob2 = fromXml.load( xml_source2, "xml/system.xml" ); 
		
		String xml_source3 = "<xmlProcessor name='p3' xmlns='com/interrupt/bob/core' />";
		IBob resultBob3 = fromXml.load( xml_source3, "xml/system.xml" ); 
		
		resultBob.addGenChild(resultBob1); 
		resultBob.addGenChild(resultBob2); 
		resultBob.addGenChild(resultBob3); 
		
		assertEquals("there SHOULD be 3 gen.children", 3, resultBob.allGenChildren().size());
		
		
		String xml_remove = "<xmlProcessor name='p2' xmlns='com/interrupt/bob/core' />";
		IBob removeBob2 = fromXml.load( xml_remove, "xml/system.xml" ); 
		boolean remove_result = resultBob.removeGenChild(removeBob2); 
		
		assertTrue("did not properly remove xmlProcessor name='p2'", remove_result); 
		assertEquals("there SHOULD NOW be 2 gen.children", 2, resultBob.allGenChildren().size());
		
	}
	
	
	public static void main(String args[]) { 
		
		/*List alist = new ArrayList(); 
		alist.add("123"); 
		alist.add("456"); 
		alist.add("789"); 
		logger.debug( "1 >> " + alist.size() ); 
		
		alist.remove("456");
		logger.debug( "2 >> " + alist.size() ); 
		
		
		// >>>>>
		System.getProperties().setProperty(Util.BASE, "."); 
		System.getProperties().setProperty(Util.END, ".xml"); 
		System.getProperties().setProperty(Util.DEF, "xml"); 
		
		
		IBob fromXml = new Bob();
		
		String xml_source = "<system xmlns='com/interrupt/bob/core' ><xmlProcessor name='p1' /><xmlProcessor name='p2' xmlns='com/interrupt/bob/core' /></system>";
		IBob resultBob = fromXml.load( xml_source, "xml/system.xml" );
		
		String xml_source3 = "<xmlProcessor name='p3' xmlns='com/interrupt/bob/core' />";
		IBob resultBob3 = fromXml.load( xml_source3, "xml/system.xml" ); 
		
		logger.debug(""); 
		logger.debug(""); 
		
		resultBob.addChild(resultBob3); 
		logger.debug("1 > Actual >> " + resultBob.allChildren().size() ); 
		
		
		boolean remove_result = resultBob.removeChild(resultBob3); 
		logger.debug("2 > Actual >> " + resultBob.allChildren().size() ); 
		
		
		
		String xml_source2 = "<xmlProcessor name='p2' xmlns='com/interrupt/bob/core' />";
		IBob resultBob2 = fromXml.load( xml_source2, "xml/system.xml" ); 
		remove_result = resultBob.removeChild(resultBob2); 
		
		logger.debug("3 > Actual >> " + resultBob.allChildren().size() ); 
		logger.debug("3 > Index >> " + resultBob.allChildren().indexOf(resultBob2) ); 
		
		IXmlProcessor xpp = ((ISystem)resultBob).findXmlProcessorByName("p2");
		boolean xpp_b = xpp.equals(resultBob2);
		logger.debug("3 > Equals >> " + xpp_b ); 
		
		String xpp_x = xpp.toXML();
		String resultBob2_x = resultBob2.toXML(); 
		
		logger.debug("3 > Compare xpp ["+xpp_x+"]" ); 
		logger.debug("3 > Compare resultBob2 ["+resultBob2_x+"]" ); 
		logger.debug("3 > XML Equals >> " + xpp_x.equals(resultBob2_x)); 
		*/
		
		TestBob tbob = new TestBob(); 
		tbob.setUp(); 
		//tbob.testToXMLPrefix002(); 
		
	}
	
}




