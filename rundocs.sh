#!/bin/bash

export BOB_HOME=/Users/timothyw/projects/software/Bob/BOBJava
export CP="\
build/src:\
build/test:\
$BOB_HOME/lib/Piccolo.jar:\
$BOB_HOME/lib/commons-cli-1.0.jar:\
$BOB_HOME/lib/commons-logging-api.jar:\
$BOB_HOME/lib/commons-logging.jar:\
$BOB_HOME/lib/junit.jar:\
$BOB_HOM/lib/javafind.jar:\
$BOB_HOME/lib/log4j-1.2.7.jar:\
$BOB_HOME/lib/velocity-1.3.1.jar:\
$BOB_HOME/lib/velocity-dep-1.3.1.jar"

export PKGS="\
com.interrupt.bob \
com.interrupt.bob.base \
com.interrupt.bob.generator \
com.interrupt.bob.handler \
com.interrupt.bob.processor \
com.interrupt.bob.util \
com.interrupt.callback"

javadoc -classpath $CP -d doc/api -sourcepath src/:test/:gen/ $PKGS


