#!/bin/bash

export BOB_HOME="."
export CP="\
$BOB_HOME/:\
$BOB_HOME/lib/bob.jar:\
$BOB_HOME/lib/Piccolo.jar:\
$BOB_HOME/lib/AntelopeTasks_3.2.19.jar:\
$BOB_HOME/lib/commons-cli-1.0.jar:\
$BOB_HOME/lib/commons-logging.jar:\
$BOB_HOME/lib/commons-logging-api.jar:\
$BOB_HOME/lib/javafind.jar:\
$BOB_HOME/lib/junit.jar:\
$BOB_HOME/lib/log4j-1.2.7.jar:\
$BOB_HOME/lib/velocity-1.3.1.jar:\
$BOB_HOME/lib/velocity-dep-1.3.1.jar"

mkdir test-gen
java -classpath $CP com.interrupt.bob.Main -gen test-gen -base . -end .xml -def 'test/xml/bookkeeping.2.system.xml' -sys 'test/xml/bookkeeping.2.system.xml'

