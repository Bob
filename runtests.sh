#!/bin/bash

export BOB_HOME=/Users/timothyw/projects/software/Bob/BOBJava
export CP="\
.:\
templates:\
build/gen:\
build/src:\
build/test:\
$BOB_HOME/lib/:\
$BOB_HOME/lib/Piccolo.jar:\
$BOB_HOME/lib/commons-cli-1.0.jar:\
$BOB_HOME/lib/commons-logging-api.jar:\
$BOB_HOME/lib/commons-logging.jar:\
$BOB_HOME/lib/javafind.jar:\
$BOB_HOME/lib/junit.jar:\
$BOB_HOME/lib/log4j-1.2.7.jar:\
$BOB_HOME/lib/velocity-1.3.1.jar:\
$BOB_HOME/lib/velocity-dep-1.3.1.jar"

#java -classpath $CP junit.textui.TestRunner com.interrupt.bob.base.TestBob
#java -classpath $CP junit.textui.TestRunner com.interrupt.bob.processor.TestDefinitionProcessor
#java -classpath $CP junit.textui.TestRunner com.interrupt.bob.TestDocumentProcessor


#./rundocs.sh
java -classpath $CP junit.textui.TestRunner com.interrupt.bob.AllTests


