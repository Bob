#!/bin/bash

export BOB_HOME=/Users/timothyw/projects/software/Bob/BOBJava
export CP="\
build/src:\
build/test:\
build/bob.jar"

export PKGS="\
com.interrupt \
com.interrupt.one \
com.interrupt.two \
com.interrupt.deux"

javadoc -classpath $CP -d gen/doc/ -sourcepath src/:test/:gen/ $PKGS


