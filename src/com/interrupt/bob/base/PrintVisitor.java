
package com.interrupt.bob.base;

import org.apache.log4j.Logger;

public class PrintVisitor implements IVisitor {
    
	private Logger logger = Logger.getLogger(PrintVisitor.class); 
	
	private int order = 0;
    public int depth = 0;
    
    public void visit(IBob bob) {
    	
    	order += 1;
		logger.debug("BOB >> order["+ this.depth +"] > namespace["+ bob.getNamespace() +"] > tag["+ bob.getTagName() +"]");
    }
        
}


