
package com.interrupt.bob.base;


import java.util.List;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

/**
 * create opening and closing xml string
 */
public class ToXMLVisitorPass1 implements ISaxVisitor {
	
	
	private Logger logger = Logger.getLogger(ToXMLVisitorPass1.class); 
	
	private int depth = -1; 
	private int depthIndex = 0; 
	private int depthPlumb = 0; 
	private boolean inlineNS = true; 
    //private String parentns = null; 
	
    private StringBuilder xmlString = null;

    public ToXMLVisitorPass1() {
		//parentns = "";
		xmlString = new StringBuilder();
    }
    
    
    public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	public void setInlineNS(boolean inline) { 
		inlineNS = inline; 
	}
	public boolean isInlineNS() { 
		return inlineNS; 
	}
	
	
	public String getXML() {
    	
		if(xmlString == null) {
		    return null;
		}
		return xmlString.toString();
    }
	
	
    public void visitStart(IBob bob) {
	    
    	// control the depth we are going to go to 
    	if(depth > 0) { 
    		
    		depthIndex += 1; 
    		//logger.debug("visitStart / tag["+bob.getTagName()+"] / depth["+depth+"] / index["+depthIndex+"]"); 
    		if(depthIndex > depth) { 
        		return;
        	}
    	}
    	//logger.debug("visitStart... entering"); 
    	
		/*  <parent xmlns="aa/bb" 
		 *	att1="123" >
		 *
		 *	<child att2="456" >
		 *  </parent>
		 */
    	// deal with 1st ns
		//if( parentns == null ) {
		//    parentns = bob.getNamespace();
		//}
		
		// write out tag name
		StringBuilder openString = new StringBuilder();
		openString.append("<"); 
		
		String nsPrefix = bob.getNSPrefix(); 
		
		if(inlineNS) { 
			//if(bob.isNSVisible() && (nsPrefix != null) && (nsPrefix.trim().length() > 0)) { 
			if((nsPrefix != null) && (nsPrefix.trim().length() > 0)) { 
					
				//logger.debug("-------------> kilimanjaro 1 xxx"); 
				openString.append(bob.getNSPrefix());
				openString.append(":"); 
			}
		}
		openString.append(bob.getTagName());
		openString.append(" ");
		
		// if currentns is not null
		//logger.debug("ToXMLVisitoPass1 visiting["+bob.getTagName()+"] / ns["+bob.getNamespace()+"] / nsVisible["+bob.isNSVisible()+"] / writeNS["+writeNS+"]"); 
		//if(!inlineNS || (bob.getParent() == null)) { 
		if( !inlineNS ) { 
			
			//logger.debug---------> kilimanjaro 2 ok"); 
			String currentns = bob.getNamespace();
			
		    // TODO - clashes with prefix logic 
			// if ns is same as parent, then DO NOT write out the ns
		    //if( !currentns.equals(parentns) && !currentns.equals("")) {
	    	if( !currentns.equals("") ) { 
	    		
				//parentns = currentns;
				openString.append("xmlns='");
				openString.append(currentns);
				openString.append("'");
				openString.append(" "); 
		    }
	    	
		}
		
		HashMap namespaces = bob.getPrefixMappings(); 
		if(namespaces == null) { 
			namespaces = new HashMap(); //** prefix mappings should never be NULL, only empty 
		}
	    //logger.debug("1. >>>>>>>> HERE >>>>>>>>>>> "+ namespaces);
	    
	    Set keys = namespaces.keySet();
	    Iterator iter = keys.iterator();
	    String nextKey = null;
	    while(iter.hasNext()) {
	    	
			nextKey = (String)iter.next();
			if(nextKey == null || (nextKey.trim().length() < 1)) { 
				continue; 
			}
			
			openString.append("xmlns:");
			openString.append(nextKey);
			openString.append("=");
			openString.append("'");
			openString.append(namespaces.get(nextKey));
			openString.append("'");
			openString.append(" ");
	    }
	    
		
		// ** 1) get bob definitions 2) get attributes from definitions
		
		// write out attribute name/value
		Attributes attrs = bob.getAttributes();
		//logger.debug("2. >>>>>>>> HERE >>>>>>>>>>> "+ attrs);
	    
		
		int attLength = attrs.getLength();
		String aname = null;
		String avalue = null;
		//logger.debug("toXML >>>> attLength["+ attLength +"]");
		for(int i=0; i < attLength; i++) {
		    
		    aname = attrs.getLocalName(i);
		    avalue = attrs.getValue(i);
		
		    openString.append(aname);
		    openString.append("='");
		    openString.append(avalue);
		    openString.append("' ");
	
		    //logger.debug("toXML >> aname["+aname+"] > avalue["+avalue+"]");
		}
		
		boolean isContent = false;
		String bcontent = bob.getContent();
		if( bcontent != null ) {
		    bcontent = bcontent.trim();
		    if( bcontent.length() > 1 ) {
		    	isContent = true;
		    }
		}
		
		// if no children / text, write empty tag -> end
		List children = bob.allChildren();
		if( children.isEmpty() && !isContent ) {
		//if( children.isEmpty() ) {
		    
		    openString.append("/>");
		    bob._setOpenString(openString.toString());
		    return;
		}
	
		// if children, write children
		openString.append(">");
		
		
		//openString.append("\n");
    	
		
		bob._setOpenString(openString.toString());
		
    }
    
    
    public void visitEnd(IBob bob) {
		
    	
    	// control the depth we are going to go to 
    	if(depth > 0) { 
        	
    		//logger.debug("visitEnd / tag["+bob.getTagName()+"] / depth["+depth+"] / index["+depthIndex+"]"); 
    		if(depthIndex > depth) { 
    			depthIndex -= 1; 
        		return;
        	}
    		depthIndex -= 1; 
    		
    	}
    	//logger.debug("visitEnd... entering"); 
    	
		boolean isContent = false;
		String bcontent = bob.getContent();
		if( bcontent != null ) {
		    bcontent = bcontent.trim();
		    if( bcontent.length() > 1 ) {
		    	isContent = true;
		    }
		}
		
		// if no children / text, write empty tag -> end
		List children = bob.allChildren();
		if( children.isEmpty() && !isContent ) {
		//if( children.isEmpty() ) {
		    bob._setCloseString("");
		    return;
		}
		
		StringBuilder closeString = new StringBuilder();
		

    	//** formatted
    	//for(int i = 0; i < depthIndex; i++) { 
    	//	xmlString.append("\t");
    	//}
    	
    	
		String text = bob.getContent();
		if(text != null) {
		    if(!text.equals("")) {
		    	closeString.append(bob.getContent());
		    }
		}
		closeString.append("<");
		closeString.append("/");
		
		String nsPrefix = bob.getNSPrefix(); 
		if(inlineNS) { 
			if(bob.isNSVisible() && (nsPrefix != null) && (nsPrefix.trim().length() > 0)) { 
				closeString.append(bob.getNSPrefix());
				closeString.append(":"); 
			}
		}
		closeString.append(bob.getTagName());
		closeString.append(">");
		
		closeString.append("\n");
    	
		bob._setCloseString(closeString.toString());
		
    }
}


