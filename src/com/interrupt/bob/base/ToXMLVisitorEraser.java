package com.interrupt.bob.base;

public class ToXMLVisitorEraser implements IVisitor {
	
	public void visit(IBob bob) { 
		
		bob._setOpenString(null); 
		bob._setCloseString(null); 
	}
}
