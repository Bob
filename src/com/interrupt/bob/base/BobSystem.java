
package com.interrupt.bob.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.FindFiles;
import com.interrupt.bob.core.Memory;
import com.interrupt.bob.core.MemoryField;
import com.interrupt.bob.core.Queue;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.BobHandler;
import com.interrupt.bob.handler.HandlerField;
import com.interrupt.bob.handler.IHandlerField;
import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;

public class BobSystem {
    
	
	private Logger logger = Logger.getLogger(BobSystem.class); 
	
    // Name stuff
    public static String DEFINITIONS = "definition.list";
    public static String SYSTEM = "bob.system";
    
    private List definitions = null;
    private IBob system = null;
    private Map _messageMap = null;
    //private boolean _initialised = false;
    private BobSystem() {
    		_messageMap = new HashMap();
    }
    
    /* Singleton stuff
     */
    private static BobSystem _instance = null;
    public static BobSystem getInstance() {
	
	if(_instance == null) {
	    _instance = new BobSystem();
	}
	return _instance;
    }
    
    /* Message
     */
    public void setMessage(String _name, Object _val) {
    		_messageMap.put(_name,_val);
    }
    public Object getMessage(String _name) {
    		return _messageMap.get(_name);
    }
    
    /* Definitions
     */
    public List getDefinitions() {
	return definitions;
    }
    public void setDefinitions(List defs) {
	definitions = defs;
    }

    /* System
     */
    public IBob getSystem() {
    	return system;
    }
    public void setSystem(IBob sys) {
    	system = sys;
    }
    
    
    public void buildSystem( List plist ) {
		
		
		StringBuilder fileString_b  = new StringBuilder();
		Iterator fileIter = plist.iterator(); 
		while( fileIter.hasNext() ) { 
			
			fileString_b.append( " " ); 
			fileString_b.append( (String)fileIter.next() ); 
			
		}
		String fileString_s = fileString_b.toString(); 
		

		logger.debug( "INPUT List["+ fileString_s +"]" ); 
		
		// definitions
		DocumentProcessor dprocessor = null;
		try {
		    
			DefinitionHandler dhandler = new DefinitionHandler(); 
			dhandler.setFiles( fileString_s ); 
			dhandler.setOut( Util.DEF ); 
				
				IHandlerField hfield = new HandlerField(); 
				hfield.setName("tagList"); 
				hfield.setValue("${"+ Util.DEF +"}"); 
				
			BobHandler bhandler = new BobHandler(); 
			bhandler.setFiles( fileString_s ); 
			bhandler.setOut( Util.SYS ); 
			bhandler.addHandlerField(hfield); 
			
		    dprocessor = new DocumentProcessor();
		    dprocessor.addHandler(dhandler);
		    dprocessor.addHandler(bhandler);
		    dprocessor.process();
		    
		    
		    Queue queue = dprocessor.getQueue();
		    logger.debug( "HERE 1 / Queue["+ queue +"]" );
		    
		    Memory memory = (Memory)queue.getMemory();
		    
		    MemoryField defField = (MemoryField)memory.findMemoryFieldByName( Util.DEF ); 
		    logger.debug( "HERE 2 / defField["+ defField +"]" );
		    definitions = (List)defField.getValue(defField); 
		    
		    MemoryField sysField = (MemoryField)memory.findMemoryFieldByName( Util.SYS ); 
		    logger.debug( "HERE 3 / sysField["+ sysField +"]" );
		    system = (IBob)sysField.getValue(sysField);
		    
		    logger.debug("============== END ==================");
		    system.toXML( System.out );
		    logger.debug("\n\n\n");
		    
		}
		catch(ProcessorException e) {
		    e.printStackTrace();
		}
		
    }
}


