package com.interrupt.bob.base;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class DepthReplaceVisitor implements IVisitor { 
	
	
	private Logger logger = Logger.getLogger(DepthReplaceVisitor.class); 
	
	private boolean found = false; 
	private String tagName = null; 
	private String id = null; 
	private IBob replacor = null; 
	
	public IBob getReplacor() {
		return replacor;
	}
	public void setReplacor(IBob res) { 
		replacor = res; 
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isFound() { 
		return found; 
	}
	
	public void visit(IBob bob) { 
		
		logger.debug( "DepthReplaceVisitor.visit ["+ !found +"]"); 
		if(!found) { 
			
			String id = bob.getAttributes().getValue("id"); 
			logger.debug( "tag["+bob.getTagName()+"] / id["+id+"] / match["+bob.getTagName().equals( this.getTagName())+"]"); 
			
			if(	bob.getTagName().equals( this.getTagName() ) && 
				id.equals( this.getId() ) ) { 
				
				//logger.debuglogger.debug"+ bob); 
				
				replacor.setParent(bob.getParent()); 
				bob = replacor; 
				
				//logger.debug("repllogger.debug; 
				found = true; 
				
			}
		}
	}
}

