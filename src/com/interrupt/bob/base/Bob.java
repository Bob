package com.interrupt.bob.base;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PushbackReader;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Stack;

import com.interrupt.IAncestor;
import com.interrupt.bob.FindFiles;
//import com.interrupt.bob.Attributes;
/*import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobException;
import com.interrupt.bob.base.ToXMLVisitorPass1;
import com.interrupt.bob.base.ToXMLVisitorPass2;
*/
import com.interrupt.bob.core.IMemory;
import com.interrupt.bob.core.IMemoryField;
import com.interrupt.bob.core.Memory;
import com.interrupt.bob.core.MemoryField;
import com.interrupt.bob.core.Queue;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.BobHandler;
import com.interrupt.bob.util.StringUtil;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.processor.DocumentProcessor; 
import com.interrupt.bob.processor.cc.DepthFirstVisitor; 

import com.interrupt.bob.util.Util;
import com.interrupt.cc.xpath.lexer.Lexer;
import com.interrupt.cc.xpath.lexer.LexerException;
import com.interrupt.cc.xpath.node.Node;
import com.interrupt.cc.xpath.node.Start;
import com.interrupt.cc.xpath.parser.Parser;
import com.interrupt.cc.xpath.parser.ParserException;

import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource; 


public class Bob implements IBob {
    
	
	private Logger logger = Logger.getLogger(Bob.class); 
	
    // '_genchildren' used when compiling definitions
    protected TreeSet _genchildren = null;
    protected List _children = null;
    protected IBob _parent = null;
    
    private Properties _properties = null; 
    private boolean _nsVisible = true;
    private String _name = null;
    protected String _namespaceURI = null;
    private HashMap _namespaceMap = null;
    
    private HashMap _nsPrefixMappings = null; 
    private String nsPrefix = null; 
    
    protected Attributes _attributes = null;
     
    private String _text = null;
    private String _openString = null;
    private String _closeString = null;

    public Bob() {
		
		_name = "bob";
		_namespaceURI = "";
		
		_properties = new Properties(); 
		
		_children = new ArrayList();
		_genchildren = new TreeSet(new BobComparator());
		_namespaceMap = new HashMap();
		_attributes = new AttributesImpl();
		this.resetPull();
    }
    public Bob(String namespace, String tagName) {
		
		_name = tagName;
		_namespaceURI = namespace;
		
		_properties = new Properties(); 
		
		_children = new ArrayList();
		_genchildren = new TreeSet(new BobComparator());
		_namespaceMap = new HashMap();
		_attributes = new AttributesImpl();
		this.resetPull();
    }
    
	
	public void overwriteShallow(IBob overwritor) { 
    	
    	this.setAttributes(overwritor.getAttributes()); 
    	this.setChildren(overwritor.getChildren()); 
    	
    }
    
    
    public void setProperty(String name, String value) { 
    	
    	_properties.setProperty(name, value); 
    }
	public String getProperty(String name) { 
		
		return (String)_properties.getProperty(name); 
	}
	
	

	public boolean isNSVisible() { 
		return _nsVisible;
	}
	public void setNSVisible(boolean visible)  {
		this._nsVisible = visible;
	}
	
	
	public void setNSPrefix(String prefix) { 
		this.nsPrefix = prefix; 
	}
    public String getNSPrefix() { 
    	return nsPrefix; 
    }
    
	
		
    public IBob getParent() {
		return _parent;
	}
	public void setParent(IBob _parent) {
		this._parent = _parent;
	}
	
	public void addChild(IBob child) {
		this._children.add(child);
    }
	public void addChildren(List children) {
		this._children.addAll(children);
    }
	
    public List allChildren() {
		return new ArrayList(_children);
    }
	public void removeAllChildren() { 
		this._children.clear(); 
	}
	public boolean  removeChild(IBob child) {
		
		// can't do this as an 'equals' comparison is not working
		//return this._children.remove(child);
		if(child == null) { 
			return false; 
		}
		
		// TODO - this is much more inefficient!!!
		Iterator liter = this._children.iterator(); 
		IBob eachbob = null; 
		while(liter.hasNext()) { 
			
			eachbob = (IBob)liter.next(); 
			if(child.equals(eachbob)) { 
				liter.remove();
				return true;
			}
		}
		return false; 
	} 
	
	public boolean remove(String tagName, String attName, String attValue) { 
		
		
    	logger.debug("remove 1 / tname["+ tagName +"] / attName["+ attName +"] / attValue["+ attValue +"]"); 
		
    	Iterator iter = this.getChildren().iterator(); 
    	
    	IBob eachBob = null; 
    	boolean matchFound = false; 
    	
    	//** only loop if there's children 
    	while(iter.hasNext()) { 
    		
    		eachBob = (IBob)iter.next(); 
    		boolean mfound = eachBob.remove(tagName, attName, attValue); 
			if(mfound) { 
				return mfound; 
			}
			
			logger.debug(" 2 EACH / tname["+ eachBob.getTagName() +"] / attName["+ attName +"] / attValue["+ eachBob.getAttributeValue(attName) +"] / MATCH["+ (eachBob.getTagName().equals( tagName ) && eachBob.getAttributeValue(attName).equals(attValue)) +"]"); 
			if(	eachBob.getTagName().equals( tagName ) && 
    			eachBob.getAttributeValue(attName).equals(attValue) ) { 
    			
    			iter.remove(); 
    			matchFound = true; 
    			logger.debug("MATCH FOUND!! / tname["+ tagName +"] / attName["+ attName +"] / attValue["+ attValue +"]"); 
    			logger.debug("Bob.logger.debugg ["+ eachBob +"] "); 
    	    	
    			break; 
    		}
			
    	}
    	
    	return matchFound;
	}
	
	public boolean remove(String tagName, String tagId) { 
		
		return this.remove(tagName, "id", tagId); 
	}
	
    
    public List allGenChildren() {
		return new ArrayList(_genchildren);
    }
    public void addGenChild(IBob child) {
		this._genchildren.add(child);
    }
	public void removeAllGenChildren() { 
		this._genchildren.clear(); 
	}
	public boolean removeGenChild(IBob child) { 
		
		
		// can't do this as an 'equals' comparison is not working
		//return this._genchildren.remove(child);
		
		// TODO - this is much more inefficient!!!
		Iterator liter = this._genchildren.iterator(); 
		IBob eachbob = null; 
		while(liter.hasNext()) { 
			
			eachbob = (IBob)liter.next(); 
			if(child.equals(eachbob)) { 
				liter.remove();
				return true;
			}
		}
		return false; 
	}
    
    
	/** 
	 * find methods 
	 */
	public IBob find(String tagName, String tagId) { 
		
		DepthFindVisitor dfVisitor = new DepthFindVisitor(); 
		dfVisitor.setTagName(tagName); 
		dfVisitor.setId(tagId); 
		
		this.accept(dfVisitor); 
		return dfVisitor.getResult(); 
	}
	
	public List find(String xpath) { 
		
		//if(true) 
		//	throw new RuntimeException("XPath 2.0 evaluation not yet implemented"); 
		
		ByteArrayInputStream baInputStream = new ByteArrayInputStream( xpath.getBytes() ); 
		PushbackReader pbreader = 
			new PushbackReader( new InputStreamReader(baInputStream), 1024 ); 
		
		List xdmResults = new ArrayList();  
		DepthFirstVisitor rdvisitor = new DepthFirstVisitor(); 
		rdvisitor.setDocument(this); 
		
		System.out.println("xpath > start"); 
		try { 
			
			//** parse the syntax and provide the MODEL 
			Lexer lexer = new Lexer(pbreader); 
			Parser parser = new Parser(lexer); 
			
			System.out.println("xpath > parsing input"); 
            
			// parse the input 
			Start tree = parser.parse(); 
			
			System.out.println(""); 
			System.out.println("xpath > running analyser"); 
            
			// apply the Visitor 
			tree.apply(rdvisitor); 
			xdmResults = rdvisitor.getXdmList(); 
			
			System.out.println("xpath > finished"); 
            
		}
		catch(java.io.IOException e) { 
			e.printStackTrace(); 
		}
		catch(LexerException e) { 
			e.printStackTrace(); 
		}
		catch(ParserException e) { 
			e.printStackTrace(); 
		}
		catch(Exception e) { 
			e.printStackTrace();  
		}
		
		return xdmResults; 
	}
	public String xpath() { 
		
		return xpath(false); 
		
	}
	public String xpath(boolean relative) { 
		
		StringBuilder sbuffer = new StringBuilder(); 
		
		// get parent axis string
		if(!relative) { 
			
			IBob parent = this.getParent(); 
			while(parent != null) { 
				
				sbuffer.insert(0, "/"); 
				sbuffer.insert(0, parent.buildAxisString()); 
				parent = parent.getParent(); 
			}
			sbuffer.insert(0, "/"); 
		}
		
		// append the axis string 
		sbuffer.append(this.buildAxisString()); 
		return sbuffer.toString(); 
		
	}
	
	public String buildAxisString() { 
		
		// take node name & append predicates 
		String axisString = this.getTagName() + this.buildPredicateString(this.getAttributes()); 
		return axisString; 
	}
	public String buildPredicateString(Attributes attributes) { 
		
		// construct predicate based on attributes. should look something like... 
		// [ @attr='value' and @attr='value' ] 
		
		StringBuilder pbuffer = new StringBuilder(); 
		pbuffer.append("[ "); 
		for(int i = 0; i < attributes.getLength(); i++) { 
			
			String avalue = attributes.getValue(i).trim(); 
			if(avalue != null) { 
				
				String aname = attributes.getLocalName(i); 
				pbuffer.append("@"); 
				pbuffer.append(aname); 
				pbuffer.append("="); 
				pbuffer.append("'"); 
				pbuffer.append(avalue); 
				pbuffer.append("'"); 
				
				if(i < (attributes.getLength() - 1)) { 
					
					pbuffer.append(" and "); 
				}
			}
		}
		pbuffer.append(" ]"); 
		
		return pbuffer.toString(); 
	}
	
	
    public void replace(IBob replacor) { 
    	
    	// the parent should stay the same 
    	IBob parent = this.getParent(); 
    	
    	replacor.setParent(parent); 
    	if( parent != null ) { 
	    	parent.removeChild(this); 
	    	parent.addChild(replacor);
    	}
    }
    
    /** 
     * ** a weakness of this method is that it cannot replace itself 
     */
    public boolean replace(String tagName, String tagId, IBob replacor) { 
		
    	logger.debug("Bob.replace["+ tagName +"] / tagId["+ tagId +"] / replacor["+ replacor.getClass() +"]"); 
    	Iterator iter = this.getChildren().iterator(); 
    	
    	IBob eachBob = null; 
    	boolean matchFound = false; 
    	
    	//** only loop if there's children 
    	while(iter.hasNext()) { 
    		
    		eachBob = (IBob)iter.next(); 
    		boolean mfound = eachBob.replace(tagName, tagId, replacor); 
			if(mfound) { 
				return mfound; 
			}
			
			logger.debug("Bob.replace 2 / logger.debugteValue(id)["+ eachBob.getAttributeValue("id") +"] / MATCH["+ (eachBob.getTagName().equals( tagName ) && 
	    			eachBob.getAttributeValue("id").equals( tagId )) +"]"); 
	    	
    		if(	eachBob.getTagName().equals( tagName ) && 
    			eachBob.getAttributeValue("id").equals( tagId ) ) { 
    			
    			//logger.debug("Bob.replace 3"); 
    			iter.remove(); 
    			matchFound = true; 
    			//logger.debug("Bob.replace 2 / MATCH FOUND!logger.debugme +"] / id["+ tagId +"]"); 
    			//logger.debug("Bob.replace 3 / replacing["+ eachBob +"]"); 
    	    	
    			break; 
    		}
			
    	}
    	
    	if(matchFound) { 
    		this.addChild(replacor);
    	}
    	
    	return matchFound; 
	}
	
    
    public IBob getRoot() { 
        
        if(this._parent != null) { 
            return this._parent.getRoot();
        }
        return this; 
    }
    
    /*public IBob searchTree(String tag, String attributeName, String attributeValue) { 
        
        return this.getRoot().search(tag, attributeName, attributeValue); 
    }
	*/
    
	
    public void accept(IVisitor visitor) throws BobException { 
		
    	//logger.debug(""); 
    	//logger.debug("Bob.accept: tag["+this.getTagName()+"] / children.size["+_children.size()+"]"); 
		Iterator iter = _children.iterator(); 
		IBob next = null; 
		while(iter.hasNext()) { 
		    
			try { 
				next = (IBob)iter.next(); 
			} 
			catch(Throwable e) { 
				
				logger.debug("Cause: "+ e.getCause()); 
				logger.debug("Message: "+ e.getMessage()); 
				e.printStackTrace(System.out); 
				//logger.debug("Bob.accept: next["+next+"]"); 
				//logger.debug("Bob.accept: children["+_children+"] / children.size["+_children.size()+"] / iterator["+_children.iterator()+"]");
				throw new BobException(e); 
			}
			
		    next.accept(visitor); 
		}
		visitor.visit(this); 
    }
    public void acceptFirst(IVisitor visitor) throws BobException {
		
		visitor.visit(this);
	
		Iterator iter = _children.iterator();
		IBob next = null;
		while(iter.hasNext()) {
		    
		    next = (IBob)iter.next();
			next.acceptFirst(visitor);
		}
    }
    public void acceptSax(ISaxVisitor visitor) throws BobException {
	
		visitor.visitStart(this);
	
		Iterator iter = _children.iterator();
		IBob next = null;
		while(iter.hasNext()) {
		    
		    next = (IBob)iter.next();
			next.acceptSax(visitor);
		}
		visitor.visitEnd(this);
    }
	

	/* some methods to pull through the entire tree of objects 
	 */
	private Stack _pullStack = null; 
	public IBob pullNext() { 
		
		if(this._pullStack == null) { 
			return null;
		}
		if(this._pullStack.empty()) { 
			return null;
		}
		return (IBob)this._pullStack.pop(); 
	}
	public boolean canPull() { 

		if(this._pullStack == null) { 
			return false;
		}
		if(this._pullStack.empty()) { 
			return false;
		}
		return !this._pullStack.empty();
	}
	public void resetPull() { 

		class PullVisitor implements IVisitor { 
			
			private Stack pullStack = new Stack(); 
			public void setStack(Stack stack) { 
				this.pullStack = stack; 
			}
			public Stack getStack() { 
				return this.pullStack;
			}
			
			public void visit(IBob bob) { 
				
				this.pullStack.push(bob);
			}
			
		}
		PullVisitor pvisitor = new PullVisitor(); 
		this.accept(pvisitor);
		
		this._pullStack = pvisitor.getStack(); 
		
	}
	
	
	/* search the tree for the first IBob node with this 
	 */
	public IBob search( String tag, String attributeName, String attributeValue ) { 
		
		
		SearchVisitor svisitor = new SearchVisitor(); 
		svisitor.setTagName(tag); 
		svisitor.setAttributeName(attributeName); 
		svisitor.setAttributeValue(attributeValue); 
		
		this.accept(svisitor); 
		return svisitor.getResult(); 
		
	}
    

    
    /* toXML
     */
    public void toXML(java.io.PrintStream ps) {
    	this.toXML(true,ps); 
    }
    public void toXML(boolean inlineNS, java.io.PrintStream ps) {
    	ps.print(toXML(inlineNS));
		ps.flush();
    }
	public String toXML() {
    	return this.toXML(true); 
    }
    public String toXML(boolean inlineNS) {
    	
    	//** clear XML String 
    	ToXMLVisitorEraser veraser = new ToXMLVisitorEraser(); 
    	this.accept(veraser); 
    	
    	//** compose XML String 
    	ToXMLVisitorPass1 txVisitor1 = new ToXMLVisitorPass1(); 
    	txVisitor1.setInlineNS(inlineNS); 
    	
    	String depth_s = this.getProperty(IBob.XML_DEPTH); 
    	if((depth_s != null) && (depth_s.length() > 0)) { 
    		int depth = Integer.parseInt(depth_s); 
    		txVisitor1.setDepth(depth); 
    	}
    	ToXMLVisitorPass2 txVisitor2 = new ToXMLVisitorPass2();
		
		this.acceptSax(txVisitor1);
		this.acceptSax(txVisitor2);
		return txVisitor2.getXML();
		
    }
    
    
    /* namespaces
     */
    public void setNamespace(String namespaceURI) {
	_namespaceURI = namespaceURI;
    }
    public String getNamespace() {
	return _namespaceURI;
    }
    public void addNamespace(String ns, String value) {
	_namespaceMap.put(ns,value);
    }
    public String getNamespace(String ns) {
	return (String)_namespaceMap.get(ns);
    }
    public HashMap getNamespaces() {
	return new HashMap(_namespaceMap);
    }
    public boolean hasNamespaces() {
	return !_namespaceMap.isEmpty();
    }
    
    
    /* namespace preix mappings 
     */
    public void setPrefixMappings(HashMap prefixMappings) { 
    	this._nsPrefixMappings = prefixMappings; 
    }
    public HashMap getPrefixMappings() { 
    	return this._nsPrefixMappings; 
    }
    
    
    /* attribute accessors
     */
    public void setTagName(String localName) {
	_name = localName;
    }
    public String getTagName() {
	return _name;
    }
    
    //public void setQName(String qName) {
    //_qname = qName;
    //}
    public String getQName() {
		
		if(this._namespaceURI != null) {
			
			StringBuilder temp = new StringBuilder(); 
	    	if(!this._namespaceURI.equals("")) {
				temp.append(this._namespaceURI);
				temp.append(":");
	    	}
	    	temp.append(this._name); 
	    	return temp.toString(); 
		}
		return ""; 
    }
    
    public void setAttributes(Attributes attribs) {
    	_attributes = attribs;
    }
    public Attributes getAttributes() {
    	return _attributes;
    }
    
    public String getAttributeValue(String aname) {
    	return _attributes.getValue(aname); 
    }
    public void setAttributeValue(String aname, String avalue) { 
    	
    	((AttributesImpl)_attributes).setValue(_attributes.getIndex(aname), avalue); 
    }
    
    public void setContent(String text) {
	_text = text;
    }
    public String getContent() {
	return _text;
    }
    
    
    
    public IBob getChild(int cindex) { 
    	return (IBob)_children.get(cindex); 
    }
    public List getChildren() {
		return _children;
	}
	public void setChildren(List _children) {
		this._children = _children;
	}
	
	
	public void _setOpenString(String os) {
	_openString = os;
    }
    public String _getOpenString() {
	return _openString;
    }
    public void _setCloseString(String cs) {
	_closeString = cs;
    }
    public String _getCloseString() {
	return _closeString;
    }
    
    
    public IBob make() {
    	return new Bob();
    }
    
    public static IBob make(String namespace, String localName) {
		
		
    	Logger logger = Logger.getLogger(Bob.class); 
		logger.debug("Bob:: make / namespace["+namespace+"] / localname["+localName+"]"); 
    	
		/* TODO *** KLUDGE *** CLASS CREATION
		 * - there should be a 'createGName' / 'createIName'
		 */
		String ns = StringUtil.namespaceToPackage(namespace);
		String cname = StringUtil.upperFirstLetter(localName);
		logger.debug("1. Bob:: make / classname["+ ns + "." + cname +"]"); 
		
		Class bobClass = null;
		try {
			
				//logger.debug("A"); 
		    if(ns.trim().length() > 0) {
				//logger.debug("B"); 
		    	bobClass = Class.forName( ns + "." + cname );
		    }
		    else {
				//logger.debug("C"); 
		    	bobClass = Class.forName( cname );
		    }
			//logger.debug("D. Bob:: make / class["+ bobClass +"]"); 
		}
		catch(ClassNotFoundException e) {
		    
		    //e.printStackTrace();
		    try {
		    	
		    	logger.debug("2. Bob:: make / classname["+ ns + "." + "G" + cname +"]"); 
				if(ns.length() > 0) {
		
				    bobClass = Class.forName( ns + "." + "G" + cname );
				}
				else {
				    bobClass = Class.forName( "G" + cname );
				}
		    }
		    catch(ClassNotFoundException ex) {
		    	ex.printStackTrace();
		    }
		}
	
		//logger.debug("E"); 
	
		IBob result = null;
		try {
			
			//logger.debug("F"); 
			
			result = (IBob)bobClass.newInstance(); 
		    result.setNamespace( namespace ); 
		    
		    //logger.debug("G: "+ result); 

		}
		catch(InstantiationException e) {
		    e.printStackTrace();
		}
		catch(IllegalAccessException e) {
		    e.printStackTrace();
		}
		logger.debug("3. Bob:: generated class["+ result +"]"); 
	
		return result;
    }
    
    

    public static IBob make( String fqclassname ) {
		
    	
		Class bobClass = null;
		try {
		    
			//logger.debug("make / fqclassname["+fqclassname+"]"); 
	    	bobClass = Class.forName( fqclassname ); 
		    
		}
		catch(ClassNotFoundException e) {
		    
		    e.printStackTrace();
		    
		}
		
		
		String package_s = fqclassname.substring( 0, fqclassname.lastIndexOf(".") ); 
		String namespace = StringUtil.packageToNamespace( package_s ); 
		IBob result = null;
		try {
		    result = (IBob)bobClass.newInstance(); 
		    result.setNamespace( namespace ); 
		}
		catch(InstantiationException e) {
		    e.printStackTrace();
		}
		catch(IllegalAccessException e) {
		    e.printStackTrace();
		}
	
		return result; 
		
    }
    
    
    public static IBob loadS(String xmlString) {

		String def_s = System.getProperties().getProperty(Util.DEF); 
		return Bob.loadS( xmlString, def_s );
	}
    public static IBob loadS(String xmlString, String definitionFiles) {
		
    	
    	Logger.getLogger(Bob.class).debug("Bob.load("+xmlString+", "+definitionFiles+")"); 
    	
    	//System.out
		ByteArrayInputStream bs = null; 
		try { 
			bs = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
		}
		catch(java.io.UnsupportedEncodingException e) { 
			e.printStackTrace();
		}
		return Bob.loadS( bs, definitionFiles ); 
	}
	
	
	
    public static IBob loadS(File xmlFile) {
		
		java.io.FileInputStream fis = null; 
		try { 
			fis = new java.io.FileInputStream(xmlFile);
		}
		catch(java.io.FileNotFoundException e) { 
			Logger.getLogger(Bob.class).error(e,e); 
		}
		return Bob._loadS( fis );
	}
    public static IBob loadS(File xmlFile, String definitionFiles) {

		java.io.FileInputStream fis = null; 
		try { 
			fis = new java.io.FileInputStream(xmlFile);
		}
		catch(java.io.FileNotFoundException e) { 
			Logger.getLogger(Bob.class).error(e,e); 
		}
		return Bob._loadS( fis, definitionFiles );
	}
	
	
	
    public static IBob loadS(InputStream xmlis) {
		return Bob._loadS( xmlis );
	}
    public static IBob loadS(InputStream xmlis, String definitionFiles) {
		return Bob._loadS( xmlis, definitionFiles );
	}
	
	
	
	private static IBob _loadS( Object inputSource ) {

		String def_s = System.getProperties().getProperty(Util.DEF); 
		return Bob._loadS( inputSource, def_s );
	}
	
	private static IBob _loadS( Object inputSource, String definitionFiles ) {
		
		Logger.getLogger(Bob.class).debug( "Bob._load("+inputSource+", "+definitionFiles+")" ); 
		
		// check NULLs 
		if( definitionFiles == null || definitionFiles.trim().length() < 1 ) { 
			Logger.getLogger(Bob.class).error("ERROR. definition files not specified for [_loadS( Object inputSource, String definitionFiles )]. Stopping."); 
			return null;
		}
		
		// create a BobHandler and its Listener 
		class LoadHandler implements com.interrupt.bob.DocumentHandler { 
			
			private Logger logger = Logger.getLogger(Bob.class); 
			private IBob bobToChange = null; 
			public void endHandle(com.interrupt.callback.CallbackEvent cevent) { 
				 
				Object result = cevent.getMessage();
				if( result instanceof IBob) { 
					
					Bob rootBob = (Bob)result; 
					logger.debug("LoadHandler.endHandle / bobResult / ["+ rootBob.toXML() +"]"); 
					bobToChange = (IBob)rootBob.allChildren().get(0);
					
				}
			}
			
			public IBob getBobToChange() { 
				return bobToChange;
			}
			public void setBobToChange(IBob bob) { 
				bobToChange = bob;
			}
			
		}
		LoadHandler lhandler = new LoadHandler();
		lhandler.setBobToChange(new Bob()); 
		
		com.interrupt.bob.handler.DocumentHandler definitionHandler = new com.interrupt.bob.handler.DefinitionHandler(); 
		definitionHandler.setNamespace("com.interrupt.bob.handler");
		definitionHandler.setType("com.interrupt.bob.handler.DefinitionHandler"); 
		definitionHandler.setFiles(definitionFiles);
		definitionHandler.setOut("definition.list"); 
		
		
		com.interrupt.bob.handler.IHandlerField handlerField = new com.interrupt.bob.handler.HandlerField(); 
		handlerField.setName("tagList");
		handlerField.setValue("${definition.list}"); 
		
		com.interrupt.bob.handler.DocumentHandler bobHandler = new com.interrupt.bob.handler.BobHandler(); 
		bobHandler.setNamespace("com.interrupt.bob.handler");
		bobHandler.setType("com.interrupt.bob.handler.BobHandler"); 
		bobHandler.setFiles("${bob.files}");
		bobHandler.setOut("bob.tree"); 
		bobHandler.addHandlerField(handlerField); 
		bobHandler.addListener(lhandler);
		
		com.interrupt.bob.core.Queue queue = new com.interrupt.bob.core.Queue(); 
		queue.addDocumentHandler(definitionHandler); 
		queue.addDocumentHandler(bobHandler); 
		
		
		MemoryField mfield = new MemoryField();
		mfield.setName("bob.files");
		mfield.setValue(inputSource);
		queue.getMemory().addMemoryField(mfield); 
		try { 
			
			Logger.getLogger(Bob.class).debug("BEGIN execute"); 
			queue.execute(); 
			Logger.getLogger(Bob.class).debug("END execute"); 
			
		}
		catch(ProcessorException e) {
			e.printStackTrace(System.out);
		}
		
		
		return lhandler.getBobToChange(); 
		
	}
    
	
	public IBob load(String xmlString) { return Bob.loadS(xmlString); } 
	public IBob load(String xmlString, String definitionFiles) { return Bob.loadS(xmlString, definitionFiles); }  
	
    public IBob load(File xmlFile) { return Bob.loadS(xmlFile); } 
	public IBob load(File xmlFile, String definitionFiles) { return Bob.loadS(xmlFile, definitionFiles); } 
	
    public IBob load(InputStream xmlis) { return Bob.loadS(xmlis); } 
	public IBob load(InputStream xmlis, String definitionFiles) { return Bob.loadS(xmlis, definitionFiles); } 
	
	
    public boolean equals(Object bob) {
    	
    	if( !(bob instanceof IBob) ) { 
    		
    		//object for comparison is not an instance of 'IBob'
    		return false; 
    	}
    	IBob ibob = (IBob)bob; 
    	
    	// the specific XML structure must match 
		//return super.equals(bob);
    	String thisXML = this.toXML(); 
    	String bobXML = ibob.toXML(); 
    	
    	return thisXML.equals(bobXML); 
    	
    }
    
    public int hashCode() { 
        
        /*String xmls = this.toXML(false); 
        int intValue = this..intValue();  
        return intValue * 178345; 
        */
        return 178345;
    }
    
    public String toString() { 
    	
    	return this.toXML(false); 
    }
    
    
    public static void main(String args[]) { 
	    
    	System.getProperties().setProperty(Util.BASE, "."); 
		System.getProperties().setProperty(Util.END, ".xml"); 
		System.getProperties().setProperty(Util.DEF, "xml"); 
		
	    IBob bobby = new Bob(); 
	    com.interrupt.bookkeeping.GBookkeeping bkeeping = (com.interrupt.bookkeeping.GBookkeeping)bobby.load(new File("test/xml/bookkeeping.2.system.xml"),"test/xml/bookkeeping.2.system.xml");
	    bkeeping.setProperty(IBob.XML_DEPTH, "1"); 
	    
	    System.out.println("bkeeping["+ bkeeping.toXML(false) +"]"); 
	    
	    
	    class XOutput implements IVisitor { 
    		
    		public void visit(IBob bob) { 
    			System.out.println(""); 
    			System.out.println("Visiting ["+ bob.getTagName() +"]"); 
    			System.out.println(bob.xpath(false)); 
    		}
    	}
    	XOutput xout = new XOutput(); 
    	
    	
    	//** basic xpath expression with predicate
    	//List result = bkeeping.find("/bookkeeping[ @id=' ' ]"); 
    	//[ok] List result = bkeeping.find("/bookkeeping"); 
    	//[ok] List result = bkeeping.find("/"); 
    	
    	
    	//** multiple predicates (implicit 'and') 
    	//[ok] List result = bkeeping.find("/bookkeeping[ @id='' ]/journals[ @id='' ]/journal[ @zzz='timmy' ] [ @id='j1' or @name='generalledger' or @type='' or @balance='' ]"); 
    	//** List result = bkeeping.find("/bookkeeping[ @id='' ]/journals[ @id='' ]/journal[ @id='j1' or @name='generalledger' or @type='' ] [ @balance='' ]"); 
    	
    	
    	//** predicates with and/or conditions 
    	//[ok] List result = bkeeping.find("/bookkeeping[ @id='' ]/journals[ @id='' ]/journal[ @id='xxx' or @name='xxx' or @type='xxx' or @balance='xxx' ]"); 
    	//[ok] List result = bkeeping.find("/bookkeeping[ @id='' ]/journals[ @id='' ]/journal[ @id='j1' and @name='generalledger' and @type='notthere' and @balance='' ]"); 
    	//[ok] List result = bkeeping.find("/bookkeeping[ @id='' ]/journals[ @id='' ]/journal[ @id='j1' or @name='generalledger' and @type='' or @balance='' ]"); 
    	
    	
    	//** System.out.println("\n\n\nFind RESULTS..."); 
    	//** System.out.println(result); 
    	
    	// needs to return an XDM 1. Bob, text, sequence, etc 
    	
    }
    
}


