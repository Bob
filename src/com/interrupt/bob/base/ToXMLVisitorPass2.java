package com.interrupt.bob.base;


import java.util.List;
import org.xml.sax.Attributes;

/**
 * Assemble the XML string
 */
public class ToXMLVisitorPass2 implements ISaxVisitor {
	
    private String parentns = null;
    private StringBuilder xmlString = null;
    private int formatDepth = 0; 
    
    public ToXMLVisitorPass2() {
		parentns = "";
		xmlString = new StringBuilder();
    }
    
    public String getXML() {
		if(xmlString == null) {
		    return null;
		}
		return xmlString.toString();
    }
    
    public void visitStart(IBob bob) {
    	
    	//** formatted
    	//for(int i = 0; i < formatDepth; i++) { 
    	//	xmlString.append("\t");
    	//}
    	//formatDepth +=1; 
    	
    	String ostring = bob._getOpenString();
    	if( (ostring != null) && (ostring.trim().length() > 0) ) { 
    		xmlString.append(ostring); 
    	}
    	
    	//xmlString.append("\n");
    	
    }
    public void visitEnd(IBob bob) {
    	
    	//** formatted
    	//for(int i = 0; i < formatDepth; i++) { 
    	//	xmlString.append("\t");
    	//}
    	//formatDepth -=1; 
    	
    	String cstring = bob._getCloseString(); 
    	if( (cstring != null) && (cstring.trim().length() > 0) ) { 
    		xmlString.append(cstring);
    	}
    	
    	//xmlString.append("\n");
    	
    }
    
}


