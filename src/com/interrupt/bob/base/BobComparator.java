package com.interrupt.bob.base;


import java.util.Comparator;

public class BobComparator implements Comparator {

    public int compare(Object obj1, Object obj2) {
	    
	IBob bob1 = (IBob)obj1;
	IBob bob2 = (IBob)obj2;
	    
	String qname1 = bob1.getQName();
	String qname2 = bob2.getQName();
	
	if((qname1 != null) && (qname2 != null)) {
	    return qname1.compareTo(qname2);
	}
	return -1;
    }

    public boolean equals(Object object) {
	Comparator comparator = null;
	
	try { 
	    comparator = (Comparator)object;
	}
	catch(ClassCastException e) {
	    return false;
	}

	return object instanceof BobComparator;
    }
}


