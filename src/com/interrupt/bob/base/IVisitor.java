package com.interrupt.bob.base;

public interface IVisitor {

    public void visit(IBob bob);
    
}
