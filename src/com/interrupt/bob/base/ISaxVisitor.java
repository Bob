
package com.interrupt.bob.base;

public interface ISaxVisitor {

    public void visitStart(IBob bob);
    public void visitEnd(IBob bob);
    
}
