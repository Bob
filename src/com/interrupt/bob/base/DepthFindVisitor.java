package com.interrupt.bob.base; 

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;


public class DepthFindVisitor implements IVisitor { 
	
	private boolean found = false; 
	private String tagName = null; 
	private String id = null; 
	private IBob result = null; 
	
	public IBob getResult() {
		return result;
	}
	public void setResult(IBob res) { 
		result = res; 
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public void visit(IBob bob) { 
		
		if(!found) { 
			
			// ((IAllowedActions)bob).getId().equals( this.getId() )
			String id = bob.getAttributes().getValue("id"); 
			
			if(	bob.getTagName().equals( this.getTagName() ) && 
				id.equals( this.getId() )
				) { 
				
				result = bob; 
				found = true;
			}
		}
	}
}
