package com.interrupt.bob.base;

import org.xml.sax.Attributes;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class SearchVisitor implements IVisitor {
    
    private String tagName = null; 
    private String attributeName = null; 
    private String attributeValue = null; 
    private IBob result = null; 
    private boolean found = false; 
    
    
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    public String getAttributeName() {
        return attributeName;
    }
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    public String getAttributeValue() {
        return attributeValue;
    }
    public void setAttributeValue(String attributevalue) {
        this.attributeValue = attributevalue;
    }
    
    public IBob getResult() {
        return result;
    }
    public void setResult(IBob result) {
        this.result = result;
    }
    
    
    public void visit(IBob bob) { 
        
        System.out.println("SearchVisitor.visit["+bob.getTagName()+"]"); 
        String tag = bob.getTagName(); 
        Attributes attributes = null; 
        
        if(found) { 
            return; 
        }
        if(tag != null && tag.trim().length() > 0 && tag.equals(tag)) { 
            
            attributes = bob.getAttributes(); 
            int asize = attributes.getLength(); 
            String aname = null; 
            String avalue = null; 
            for(int i = 0; i < asize; i++) { 
                aname = attributes.getLocalName(i); 
                if(aname != null && aname.trim().length() > 0 && aname.equals(attributeName)) { 
                    avalue = attributes.getValue(i); 
                    if(avalue != null && avalue.trim().length() > 0 && avalue.equals(attributeValue)) { 
                        
                        result = bob; 
                    }
                }
            }
        }
    }
    
}
