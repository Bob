
package com.interrupt.bob.base;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import org.xml.sax.Attributes;

public interface IBob extends Serializable {
	
	
	public static final String XML_DEPTH = "XML_DEPTH"; 
	
	public void replace(IBob replacor); 
	public void overwriteShallow(IBob overwritor); 
	
	// Properties for this Bob node 
	public void setProperty(String name, String value); 
	public String getProperty(String name); 
	
	
	public boolean isNSVisible();
	public void setNSVisible(boolean visible); 
	
	
	// Children 
    public List allChildren();
	public void removeAllChildren(); 
    public void addChild(IBob child);
    public void addChildren(List children); 
    public boolean removeChild(IBob child); 
    public boolean remove(String tagName, String attName, String attValue); 
    public boolean remove(String tagName, String tagId); 
	
    
	// Gen Children 
    public List allGenChildren();
	public void removeAllGenChildren(); 
    public void addGenChild(IBob child);
	public boolean removeGenChild(IBob child); 
    
    public IBob getParent(); 
	public void setParent(IBob _parent); 
	
	
	/** 
	 * find methods 
	 *
		added xpath find to the interface  
		A) public IBob find(String tagName, String tagId); 
		B) public IBob find(String xpath); 
		
		to take an xpath expression and return an XDM data type (sequence, element, string, number) 
	*/
	public IBob find(String tagName, String tagId); 
	public List find(String xpath); 			// return result based on xpath string 
	public String xpath(); 						// return the xpath string of this Bob node 
	public String xpath(boolean relative); 
	public boolean replace(String tagName, String tagId, IBob replacor); 
	
	public String buildAxisString(); 
	public String buildPredicateString(Attributes attributes); 
	
	
    public IBob getRoot(); 
    
    // Search for the first node with tag and attribute name/value match 
    //public IBob search(String tag, String attributeName, String attributeValue); 
    //public IBob searchTree(String tag, String attributeName, String attributeValue); 
    
    
    /* 'accept' methods
     *
     * accept(IVisitor visitor) - visits the children first
     * acceptFirst(IVisitor visitor) - visits the parent first
     * acceptSax(ISaxVisitor visitor) 
     *	    - uses an ISaxVisitor to visit before(visitStart) and after(visitEnd) children
     */
    public void accept(IVisitor visitor);
    public void acceptFirst(IVisitor visitor);
    public void acceptSax(ISaxVisitor visitor);
    
    
	// Pull methods
	public IBob pullNext(); 
	public boolean canPull();
	public void resetPull();
	
	
	// toXML
    public void toXML(java.io.PrintStream ps);
    public void toXML(boolean inlineXML, java.io.PrintStream ps);
    public String toXML();
    public String toXML(boolean inlineXML);
    
    public void _setOpenString(String os);
    public String _getOpenString();
    public void _setCloseString(String cs);
    public String _getCloseString();
    
    // namespace and namespace list
    public void setNamespace(String namespaceURI);
    public String getNamespace();
    
    public void addNamespace(String ns, String value); // TODO - deprecate 
    public String getNamespace(String ns);	// TODO - deprecate 
    public HashMap getNamespaces(); // TODO - deprecate 
    public boolean hasNamespaces(); // TODO - deprecate 
    
    
    public void setPrefixMappings(HashMap prefixMappings); 
    public HashMap getPrefixMappings(); 
    
    public void setNSPrefix(String prefix); 
    public String getNSPrefix(); 
    
    
    // attribute accessors
    public void setTagName(String localName);
    public String getTagName();
    //public void setQName(String qName);
    public String getQName();
    //public void setAttributes(com.interrupt.bob.Attributes attribs);
    //public com.interrupt.bob.Attributes getAttributes();
    public void setAttributes(Attributes attribs);
    public Attributes getAttributes(); 
    public String getAttributeValue(String avalue); 
    public void setAttributeValue(String aname, String avalue); 
    
    public void setContent(String text);
    public String getContent();
    
    public IBob getChild(int cindex); 
    public List getChildren(); 
	public void setChildren(List _children); 
	
    public IBob make(); 
    //public static IBob make(String namespace, String localName); 
    //public static IBob make( String fqclassname ); 
    
	public IBob load(String xmlString);
	public IBob load(String xmlString, String definitionFiles); 
	
    public IBob load(File xmlFile);
	public IBob load(File xmlFile, String definitionFiles); 
	
    public IBob load(InputStream xmlis);
	public IBob load(InputStream xmlis, String definitionFiles); 
	
}

