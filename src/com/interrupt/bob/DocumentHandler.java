package com.interrupt.bob;

import com.interrupt.callback.CallbackEvent;

public interface DocumentHandler {
    
    public void endHandle(CallbackEvent event);
    
}

