package com.interrupt.bob;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.interrupt.bob.base.Bob;

public class Balloon {

    
	private Logger logger = Logger.getLogger(Balloon.class); 
	
	public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    private String namespace = null;
    private Attributes attributes = null;

    private StringBuilder packageBalloon = null;
    private StringBuilder classDeclBalloon = null;

    private StringBuilder attributesBalloon = null;
    private StringBuilder accessorsBalloon = null;

    private StringBuilder childrenBalloon = null;
    private StringBuilder findersBalloon = null;
    private StringBuilder listersBalloon = null;
    private StringBuilder addersBalloon = null;
    private StringBuilder removersBalloon = null;
    private StringBuilder removeAllBalloon = null;
    private StringBuilder makeBalloon = null;

    private StringBuilder classCloseBalloon = null;
    
    
    public Balloon() {
		
		packageBalloon = new StringBuilder();
		classDeclBalloon = new StringBuilder();
		
		attributesBalloon = new StringBuilder();
		accessorsBalloon = new StringBuilder();
		
		childrenBalloon = new StringBuilder();
		findersBalloon = new StringBuilder();
		listersBalloon = new StringBuilder();
		addersBalloon = new StringBuilder();
		removersBalloon = new StringBuilder();
		removeAllBalloon = new StringBuilder();
		makeBalloon = new StringBuilder();
		
		classCloseBalloon = new StringBuilder();
    }
    
    
    /*
     */
    public String makeClass() {

	//logger.debug("2... appendMakeBalloon CALLED: " + this.getMakeBalloon());
        StringBuilder sb = new StringBuilder();
        sb.append( (this.getPackageBalloon()).toString() );
        sb.append( (this.getClassDeclBalloon()).toString() );
        sb.append( (this.getAttributesBalloon()).toString() );
        sb.append( (this.getAccessorsBalloon()).toString() );

        if( this.getChildrenBalloon() != null ) {
            sb.append( (this.getChildrenBalloon()).toString() );
            sb.append( (this.getFindersBalloon()).toString() );
            sb.append( (this.getListersBalloon()).toString() );
            sb.append( (this.getAddersBalloon()).toString() );
            sb.append( (this.getRemoversBalloon()).toString() );
        }
        sb.append( (this.getRemoveAllBalloon()).toString() );
        //logger.debug("2... appendMakeBalloon CALLED: " + (getMakeBalloon()).toString());
        sb.append( (this.getMakeBalloon()).toString() );
        sb.append( (this.getClassCloseBalloon()).toString() );

        return sb.toString();
    }

    public void flush() {
    	
    	this.setPackageBalloon(null); 
		this.setAccessorsBalloon(null);
		this.setAddersBalloon(null);
		this.setAttributesBalloon(null);
		this.setChildrenBalloon(null);
		this.setClassCloseBalloon(null);
		this.setClassDeclBalloon(null);
		this.setFindersBalloon(null);
		this.setFindersBalloon(null);
		this.setListersBalloon(null);
		this.setRemoversBalloon(null);
		this.setRemoveAllBalloon(null);
		this.setMakeBalloon(null);
    }

    public void appendChildrenBalloon(StringBuilder buf) {

        if(null == getChildrenBalloon()) {
            setChildrenBalloon(new StringBuilder());
        }
        (getChildrenBalloon()).append(buf.toString());
    }

    public void appendFindersBalloon(StringBuilder buf) {

        if(null == getFindersBalloon()) {
            setFindersBalloon(new StringBuilder());
        }
        (getFindersBalloon()).append(buf.toString());
    }
    
    public void appendListersBalloon(StringBuilder buf) {

        if(null == getListersBalloon()) {
            setListersBalloon( new StringBuilder() );
        }
        (getListersBalloon()).append(buf.toString());
    }
    
    
    public void appendAddersBalloon(StringBuilder buf) {

        if(null == getAddersBalloon()) {
            setAddersBalloon(new StringBuilder());
        }
        (getAddersBalloon()).append(buf.toString());
    }

    public void appendRemoversBalloon(StringBuilder buf) {

        if(null == getRemoversBalloon()) {
            setRemoversBalloon(new StringBuilder());
        }
        (getRemoversBalloon()).append(buf.toString());
    }

    public void appendRemoveAllBalloon(StringBuilder buf) {
	
        if(null == getRemoveAllBalloon()) {
            setRemoveAllBalloon(new StringBuilder());
        }
        (getRemoveAllBalloon()).append(buf.toString());
    }
    
    public void appendMakeBalloon(StringBuilder buf) {
	
        if(null == getMakeBalloon()) {
            setMakeBalloon(new StringBuilder());
        }
        (getMakeBalloon()).append(buf.toString());
	//logger.debug("1... appendMakeBalloon CALLED: " + (getMakeBalloon()).toString());
    }
    
    public void setAttributes(Attributes atts) {
        attributes = atts;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public StringBuilder getPackageBalloon() {
        return packageBalloon;
    }

    public void setPackageBalloon(StringBuilder packageBallon) {
        this.packageBalloon = packageBallon;
    }

    public StringBuilder getClassDeclBalloon() {
        return classDeclBalloon;
    }

    public void setClassDeclBalloon(StringBuilder classDeclBallon) {
        this.classDeclBalloon = classDeclBallon;
    }

    public StringBuilder getAttributesBalloon() {
        return attributesBalloon;
    }

    public void setAttributesBalloon(StringBuilder attributesBallon) {
        this.attributesBalloon = attributesBallon;
    }

    public StringBuilder getAccessorsBalloon() {
        return accessorsBalloon;
    }

    public void setAccessorsBalloon(StringBuilder accessorsBallon) {
        this.accessorsBalloon = accessorsBallon;
    }

    public StringBuilder getChildrenBalloon() {
        return childrenBalloon;
    }

    public void setChildrenBalloon(StringBuilder childrenBallon) {
        this.childrenBalloon = childrenBallon;
    }
    
    
    public StringBuilder getFindersBalloon() {
        return findersBalloon;
    }
    public void setFindersBalloon(StringBuilder findersBallon) {
        this.findersBalloon = findersBallon;
    }
    
    
    public StringBuilder getListersBalloon() {
        return listersBalloon;
    }
    public void setListersBalloon(StringBuilder listersBallon) {
        this.listersBalloon = listersBallon;
    }
    
    
    
    // addersBalloon
    public StringBuilder getAddersBalloon() {
        return addersBalloon;
    }
    public void setAddersBalloon(StringBuilder addersBalloon) {
        this.addersBalloon = addersBalloon;
    }

    // removersBalloon
    public StringBuilder getRemoversBalloon() {
        return removersBalloon;
    }
    public void setRemoversBalloon(StringBuilder removersBalloon) {
        this.removersBalloon = removersBalloon;
    }
    
    // removeAllBalloon
    public StringBuilder getRemoveAllBalloon() {
	return this.removeAllBalloon;
    }
    public void setRemoveAllBalloon(StringBuilder removerAllBalloon) {
        this.removeAllBalloon = removerAllBalloon;
    }
    
    // makeBalloon
    public StringBuilder getMakeBalloon() {
	return this.makeBalloon;
    }
    public void setMakeBalloon(StringBuilder sb) {
	this.makeBalloon = sb;
    }

    // classCloseBalloon
    public StringBuilder getClassCloseBalloon() {
        return classCloseBalloon;
    }
    public void setClassCloseBalloon(StringBuilder sb) {
        this.classCloseBalloon = sb;
    }

}
