
package com.interrupt.bob;

import java.io.File;
import java.io.IOException;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.util.Util;
import com.greenfabric.find.Find;

/**
 * In all cases, 'base' attribute should be set. The uses of this class are to:
 *
 * find files with 'suffix' -		find()
 * find specific 'list' of files -	find(String args[])
 * find a specific file or directory -	find(String fileString)
 */
public class FindFiles {

	
	private Logger logger = Logger.getLogger(FindFiles.class); 
	
	private String base = null;
    private String suffix = null;

    public FindFiles() {
    }
    
    public void setBase(String bs) {
	base = bs;
    }
    public String getBase() {
	return base;
    }

    public void setSuffix(String sfx) {
	suffix = sfx;
    }
    public String getSuffix() {
	return suffix;
    }
    
    /* check that a list of files is valid
     */
    public static void listCheck(List flist) {
	
	Iterator iter = flist.iterator();
	File checking = null;
	String fname = null;
	while(iter.hasNext()) {
	    
	    fname = (String)iter.next();
	    checking = new File(fname);
	    if(!checking.exists()) {
		throw new RuntimeException("Invalid File[ "+ fname +" ]");
	    }
	}
    }
    
    /* find files with 'suffix'
     */
    public List find() {

	List fileList = new ArrayList();

	if(this.getBase() == null) {
	    throw new RuntimeException("set the 'base' attribute before executing");
	}
	if(this.getSuffix() == null) {
	    throw new RuntimeException("set the 'suffix' attribute before executing");
	}
	
	return this._subFind(new File(this.getBase()));
    }
    

    /* find specific 'list' of files
     */
    public List find(String args[]) {
    	
		List fileList = new ArrayList();
	
		if(this.getBase() == null) {
		    throw new RuntimeException("set the 'base' attribute before executing");
		}
		
		// i) file and ii) directory list
		Find myFind = new Find(this.getBase());
		for(int i=0; i < args.length; i++) {
		    
			// do not count empty Strings
			if( args[i].trim().length() == 0 ) { 
				continue;
			}
		    fileList.addAll(this.find(args[i].trim()));
		}
		
		return fileList;
    }

    /* find a specific file or directory
     */
    public List find(String fileString) {
		
		List fileList = new ArrayList();
		if(fileString == null || fileString == "") {
		    return fileList;
		}
		
		// is this an absolute path?
		if(fileString.startsWith(File.separator)) {
		    
		    File absoluteFile = new File(fileString);
		    if(!absoluteFile.isDirectory()) {
			fileList.add(fileString);
				return fileList;
		    }
		    return _subFind(absoluteFile);
		}
		
		// this is a partial path
		if(this.getBase() == null) {
		    throw new RuntimeException("set the 'base' attribute before executing");
		}
		
		
		String _base = this.getBase();
		
		if(!_base.endsWith(File.separator)) {
		    _base += File.separator;
		}
		
		if(fileString.startsWith( "."+File.separator)) { 
			fileString = fileString.substring(2); 
		}
		
		//logger.debug(">> find ["+_base+"] ["+fileString+"]");
		File partialFile = new File(_base,fileString);
		return _subFind(partialFile);
		
    }
    
    public List _subFind(File fileToFind) {
	    
		List fileList = new ArrayList();
		
		// i) file and ii) directory list
		Find myFind = new Find(fileToFind);
		myFind.setFindDirectories(false);
		
		String suffix = this.getSuffix();
		if(suffix == null) {
		    suffix = "";
		}
		if(suffix != "") {
		    if(suffix.startsWith(".")) {
			suffix = "\\" + suffix;
		    }
		    myFind.setPattern("/"+suffix+"/i");
		}
		else {
		    myFind.setPattern("/\\*?$/i");
		}
	
	
		// find files in directory
		try {
		    Iterator files = myFind.listRecursively();
		    while(files.hasNext()) {
		    	fileList.add((String)files.next());
		    }
		}
		catch(IOException e) {
		    e.printStackTrace();
		}
		
		return fileList;
    }
}


