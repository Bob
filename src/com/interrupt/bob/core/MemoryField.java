package com.interrupt.bob.core;

import com.interrupt.bob.core.GMemoryField;

public class MemoryField extends GMemoryField {
	
	
	private Object _value = null;
	
	public Object getValue(Object value) { 
		return this._value; 
	}
	public void setValue(Object value) { 
		
		this._value = value;
		super.setValue(value.toString());
	}
}
