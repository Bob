package com.interrupt.bob.core;

public class Memory extends GMemory {
	
	
	// ** ensure there can only be one named value in memory 
	// at a time 
	public void addMemoryField(com.interrupt.bob.core.IMemoryField addition) { 
		
		String mname = addition.getName();
		IMemoryField mfield = this.findMemoryFieldByName(mname); 
		if(mfield != null) { 
			this.removeMemoryFieldByName(mname);
		}
		super.addMemoryField(addition); 
	}
	
}
