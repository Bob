package com.interrupt.bob.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import com.interrupt.bob.FindFiles;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.handler.BobHandler;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.GHandlerField;
import com.interrupt.bob.handler.HandlerField;
import com.interrupt.bob.handler.IHandlerField;
import com.interrupt.bob.handler.QueueEvent;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;
import com.interrupt.callback.CallbackBroadcaster;
import com.interrupt.callback.CallbackEvent;
import com.interrupt.callback.CallbackHandler;

import com.interrupt.bob.handler.IDocumentHandler; 

import org.apache.log4j.Logger;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLReaderFactory;



/**
 * This class extends generated 'GQueue'
 * 
 * 1. There should only be 1 'IMemory'
 * 2. 'IDocumentHandlers' will be processsed in the 
 * 		order they were declared in the XML 
 * 
 * @author timothywashington
 *
 */
//public class Queue extends DocumentHandler implements com.interrupt.bob.DocumentHandler {
public class Queue extends GQueue implements CallbackBroadcaster, com.interrupt.bob.DocumentHandler {
    
	
	private final Logger logger = Logger.getLogger(Queue.class); 
	
	private String _nextFile = null;
    
	private List 	_listeners = null;
    private List	_handlerList = null;
    private Iterator	_handlersIter = null;
    private Iterator	_filelistIter = null;
    private XMLReader 	_reader = null;
    private Object 		_handlerInputValue = null; 
    

    // this is a poor attempt at controlflow 
    // TODO - find a better way 
    private boolean HandlerInputProcessed = false; 
    
    public Queue() {
		
    	
    	_handlerList = new ArrayList(); 
    	_listeners = new ArrayList(); 
    	
		// set XML reader
		System.setProperty("org.xml.sax.driver", "com.bluecast.xml.Piccolo"); 
		try {
		    this._reader = XMLReaderFactory.createXMLReader();
		}
		catch(SAXException e) {
		    e.printStackTrace();
		}
		
		// add a default Memory
		Memory defaultMemory = new Memory();
		this.setMemory(defaultMemory); 
		
    }
    
    
	public void setMemory(IMemory mem) { 
		
		this.removeAllMemory();
		this.addMemory(mem);
	}
    public IMemory getMemory() { 
    	
    	//logger.debug("Mem size["+ this.allMemory().size() +"]"); 
    	return (IMemory)this.allMemory().get(0); 
    }
    
    
    public void addDocumentHandler(com.interrupt.bob.handler.IDocumentHandler addition) {
    	
    	((com.interrupt.bob.handler.DocumentHandler)addition).addListener(this);
    	super.addDocumentHandler(addition); 
        
    }
    
    
    /* ENTRY POINT
     * 
     * The algorithm is to process a list of files within each Handler. 
     * The result of each process get handed off to the next Handler in the chain. 
     */
    public void execute() throws ProcessorException { 
    	
    	logger.debug( "... Queue.execute()");
		this.execute(this); 
    	
    }
    public void execute(IQueue iqueue) throws ProcessorException {
    	
    	logger.debug( "... Queue.execute(IQueue iqueue)");
		_handlerList = iqueue.allDocumentHandler();
    	_handlersIter = _handlerList.iterator();
		this.nextHandler();
		
    }
    
    
    public void nextHandler() throws ProcessorException { 
    	
    	
    	logger.debug( ">>> Queue.nextHandler()");
		if( _handlersIter.hasNext() ) { 
    		
			
			//** next handler
		    IDocumentHandler handler = (IDocumentHandler)_handlersIter.next(); 
		    String inputFiles = handler.getFiles(); 
		    
		    logger.debug("NEXT Handler["+ handler.getTagName() +"] > class["+ handler.getClass() +"]"); 
			
		    
		    //** get handler input value
		    boolean interpretable = Util.isInterpretable(inputFiles); 
		    if(interpretable) { 
		    	
		    	String interpretableName = Util.getInterpretableName(inputFiles);
		    	logger.debug("inputFiles is Interpretable > Name["+interpretableName+"]");
		    	
		    	MemoryField inputField = (MemoryField)this.getMemory().findMemoryFieldByName(interpretableName);
		    	_handlerInputValue = inputField.getValue(inputField); // *** kind of a dangerous way to implement this 
		    	
		    	try { 
			    	logger.debug("["+interpretableName+"] > _handlerInputValue from MEMORY["+_handlerInputValue+"] > bytes to read["+ 
			    		((InputStream)_handlerInputValue).available() +"]"); 
		    	}
		    	catch(IOException e) { 
		    		throw new ProcessorException(e); 
		    	}
		    	
		    }
		    else {
		    	
		    	_handlerInputValue = inputFiles; 
		    	logger.debug("_handlerInputValue from 'inputFiles'["+_handlerInputValue+"]"); 
		    	
		    }
		    
		    
		    //** set <handlerField/>
		    Iterator iterator = handler.allHandlerField().iterator(); 
		    IHandlerField eachHandlerField = null; 
		    
		    String fieldName = null;
		    while(iterator.hasNext()) { 
		    	
		    	// get handler field name 
		    	eachHandlerField = (IHandlerField)iterator.next(); 
		    	fieldName = eachHandlerField.getName(); 
		    	
		    	logger.debug("...each IHandlerField["+ eachHandlerField +"]"); 
		    	
		    	
		    	//** deal with NAME  
		    	Class handlerFieldClass = eachHandlerField.getClass();
		    	Field handlerNameField = null; 
		    	
		    	
		    	// get handler value and set handler instance field 
		    	boolean handlerInterpretable = Util.isInterpretable(fieldName); 
		    	if(handlerInterpretable) { 
		    		
		    		String handlerInterpretableName = Util.getInterpretableName(fieldName); 
		    		fieldName = handlerInterpretableName; 
		    		
		    		MemoryField mfield = (MemoryField)((Memory)this.getMemory()).findMemoryFieldByName(fieldName); 
		    		fieldName = (String)mfield.getValue(mfield); 
		    		
		    	}
		    	
		    	//** deal with VALUE 
		    	Object handlerValue = null; 
		    	String handlerValueString = eachHandlerField.getValue(); // *** kind of a dangerous way to implement this 
		    	if(Util.isInterpretable(handlerValueString)) { 
		    		
		    		String handlerValueString_n = Util.getInterpretableName(handlerValueString);
		    		MemoryField mfield = (MemoryField)((Memory)this.getMemory()).findMemoryFieldByName(handlerValueString_n); 
		    		handlerValue = mfield.getValue(mfield); 
		    		
		    		logger.debug("Handler Interpretable Value["+handlerValueString+"] > value from memory["+handlerValue+"]");
		    	}
		    	//else { 
		    	//	handlerValue = inputField.getValue(inputField); // *** kind of a dangerous way to implement this 
		    	//}
		    	//logger.debug("Handler Field value["+handlerValue+"]"); 
		    	
		    	//try { 
		    		
		    		logger.debug("...eachHandlerField["+eachHandlerField.getTagName()+"] >> NAME["+fieldName
		    			+"] to VALUE["+handlerValue+"]"); 
			    	//handlerField.set(eachHandlerField, handlerValue); 
		    		eachHandlerField.setProperty("name", fieldName); 
		    		eachHandlerField.setProperty("value", handlerValue.toString()); 
			    	
		    		logger.debug("eachHandlerField AFTER population[ "+ eachHandlerField.toXML(false) +" ]"); 
				    
		    		
				    //** FUBARRRED 
				    if(handler instanceof DocumentHandler) { 
				    	((DocumentHandler)handler).setMessage(handlerValue); 
				    }
				    //** END FUBAR 
				    
				    
		    	//}
		    	//catch(IllegalAccessException e) { 
		    	//	e.printStackTrace(); 
		    	//	throw new ProcessorException(e.getMessage(),e);
		    	//}
		    	
		    }
		    logger.debug(""); 
		    logger.debug("Handler AFTER field population[ "+ handler.toXML(false) +" ]"); 
		    
		    
		    // set the file list iterator if inputValue
		    // is a String 
		    if( _handlerInputValue instanceof String ) { 
		    	
			    Properties properties = System.getProperties(); 
				FindFiles ffiles = new FindFiles(); 
			    ffiles.setBase(properties.getProperty(Util.BASE)); 
				ffiles.setSuffix(properties.getProperty(Util.END)); 
				
				List fileList = ffiles.find( ((String)_handlerInputValue).split("\\s") ); 
			    
				_filelistIter = fileList.iterator(); 
			    
		    }
		    
		    
		    //** configure XMLReader 
		    logger.debug("");
	        logger.debug( "... Handler["+handler.getClass().getName()+"] / InputFiles["+_handlerInputValue+"]" );
		    
		    try {
				_reader.setProperty("http://xml.org/sax/properties/lexical-handler", handler); 
				_reader.setProperty("http://xml.org/sax/properties/declaration-handler", handler); 
				
				_reader.setContentHandler((ContentHandler)handler);
		    }
		    catch(SAXException e) {
				e.printStackTrace();
		    }
		    
		    // there can be more than 1 type of input, so 
		    // we should decouple how we process the input from 
		    // from the fact that each handler has to process some input

		    this.HandlerInputProcessed = false; 
	    	this.nextHandlerInput(); 
		    
    	}
	    else { 
		    
		    logger.debug(">>> Queue > finished executing"); 
		    QueueEvent qevent = new QueueEvent(); 
		    qevent.setMessage(this);
		    this.notifyListeners(qevent); 
		}
    	
    }
    
    /* this method decouples processing handlerInput from the actual
     * type of handler input 
     */
    public void nextHandlerInput() throws ProcessorException { 
    	
    	
    	logger.debug( "... Queue.nextHandlerInput() / _handlerInputValue["+_handlerInputValue+"] / Class["+_handlerInputValue.getClass()+"]");
    	
    	
	    //** get INPUT (file list/inputStream)
	    if( _handlerInputValue instanceof String ) { 
	    	
	    	this._nextFile(); 
	    }
	    else if( _handlerInputValue instanceof java.io.InputStream && !this.HandlerInputProcessed) { 
	    	
	    	this.HandlerInputProcessed = true; 
	    	this._processXML(_handlerInputValue); 
	    }
	    else { 
	    	
	    	this.nextHandler();
	    }
	    
    }
    public void _nextFile() throws ProcessorException {
		
    	logger.debug( "Queue._nextFile() CALLED");
    	
    	this.HandlerInputProcessed = true; 
    	
		// set the next file
		if(_filelistIter.hasNext()) {
			
			_nextFile = (String)_filelistIter.next(); 
			logger.debug( "... PROCESSING ["+ _nextFile +"]" );
		    
			File fileToParse = new File(_nextFile);
		    this._processXML(fileToParse.getAbsolutePath()); 
		    
		}
		else {
		    this.nextHandler();
		}
		
    }
    
    
    /* the core method that will start XML processing from 
     * a file string or an InputStream 
     */
    public void _processXML(Object input) throws ProcessorException { 
    	
    	
    	logger.debug( "Queue._processXML("+ input +") CALLED");
    	
    	//logger.debug( "[unused] versus fileToParse ["+ _nextFile +"]");
    	//File fileToParse = new File(_nextFile);
	    try {	
	    	
	    	if( input instanceof String ) { 
	    		_reader.parse((String)input); 
	    	}
	    	
	    	//if( _nextFile instanceof String ) { 
	    	//	_reader.parse(_nextFile); 
	    	//}
	    	else if( input instanceof java.io.InputStream ) { 
	    		
	    		logger.debug("_reader.parse > InputStream["+input+"] > bytes to read["+ 
	    			((InputStream)input).available() +"]"); 
	    		_reader.parse(new InputSource((java.io.InputStream)input)); 
	    	}
	    }
	    catch(SAXException e) {
			throw new ProcessorException( e.getMessage(), e);
	    }
	    catch(IOException e) {
			throw new ProcessorException( e.getMessage(), e);
	    }
	    
    }
    
    
    /* CallbackBroadcaster methods
     */
    public void addListener(com.interrupt.bob.DocumentHandler handler) {
    	_listeners.add(handler);
    }
    public List allListener() { 
    	return new ArrayList(_listeners); 
    }
    public void notifyListeners(CallbackEvent event) {
		
		Iterator iter = _listeners.iterator();
		com.interrupt.bob.DocumentHandler next = null;
		while(iter.hasNext()) {
		    next = (com.interrupt.bob.DocumentHandler)iter.next();
		    next.endHandle(event);
		}
		
    }
    public Object removeListener(com.interrupt.bob.DocumentHandler handler) {
    	
		int index = _listeners.indexOf(handler);
		return _listeners.remove(index);
    }
    

    /* 'com.interrupt.bob.DocumentHandler' method
     */
    public void endHandle(CallbackEvent event) {
    	
    	logger.debug("... Queue.endHandle()");
    	try {
    		this.nextHandlerInput();
    	}
    	catch(ProcessorException e) {
    		e.printStackTrace();
    	}
    	
    }
    
    
    
    public static void main( String args[] ) { 
    	
    	IBob bob = new Bob();
    	IBob resultBob = null; 
    	try { 
    		
    		resultBob = bob.load( new FileInputStream("xml/system.xml"), "xml/system.xml" ); 
    	}
    	catch( java.io.FileNotFoundException e ) { 
    		e.printStackTrace(); 
    	}
    	
    	//Syste( "Result XML["+ resultBob.toXML() +"]" ); 
    	
    }
	
	
}


