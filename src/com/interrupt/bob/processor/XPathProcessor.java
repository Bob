package com.interrupt.bob.processor; 


import com.interrupt.bob.processor.cc.DepthFirstVisitor;
import com.interrupt.cc.xpath.parser.Parser; 
import com.interrupt.cc.xpath.parser.ParserException; 
import com.interrupt.cc.xpath.lexer.Lexer; 
import com.interrupt.cc.xpath.lexer.LexerException; 
import com.interrupt.cc.xpath.node.*; 

import java.io.InputStreamReader; 
import java.io.PushbackReader; 
import java.io.InputStream; 


public class XPathProcessor { 
	
	public static void main(String args[]) { 
		
		InputStream inputStream = System.in; 
		PushbackReader pbreader = 
			new PushbackReader( new InputStreamReader(inputStream), 1024 ); 
		DepthFirstVisitor rdvisitor = new DepthFirstVisitor(); 
		
		System.out.println("xpath > start"); 
		try { 
			
			//** parse the syntax and provide the MODEL 
			Lexer lexer = new Lexer(pbreader); 
			Parser parser = new Parser(lexer); 
			
			System.out.println("xpath > parsing input"); 
            
			// parse the input 
			Start tree = parser.parse(); 
			
			System.out.println(""); 
			System.out.println("xpath > running analyser"); 
            
			// apply the Visitor 
			tree.apply(rdvisitor); 
			
			System.out.println("xpath > finished"); 
            
		}
		catch(java.io.IOException e) { 
			System.out.println("Error: " + e.getMessage());
		}
		catch(LexerException e) { 
			System.out.println("Error: " + e.getMessage());
		}
		catch(ParserException e) { 
			System.out.println("Error: " + e.getMessage());
		}
		catch(Exception e) { 
			System.out.println("Error: " + e.getMessage()); 
		}
		
	}
	
}
