package com.interrupt.bob.processor;

public class GenericEquation {
	
	private String lhs = null; 
	private String rhs = null;
	private String arbiter = null; 
	
	public String getLhs() {
		return lhs;
	}
	public void setLhs(String lhs) {
		this.lhs = lhs;
	}
	public String getRhs() {
		return rhs;
	}
	public void setRhs(String rhs) {
		this.rhs = rhs;
	}
	public String getArbiter() {
		return arbiter;
	}
	public void setArbiter(String arbiter) {
		this.arbiter = arbiter;
	} 
	
}
