package com.interrupt.bob.processor;

import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.io.File;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import java.io.IOException;

import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.util.Util;
import com.interrupt.callback.CallbackEvent;
import com.interrupt.bob.handler.DefinitionEvent;

public class FileProcessor {
    
    
    private XMLReader reader = null;
    private DefinitionHandler handler = null;
    private DocumentHandler dhandler = null;

    /* List files - the java.util.List in which to put file names
     * String uri - uri from the home base
     */
    public void getFileList(List files, String uri) throws ProcessorException {
	
	String home = System.getProperty(Util.HOME);
	File file = new File(home, uri);
	//List files = new ArrayList();
	
	if( file.isDirectory() ) {
	    
	    String oldHome = System.getProperty(Util.HOME);
	    System.setProperty(Util.HOME, file.toString());
	    File[] children = file.listFiles();
	    for(int i=0; i < children.length; i++) {
		
		//logger.debug( "2 >> " + children[i] );
		this.getFileList( files, children[i].getName() );
	    }

	    System.setProperty(Util.HOME, oldHome);
	}
	else {
	    files.add(file);
	}
	
    }
    
}


