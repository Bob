package com.interrupt.bob.processor.cc;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.cc.xpath.node.ANameNodetest;
import com.interrupt.cc.xpath.node.Node;

public abstract class NodeHandler {
	
	
	protected final static String COMPARATOR_EQUALS = "="; 
	
	private NodeHandler parent = null; 
	private NodeHandler handler = null; 
	protected Node node = null; 
	
	public NodeHandler(Node node) { 
		
		this.node = node; 
	}
	public Node getNode() { 
		return node; 
	}
	public void setNode(Node node) { 
		this.node = node; 
	}
	public NodeHandler getParent() {
		return parent;
	}
	public void setParent(NodeHandler parent) {
		this.parent = parent;
	}
	public NodeHandler getHandler() {
		return handler;
	}
	public void setHandler(NodeHandler handler) {
		this.handler = handler;
	}
	
	
	/**
	 * Each handler implements the way it filters out an XDM 
	 */
	public abstract List handle(List xdmlist);
	
	/**
	 * iterate through the handler chain and apply all each filter 
	 */
	public List apply(List xdmlist) {
		
		NodeHandler nexth = this; 
		while(nexth != null) { 
			
			xdmlist = nexth.handle(xdmlist); 
			nexth = nexth.getHandler(); 
		}
		return xdmlist; 
	}
	
	/**
	 * add a handler to the end of the current handler chain 
	 */
	public void chainHandler(NodeHandler nh) { 
		
		NodeHandler nexth = this.getHandler(); 
		if(nexth == null) { 
			
			this.setHandler(nh); 
		}
		else { 
			
			while(nexth != null) { 
				
				if(nexth.getHandler() == null) { 
					nexth.setHandler(nh); 
					break; 
				}
				nexth = nexth.getHandler(); 
			}
		}
	}
	public IBob doit(IBob xdm, Map dynamicContext) { 
		
		/*NodeHandler nh1 = (NodeHandler)dynamicContext.get("nh1"); 
		NodeHandler nh2 = (NodeHandler)dynamicContext.get("nh2"); 
		NodeHandler nh3 = (NodeHandler)dynamicContext.get("nh3"); 
		IBob resultXdm = xdm; 
		
		//** 
		if((nh1 instanceof AForwardAxisstepPartHandler) && (nh3 instanceof ANameNodetestHandler)) { 
			
			resultXdm = new Bob(); 
			
			// find nodes with 'name' in xdm list 
			Node node = nh1.getNode(); 
			String nodeName = node.toString(); 
			nodeName = nodeName.toString().trim(); 
			String tagName = null; 
			
			ListIterator literator = xdm.allChildren().listIterator(); 
			IBob nextXdm = null; 
			while(literator.hasNext()) { 
			 	
			 	nextXdm = (IBob)literator.next(); 
			 	tagName = nextXdm.getTagName().trim(); 
			 	if(nodeName.equals(tagName)) { 
			 		resultXdm.addChild(nextXdm); 
			 	}
			}
			
		}
		
		
		if((nh1 instanceof APredicateListHandler) && (nh2 instanceof APredicateHandler)) { 
			
		}
		
		if((nh1 instanceof BracketHandler) && (nh2 instanceof AComparisonexprHandler)) { 
			
			
			//nh2.apply(resultXdm, dynamicContext); 
			
		}
		
		if((nh1 instanceof AAbbrevforwardForwardstepHandler) && (nh2 instanceof AEqGeneralcompHandler) && (nh3 instanceof AStringLiteralHandler)) { 
			
			String lhs = nh1.getNode().toString().trim(); 
			String comparator = nh2.getNode().toString().trim(); 
			String rhs = nh3.getNode().toString().trim(); 
			
		}
		
		return resultXdm; 
		*/
		return null; 
	}
	
}
