package com.interrupt.bob.processor.cc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.cc.xpath.node.Node;

public class PredicateHandler extends NodeHandler {
	
	public PredicateHandler(Node node) { 
		super(node); 
	}
	
	public List handle(List xdmlist) {
		return null;
	}
	
	
	public List apply(List xdmlist) {
		
		NodeHandler nexth = this; 
		
		String name = null; 
		String comparator = null; 
		String value = null; 
		
		List resultList = new ArrayList(); 
		List currentList = new ArrayList(); 
		
		while(nexth != null) { 
			
			//**
			if(nexth.getClass() == com.interrupt.bob.processor.cc.LetterHandler.class) { 
				name = nexth.getNode().toString().trim(); 
			}
			else if(nexth.getClass() == com.interrupt.bob.processor.cc.EqualsHandler.class) { 
				comparator = nexth.getNode().toString().trim(); 
			}
			else if(nexth.getClass() == com.interrupt.bob.processor.cc.StringLiteralHandler.class) { 
				
				value = nexth.getNode().toString(); 
				
				if(value == null) { 
					value = ""; 
				}
				else { 
					value = value.trim(); 
					value = value.replaceAll("'",""); 
					value = value.replaceAll("\"",""); 
				}
			}
			else if( nexth.getClass() == com.interrupt.bob.processor.cc.OrHandler.class ) { 
				currentList.add("OR"); 
			}
			else if( nexth.getClass() == com.interrupt.bob.processor.cc.AndHandler.class ) { 
				currentList.add("AND"); 
			}
			
			
			//**
			if((name != null) && (comparator != null) && (value != null)) { 
				
				currentList.add( zzz(xdmlist, name, comparator, value) ); 
				
				name = null; 
				comparator = null; 
				value = null; 
				
			}
			
			nexth = nexth.getHandler(); 
		}
		
		//** now filter the list through and / or 
		Iterator xiter = currentList.iterator(); 
		Object eachx = null; 
		List prevList = new ArrayList(); 
		List nextList = new ArrayList(); 
		String pcompare = "OR"; 
		while(xiter.hasNext()) { 
			
			eachx = xiter.next(); 
			if( eachx instanceof String ) { 
				
				if( ((String)eachx).equals("OR") ) { 
					pcompare = "OR"; 
				}
				else { 
					pcompare = "AND"; 
				}
			}
			else if( eachx instanceof List ) { 
				
				nextList = (List)eachx; 
				if(pcompare.equals("AND")) { 
					
					if( (!nextList.isEmpty()) && (!prevList.isEmpty())) { 
						//resultList.clear(); 
						//resultList.addAll(currentList); 
						resultList = new ArrayList(nextList); 
					}
					else { 
						resultList = new ArrayList(); 
					}
				}
				else { 
					
					if( (!nextList.isEmpty()) || (!prevList.isEmpty())) { 
						
						if(!nextList.isEmpty()) { 
							
							//resultList.clear(); 
							//resultList.addAll(currentList);
							resultList = new ArrayList(nextList); 
						}
						else if( !prevList.isEmpty() ) { 
							
							//resultList.clear(); 
							//resultList.addAll(prevList.); 
							resultList = new ArrayList(prevList); 
						}
					}
					else { 
						resultList = new ArrayList(); 
					}
					
				}
				prevList = resultList; 
				
			}
			
		}
		
		return resultList; 
	}
	
	
	private List zzz(List xdmlist, String name, String comparator, String value) { 
		
		List resultList = new ArrayList(); 
		
		Iterator iter = xdmlist.iterator(); 
		IBob eachb = null; 
		while(iter.hasNext()) { 
			
			eachb = (IBob)iter.next(); 
			String avalue = eachb.getAttributeValue(name); 
			if(NodeHandler.COMPARATOR_EQUALS.equals(comparator)) { 
				
				if(avalue == null) 
					avalue = ""; 
				
				if( (avalue.length() == 0) && (value.length() == 0) ) { 
					resultList.add(eachb); 
				}
				else if (avalue.trim().equals(value.trim())) { 
					resultList.add(eachb); 
				}
			}
		}
		return resultList; 
	}
	
}

