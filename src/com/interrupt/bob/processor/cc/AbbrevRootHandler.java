package com.interrupt.bob.processor.cc;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.cc.xpath.node.ANameNodetest;
import com.interrupt.cc.xpath.node.Node;
import com.interrupt.cc.xpath.node.TAbbrevRoot;

public class AbbrevRootHandler extends NodeHandler {
	
	
	private TAbbrevRoot node = null;
	public AbbrevRootHandler(Node node) { 
		super(node); 
	}
	
	public List handle(List xdmlist) { 
		
		List result = new ArrayList(); 
		Iterator iter = xdmlist.iterator(); 
		IBob eachb = null; 
		while(iter.hasNext()) { 
			
			eachb = (IBob)iter.next(); 
			result.add(eachb); 
		}
		return result; 
	}
	
}
