package com.interrupt.bob.processor.cc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.cc.xpath.node.Node;

public class LetterHandler extends NodeHandler {
	
	public LetterHandler(Node node) { 
		super(node); 
	}
	
	public List handle(List xdmlist) {
		
		List resultx = new ArrayList(); 
		String comparator = getNode().toString().trim(); 
		
		Iterator iter = xdmlist.iterator(); 
		IBob xdm = null; 
		while(iter.hasNext()) { 	//** go through xdmlist
			
			xdm = (IBob)iter.next(); 
			if(xdm.getTagName().equals( comparator )) { 
				resultx.add(xdm); 
			}
			/*IBob eachx = null; 
			List children = xdm.allChildren(); 
			ListIterator liter = children.listIterator(); 
			while(liter.hasNext()) { 	//** loop through each child 
				
				eachx = (IBob)liter.next(); 
				if(eachx.getTagName().equals( comparator )) { 
					resultx.add(eachx); 
				}
			}
			*/
		}
		
		return resultx; 
	}
	
}
