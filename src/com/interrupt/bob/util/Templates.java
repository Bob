package com.interrupt.bob.util;

public class Templates {

    public static final String PACKAGE = "package.vm";
    public static final String CLASSDECL = "classDecl.vm";
    public static final String ATTRDECL = "attrDecl.vm";
    public static final String GETTER = "getter.vm";
    public static final String SETTER = "setter.vm";
    public static final String CHILD = "child.vm";
    public static final String FINDER = "finder.vm";
    public static final String LISTER = "lister.vm";
    public static final String ADDER = "adder.vm";
    public static final String REMOVER = "remover.vm";
    public static final String REMOVEALL = "removeAll.vm";
    public static final String MAKE = "make.vm";

    public static final String IADDER = "Iadder.vm";
    public static final String ICLASSDECL = "IclassDecl.vm";
    public static final String IFINDER = "Ifinder.vm";
    public static final String ILISTER = "Ilister.vm";
    public static final String IGETTER = "Igetter.vm";
    public static final String IREMOVER = "Iremover.vm";
    public static final String IREMOVEALL = "IremoveAll.vm";
    public static final String ISETTER = "Isetter.vm";
    
    public static final String GENCHILD_NEW = "_genchildren_new.vm";
    public static final String GENCHILD_ALL = "_genchildren_all.vm";
}
