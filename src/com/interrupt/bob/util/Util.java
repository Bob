
package com.interrupt.bob.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;

public class Util {
	
    public static final String HOME = "bob.home";
    public static final String BASE = "bob.base";
    public static final String END = "bob.end";
    public static final String GEN = "bob.gen";
    
    public static final String DEF = "bob.def";
    public static final String SYS = "bob.sys";
    
    public static boolean isInterpretable( String value ) { 
    	
    	if( value == null ) { 
    		return false; 
    	}
    	
    	
    	boolean starter = value.trim().startsWith("${");
    	boolean ending = value.trim().endsWith("}");
    	if( starter && ending ) { 
    		return true; 
    	}
    	return false; 
    }
    
    public static String getInterpretableName(String name) { 
    	
    	//String sansPrefix = name.substring(name.indexOf("${")); 
    	String sansPrefix = name.substring(2); 
    	String sansSuffix = sansPrefix.substring(0, sansPrefix.indexOf("}"));
    	return sansSuffix; 
    }
    
    
    public static void evalReflectedAdd(IBob parent, String tagName, Class methodDefinitionParams[], IBob methParams[]) 
    	throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, Throwable { 
    	
    	
    	//parent.add{child}(created);
    	Class pclass = parent.getClass();
    	String methName = "add" + StringUtil.upperFirstLetter(tagName);
    	Method setMeth = null;
    	
    	try {
    	    
    		setMeth = pclass.getMethod( methName, methodDefinitionParams );
    	    setMeth.invoke( parent, methParams );
    	}
    	catch(NoSuchMethodException e) {
    	    e.printStackTrace(); 
    	    throw e; 
    	}
    	catch(IllegalAccessException e) {
    	    e.printStackTrace(); 
    	    throw e; 
    	}
    	catch(InvocationTargetException e) {
    	    e.printStackTrace(); 
    	    throw e; 
    	}
    	
    }
    
    
	public static IBob loadBobFromXML(String xmlString) { 
		
		Logger logger = Logger.getLogger(com.interrupt.bob.util.Util.class); 
		logger.debug("com.interrupt.bob.util.Util.loadBobFromXML:: ["+ xmlString +"]"); 
		
		//** load a Bob object 
		InputStream istream = null; 
		try { 
			istream = new ByteArrayInputStream(xmlString.getBytes("UTF-8")); 
		}
		catch(UnsupportedEncodingException e) { 
			throw new RuntimeException(e); 
		}
		
	    IBob bob = Bob.loadS(istream, System.getProperty(Util.DEF)); 
		logger.debug("com.interrupt.bob.util.Util.loadBobFromXML:: Loaded Bob from XML [" + bob.toXML(false) + "]"); 
		
		return bob; 
	}
    
}


