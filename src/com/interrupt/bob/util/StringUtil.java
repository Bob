package com.interrupt.bob.util;

public class StringUtil {

    public static String upperFirstLetter(String word) {

        String className = word.trim();

        String firstLetter = className.substring(0,1);
        String restOf = className.substring(1);

        StringBuilder sb = new StringBuilder(firstLetter.toUpperCase());
        sb.append(restOf);

        return sb.toString();
    }
    
    public static String namespaceToPackage(String word) {
    	
		String pkg = word.trim();
		pkg = pkg.replace('/','.');
		
		if(pkg.length() <= 0) {
		    return pkg;
		}
		
		int plength = pkg.length() - 1;
		char lastchar = pkg.charAt(plength);
		if(lastchar == '.') {
		    pkg = pkg.substring(0, plength);
		}
	
		return pkg;
    }
    

    public static String packageToNamespace(String word) {
    	
		String nspace = word.trim();
		nspace = nspace.replace('.','/');
		
		if(nspace.length() <= 0) {
		    return nspace;
		}
		
		int plength = nspace.length() - 1;
		char lastchar = nspace.charAt(plength);
		if(lastchar == '/') {
		    nspace = nspace.substring(0, plength);
		}
	
		return nspace;
    }
    
}


