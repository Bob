
package com.interrupt.bob;

import java.util.Map;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;

public class Attributes extends AttributesImpl {
    
	
	private Logger logger = Logger.getLogger(Attributes.class); 
	
	public static String URI = "uri";
    public static String LOCALNAME = "localname";
    public static String QNAME = "qname";
    public static String TYPE = "type";
    public static String VALUE = "value";
    
    private HashMap _nameMap = null;

    public Attributes() {
	_nameMap = new HashMap();
    }
    public Attributes(org.xml.sax.Attributes atts) {
	
	super(atts);
	_nameMap = new HashMap();
	
    }
    
    public void setAttributes(org.xml.sax.Attributes atts) {
	
	String lname = null;
	String qname = null;
	for(int i=0; i< atts.getLength(); i++) {
	    
	    lname = atts.getLocalName(i);
	    String converted = this.genJavaName(lname);
	    this.setJNameMapping(converted,lname);
	}
	//logger.debug("Zzz >>> lname["+lname+"] > qname["+qname+"] > " + _nameMap.toString());
	super.setAttributes(atts);
    }
    
    /* gen java name
     */
    public String genJavaName(String aname) {
	return aname.replaceAll("-","");
    }
    
    /* map accessors
     */
    public void setMap(HashMap map) {
	_nameMap = map;
    }
    public Map getMap() {
	return _nameMap;
    }
    
    /* MAPPING
     * name - javaName
     * value - xml attribute
     */
    public void setJNameMapping(String name, String value) {
	_nameMap.put(name,value);
    }
    public String getJNameMapping(String name) {
	return (String)_nameMap.get(name);
    }
    
    
    /* convert 
     */
    public void convertXMLToJava(String name, String value) {
	
	for(int i=0; i < this.getLength(); i++) {
	    
	    String each = super.getLocalName(i);
	    if(each.equals(value)) {
		super.setLocalName(i,name);
		super.setQName(i,name);
	    }
	}
    }
    public void convertJavaToXML(String name, String value) {
	
	for(int i=0; i < this.getLength(); i++) {
	    
	    String each = super.getLocalName(i);
	    if(each.equals(name)) {
		super.setLocalName(i,value);
		super.setQName(i,value);
	    }
	}
    }
    
    
    /* extend accessors
     */
    public String getLocalNameJ(int i) {
	String lname = super.getLocalName(i);
	return lname.replaceAll("-","");
    }
    public String getQNameJ(int i) {
	String qname = super.getQName(i);
	return qname.replaceAll("-","");
    }
    
    /*public void setLocalNameJ(int i, String name) {
	super.setLocalName(i,name);
    }
    public void setQNameJ(int i, String name) {
	super.setQName(i,name);
    }
    public void setValueJ(int i, String name) {
	super.setValue(i,name);
    }
    */
}


