package com.interrupt.bob.generator;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.Template;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.xml.sax.Attributes;
import java.io.StringWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.util.StringUtil;
import com.interrupt.bob.util.Templates;

public class ClassGenerator implements IGenerator {

    private static ClassGenerator instance = null;

    private ClassGenerator() {
    }

    public static IGenerator getInstance() {
        if(instance == null) {
            instance = new ClassGenerator();
        }
        return instance;
    }

    public void mergeTemplate(VelocityContext context, String tplName, StringBuilder sb) {
	
		Template tpl = null;
		java.io.InputStream istream = null; 
		try {
		    //Velocity.init("velocity.properties");
		    ClassLoader cl = ClassLoader.getSystemClassLoader();
		    istream = cl.getResourceAsStream("velocity.properties");
	    
		    java.util.Properties props = new java.util.Properties();
		    props.load(istream);
	    
		    Velocity.init(props);
		    tpl = Velocity.getTemplate(tplName);

		    StringWriter swriter = new StringWriter();
    
		    if(tpl != null) {
				tpl.merge(context, swriter);
		    }
		    sb.append(swriter.toString());
		}
		catch(ResourceNotFoundException e) {
		    e.printStackTrace();
		}
		catch(ParseErrorException e) {
		    e.printStackTrace();
		}
		catch(Exception e) {
		    e.printStackTrace();
		}
		finally { 
		
			try { 
				istream.close(); 
			}
			catch( IOException e ) { 
				e.printStackTrace(); 
			}
			
		}
		
		
    }
    
    //public void appendClassName(String visibility, String clazz,
    //    String clazzName, StringBuilder sb) {
    public void appendClassName(IBob tagdef, String visibility, String clazz,
        String clazzName, StringBuilder sb) {
	
	String _new_gen = this._genNewChildren(tagdef);
	//String _all_gen = this._genAllChildren(tagdef);
	String _attArray = _genAttributeArray(tagdef);
	
	VelocityContext context = new VelocityContext();
	context.put("visibility", visibility);
	context.put("clazz", clazz);
	context.put("clazzName", clazzName);
	context.put("_new_genchildren", _new_gen);
	context.put("attArray", _attArray);
	context.put("tag", tagdef.getTagName());
	context.put("namespace", tagdef.getNamespace());
	
	// ** complete HACK to get interface name
	String iname = clazzName.trim();
	iname = iname.substring(1);
	iname = "I" + iname;
	context.put("interface_", iname);

	mergeTemplate(context, Templates.CLASSDECL, sb);
	Template tpl = null;
    }
    
    public String _genNewChildren(IBob tagdef) {
	
	IBob next = null;
	Iterator iter = (tagdef.allChildren()).iterator();
	StringBuilder sbuffer = new StringBuilder();
	String tname = null;
	VelocityContext context = new VelocityContext();
	
	while(iter.hasNext()) {
	    
	    next = (IBob)iter.next();
	    tname = next.getTagName();
	    
	    context.put("tag",tname);
	    mergeTemplate(context, Templates.GENCHILD_NEW, sbuffer);
	}
	
	return sbuffer.toString();
    }
    public String _genAttributeArray(IBob tagDef) {
	
	//{"elem1","elem2","elem3"}
	StringBuilder sbuffer = new StringBuilder();
	//sbuffer.append("{\"");
	sbuffer.append("{");
	Attributes attrs = tagDef.getAttributes();
	for(int i=0; i < attrs.getLength(); i++) {

	    if( i == 0 ) {
		sbuffer.append( "\"" );
	    }
	    
	    sbuffer.append( attrs.getLocalName(i) );
	    if((i + 1) == attrs.getLength()) {
		sbuffer.append("\"");
	    }
	    else {
		sbuffer.append("\",\"");
	    }
	}
	//sbuffer.append("\"}");
	sbuffer.append("}");
	return sbuffer.toString();
    }
    /*public String _genAllChildren(IBob tagdef) {
	
	IBob next = null;
	Iterator iter = (tagdef.allChildren()).iterator();
	StringBuilder sbuffer = new StringBuilder();
	String tname = null;
	VelocityContext context = new VelocityContext();
	
	while(iter.hasNext()) {
	    
	    next = (IBob)iter.next();
	    tname = next.getTagName();
	    
	    context.put("tag",tname);
	    mergeTemplate(context, Templates.GENCHILD_ALL, sbuffer);
	}
	
	return sbuffer.toString();
    }
    */
    
    public void appendAttribute(String attName, StringBuilder sb) {
	VelocityContext context = new VelocityContext();
	context.put("visibility", "private");
	context.put("attName", attName);
	mergeTemplate(context, Templates.ATTRDECL, sb);
    }

    public void appendAttributeAccessors(String attName, StringBuilder sb) {

	VelocityContext context = new VelocityContext();
	context.put("attrUpper", StringUtil.upperFirstLetter(attName));
	context.put("attr", attName);
	context.put("in", "_in");
	mergeTemplate(context, Templates.GETTER, sb);
	mergeTemplate(context, Templates.SETTER, sb);
    }

    public void appendChild(String tagName, StringBuilder sb) {

	VelocityContext context = new VelocityContext();
	context.put("tag", tagName);
	mergeTemplate(context, Templates.CHILD, sb);
    }

    public void appendAdders(IBob td, StringBuilder sb) {

	String tagName = td.getTagName();
	String package_ = td.getNamespace();
	package_ = StringUtil.namespaceToPackage(package_);

	VelocityContext context = new VelocityContext();
	context.put("package_", package_);
	context.put("tag", tagName);
	context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	mergeTemplate(context, Templates.ADDER, sb);
    }
    
    public void appendFinders(IBob td, StringBuilder sb) {

        Attributes attrs = td.getAttributes();
		String tagName = td.getTagName();
		String package_ = td.getNamespace();
		package_ = StringUtil.namespaceToPackage(package_);
		
        String currentAttr = null;
        
        VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {

            currentAttr = attrs.getLocalName(i);
            
		    context = new VelocityContext();
		    context.put("package_", package_);
		    context.put("tag", tagName);
		    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
		    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));
	
		    mergeTemplate(context, Templates.FINDER, sb);
		}
    }
    
    public void appendListers(IBob td, StringBuilder sb) {
    	
        Attributes attrs = td.getAttributes();
        String tagName = td.getTagName();
        String package_ = td.getNamespace();
        package_ = StringUtil.namespaceToPackage(package_);
        
        String currentAttr = null;
        VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {

            currentAttr = attrs.getLocalName(i);
            
		    context = new VelocityContext();
		    context.put("package_", package_);
		    context.put("tag", tagName);
		    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
		    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));
	
		    mergeTemplate(context, Templates.LISTER, sb);
        }
        
    }
    
    public void appendRemovers(IBob td, StringBuilder sb) {

        Attributes attrs = td.getAttributes();
	String tagName = td.getTagName();
	String package_ = td.getNamespace();
	package_ = StringUtil.namespaceToPackage(package_);

        String currentAttr = null;
        StringBuilder findersBuf = new StringBuilder();

	VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {

            currentAttr = attrs.getLocalName(i);

	    context = new VelocityContext();
	    context.put("package_", package_);
	    context.put("tag", tagName);
	    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));

	    mergeTemplate(context, Templates.REMOVER, sb);
	}
    }
    
    public void appendRemoveAll(IBob td, StringBuilder sb) {
	
	String tagName = td.getTagName();
	String package_ = td.getNamespace();
	package_ = StringUtil.namespaceToPackage(package_);
	
	VelocityContext context = new VelocityContext();
	context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	context.put("tag", tagName);
	context.put("package_", package_);
	
	mergeTemplate(context, Templates.REMOVEALL, sb);
    }
    
    public void appendMake(IBob td, StringBuilder sb) {
	
	String tagName = td.getTagName();
	VelocityContext context = new VelocityContext();
	context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	
	mergeTemplate(context, Templates.MAKE, sb);
    }
    
    /* close off the class' braces
     */
    public void appendClose(StringBuilder sb) {
        sb.append("\n");
        sb.append("}");
    }

    public void appendPackage(String nameSpace, StringBuilder sb) {

	String package_ = StringUtil.namespaceToPackage(nameSpace);
	VelocityContext context = new VelocityContext();
	context.put("package_", package_);

	mergeTemplate(context, Templates.PACKAGE, sb);
    }

    public static void main(String[ ] args) {

        String classVisibility = "public";
        String className = "MyClass";


        StringBuilder sb = new StringBuilder();

        //(ClassGenerator.getInstance()).appendClassName(classVisibility, "class", className, sb);

        // print the closing brace that ends the class file
        sb.append("}");

        FileWriter fw = null;

        try {
            fw = new FileWriter("MyClass.java");
            fw.write(sb.toString());

            fw.close();
        }
        catch(IOException e) {
            e.printStackTrace(System.out);
        }

    }
}
