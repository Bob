package com.interrupt.bob.generator;

import org.apache.log4j.Logger;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.Template;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.xml.sax.Attributes;
import java.io.StringWriter;
import java.io.FileWriter;
import java.io.IOException;


import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader; 


import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.util.StringUtil;
import com.interrupt.bob.util.Templates;

public class InterfaceGenerator implements IGenerator {

    
	private Logger logger = Logger.getLogger(InterfaceGenerator.class); 
	
	private static InterfaceGenerator instance = null;

    private InterfaceGenerator() {
    }
	
    public static IGenerator getInstance() {
        if(instance == null) {
            instance = new InterfaceGenerator();
        }
        return instance;
    }
	
    public void mergeTemplate(VelocityContext context, String tplName, StringBuilder sb) {
		
		Template tpl = null;
		java.io.InputStream istream = null;
		try {
			
		    
			// Option 1 
			ClassLoader cl = ClassLoader.getSystemClassLoader();
		    istream = cl.getResourceAsStream("velocity.properties"); 
			
			
			// inputstream for velocity.properties 
			//logger.debug( "InputStream ["+istream+"]"); 
			
			
			java.util.Properties props = new java.util.Properties();
			props.load(istream);
			
			
			//props.list( System.out ); 
			//System.exit(0); 
			
			ClasspathResourceLoader cpl = new ClasspathResourceLoader(); 
			//logger.debug( "Initialised Class ["+ cpl +"]"); 
			
			// System Classpath
			//logger.debug( "System Classpath ["+System.getProperty("java.class.path")+"]"); 
			//logger.debug( "Loaded Class ["+cl.loadClass( "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader" )+"]"); 
			//System.exit(0); 
			
			
		    Velocity.init(props);
		    tpl = Velocity.getTemplate(tplName);
			
		    StringWriter swriter = new StringWriter();
    		
		    if(tpl != null) {
				tpl.merge(context, swriter);
		    }
		    sb.append(swriter.toString());
		}
		catch(ResourceNotFoundException e) {
		    e.printStackTrace();
		}
		catch(ParseErrorException e) {
		    e.printStackTrace();
		}
		catch(Exception e) {
		    e.printStackTrace();
		}
		finally { 
		
			try { 
				istream.close(); 
			}
			catch( IOException e ) { 
				e.printStackTrace(); 
			}
			
		}
		
    }
    
    public void appendClassName(IBob tagdef, String visibility, String clazz,
        String clazzName, StringBuilder sb) {

	VelocityContext context = new VelocityContext();
	context.put("visibility", visibility);
	context.put("clazz", clazz);
	context.put("clazzName", clazzName);

	mergeTemplate(context, Templates.ICLASSDECL, sb);
	Template tpl = null;
    }

    public void appendAttribute(String attName, StringBuilder sb) {
	
	/*VelocityContext context = new VelocityContext();
	context.put("visibility", "private");
	context.put("attName", attName);
	mergeTemplate(context, Templates.ATTRDECL, sb);
	*/
    }

    public void appendAttributeAccessors(String attName, StringBuilder sb) {

	VelocityContext context = new VelocityContext();
	context.put("attrUpper", StringUtil.upperFirstLetter(attName));
	context.put("attr", attName);
	context.put("in", "_in");
	mergeTemplate(context, Templates.IGETTER, sb);
	mergeTemplate(context, Templates.ISETTER, sb);
    }

    public void appendChild(String tagName, StringBuilder sb) {

	/*VelocityContext context = new VelocityContext();
	context.put("tag", tagName);
	mergeTemplate(context, Templates.CHILD, sb);
	*/
    }

    public void appendAdders(IBob td, StringBuilder sb) {

	String tagName = td.getTagName();
	String package_ = td.getNamespace();
	package_ = StringUtil.namespaceToPackage(package_);

	VelocityContext context = new VelocityContext();
	context.put("package_", package_);
	context.put("tag", tagName);
	context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	mergeTemplate(context, Templates.IADDER, sb);
    }
    
    public void appendFinders(IBob td, StringBuilder sb) {

        Attributes attrs = td.getAttributes();
		String tagName = td.getTagName();
		String package_ = td.getNamespace();
		package_ = StringUtil.namespaceToPackage(package_);
		
        String currentAttr = null;
        VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {
        	
            currentAttr = attrs.getLocalName(i);
            
		    context = new VelocityContext();
		    context.put("package_", package_);
		    context.put("tag", tagName);
		    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
		    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));
	
		    mergeTemplate(context, Templates.IFINDER, sb);
		}
        
    }
    

    public void appendListers(IBob td, StringBuilder sb) {
    	
        Attributes attrs = td.getAttributes();
		String tagName = td.getTagName();
		String package_ = td.getNamespace();
		package_ = StringUtil.namespaceToPackage(package_);
		
        String currentAttr = null;
        VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {
        	
            currentAttr = attrs.getLocalName(i);
            
		    context = new VelocityContext();
		    context.put("package_", package_);
		    context.put("tag", tagName);
		    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
		    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));
		    
		    mergeTemplate(context, Templates.ILISTER, sb);
		    
		}
        
    }
    
    
    public void appendRemovers(IBob td, StringBuilder sb) {
	
        Attributes attrs = td.getAttributes();
	String tagName = td.getTagName();
	String package_ = td.getNamespace();
	package_ = StringUtil.namespaceToPackage(package_);

        String currentAttr = null;
        StringBuilder findersBuf = new StringBuilder();

	VelocityContext context = new VelocityContext();
        for(int i = 0; i  < attrs.getLength(); i++) {

            currentAttr = attrs.getLocalName(i);

	    context = new VelocityContext();
	    context.put("package_", package_);
	    context.put("tag", tagName);
	    context.put("tagUpper", StringUtil.upperFirstLetter(tagName));
	    context.put("attributeUpper", StringUtil.upperFirstLetter(currentAttr));

	    mergeTemplate(context, Templates.IREMOVER, sb);
	}
    }
    
    public void appendRemoveAll(IBob td, StringBuilder sb) {
	
	String tagName = td.getTagName();
	
	VelocityContext context = new VelocityContext();
	context.put("tagUpper", StringUtil.upperFirstLetter(tagName));

	mergeTemplate(context, Templates.IREMOVEALL, sb);
    }
    
    public void appendMake(IBob td, StringBuilder sb) {
    
    }

    /* close off the class' braces
     */
    public void appendClose(StringBuilder sb) {
        sb.append("\n");
        sb.append("}");
    }

    public void appendPackage(String nameSpace, StringBuilder sb) {

	String package_ = StringUtil.namespaceToPackage(nameSpace);
	VelocityContext context = new VelocityContext();
	context.put("package_", package_);

	mergeTemplate(context, Templates.PACKAGE, sb);
    }

    public static void main(String[ ] args) {

        String classVisibility = "public";
        String className = "MyClass";


        StringBuilder sb = new StringBuilder();

        //(InterfaceGenerator.getInstance()).appendClassName(classVisibility, "class", className, sb);

        // print the closing brace that ends the class file
        sb.append("}");

        FileWriter fw = null;

        try {
            fw = new FileWriter("MyClass.java");
            fw.write(sb.toString());

            fw.close();
        }
        catch(IOException e) {
            e.printStackTrace(System.out);
        }

    }
}
