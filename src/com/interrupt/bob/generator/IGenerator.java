package com.interrupt.bob.generator;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.Template;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.xml.sax.Attributes;
import java.io.StringWriter;
import java.io.FileWriter;
import java.io.IOException;

import com.interrupt.bob.base.IBob;

public interface IGenerator {

    public void mergeTemplate(VelocityContext context, String tplName, StringBuilder sb);
    
    public void appendClassName(IBob tagdef, String visibility, String clazz, String clazzName, StringBuilder sb);

    public void appendAttribute(String attName, StringBuilder sb);

    public void appendAttributeAccessors(String attName, StringBuilder sb);

    public void appendChild(String tagName, StringBuilder sb);

    public void appendAdders(IBob td, StringBuilder sb);
    
    public void appendFinders(IBob td, StringBuilder sb);
    
    public void appendListers(IBob td, StringBuilder sb);
    
    public void appendRemovers(IBob td, StringBuilder sb);

    public void appendRemoveAll(IBob td, StringBuilder sb);

    public void appendMake(IBob td, StringBuilder sb);

    public void appendClose(StringBuilder sb);

    public void appendPackage(String nameSpace, StringBuilder sb);

}

