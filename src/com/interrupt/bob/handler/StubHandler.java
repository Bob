package com.interrupt.bob.handler;

import org.apache.log4j.Logger;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class StubHandler extends DocumentHandler {

    
	private Logger logger = Logger.getLogger(StubHandler.class); 
	
	public StubHandler() {
	this(new ArrayList());
    }
    public StubHandler(List tl) {
    }

    public void startDocument() throws SAXException {
	//logger.debug(">> StubHandler.starDocument CALLED >> ");
        super.startDocument();
    }

    public void endDocument() throws SAXException{
	//logger.debug(">> StubHandler.endDocument CALLED >> ");
        super.endDocument();
    }

    public void startElement (String namespaceURI, String localName,
        String qName, Attributes atts) throws SAXException {

        super.startElement(namespaceURI, localName, qName, atts);
    }

    public void endElement(String namespaceURI, String localName, String qName) 
	throws SAXException {
	
        super.endElement(namespaceURI, localName, qName);
    }

}


