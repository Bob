package com.interrupt.bob.handler;


import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.base.IVisitor;

import com.interrupt.bob.core.Memory;
import com.interrupt.bob.core.MemoryField;
import com.interrupt.bob.core.Queue;
import com.interrupt.bob.handler.DocumentHandler;

import com.interrupt.bob.util.StringUtil;
import com.interrupt.callback.CallbackEvent;

/**
 *  build a bob tree hierarchy from definitions and xml files
 */
public class BobHandler extends DocumentHandler {
    
    

	private Logger logger = Logger.getLogger(BobHandler.class); 
	
	private Stack docStack = null;
    public List tagList = null;
    private Bob rootBob = null;
    
    private Stack _prefixMapping_stack = null; 
    
    
    public BobHandler() {
		
		super();
		rootBob = new Bob();
		docStack = new Stack();
		tagList = new ArrayList(); 
    }
    public BobHandler(List tl) {
	
		super();
        rootBob = new Bob();
		docStack = new Stack();
		tagList = tl;
    }
    
    public void startDocument() throws SAXException{
		
    	
    	logger.debug("!!! START !!! BobHandler.startDocument CALLED"); 
		super.startDocument();
		
		logger.debug("BobHandler.startDocument:: tagList ["+this.getMessage()+"]"); 
		if(this.getMessage() instanceof List) {
			
			tagList = (List)this.getMessage();
		} 
		
		
		HashMap prefixMapping_map = new HashMap(); 
		
		// initialise the prefix mapping stack & hashmap for first set of prefixes 
		this._prefixMapping_stack = new Stack(); 
		this._prefixMapping_stack.push(prefixMapping_map); 
		
    }
    public void endDocument() throws SAXException {
		
    	
    	logger.debug( "BobHandler.endDocument CALLED" ); 
    	logger.debug( "listener size["+ this.allListener().size() +"] / rootBob kids["+ rootBob.allChildren().size() +"]"); 
		
        //super.endDocument();
    	
		
		/* namespace PREFIX visibility 
		 */ 
		PrefixMappingVisitor pvisitor = new PrefixMappingVisitor(); 
		rootBob.accept(pvisitor); 
		
		String themessage = "rootBob["+ rootBob +"]"; 
    	logger.debug( themessage ); 
    	//System.out.println( themessage ); 
    	//if(true)
    	//	throw new RuntimeException( themessage ); 
    	
    	
    	String memoryName = this.getOut(); 
    	MemoryField memoryField = new MemoryField(); 
    	memoryField.setName(memoryName); 
    	memoryField.setValue(rootBob); 
    	
    	Memory memory = (Memory)((Queue)this.getParent()).getMemory();
		memory.addMemoryField(memoryField);
		
		
    	CallbackEvent event = new DocumentEvent();
		event.setMessage(rootBob);
		event.setName(memoryName);
		notifyListeners(event); 
		
    }
    
    public void ignorableWhitespace(char[] ch, int start, int length) {
    	//logger.debug( "ignorableWhitespace CALLED >> " + new String( ch, start, length ) );
    }
    public void characters(char[] ch, int start, int length) {
		
		String content = new String( ch, start, length );
		
		if( (content.trim()).length() > 0 ) {
		    IBob rr = (IBob)docStack.peek();
		    rr.setContent( content );
		    
		    //logger.debug( "characters CALLED >> " + new String( ch, start, length ) + " >> size ["+docStack.size() +"] >> ["+ rr.getTagName() +"]" );
		}
		
		//((IBob)docStack.peek()).setContent( new String( ch, start, length ) );
		//logger.debug( "characters CALLED >> " + new String( ch, start, length ) + " >> size ["+docStack.size() +"] >> ["+ ((IBob)docStack.peek()).getTagName() +"]" );
		
    }
    
    public void startCDATA() {
    	//logger.debug( "[[[ startCDATA CALLED" );
    }
    public void endCDATA() {
		//logger.debug( "]]] endCDATA CALLED" );
		IBob rr = (IBob)docStack.peek();
		String content = rr.getContent();
		content = "<![CDATA[" + content + "]]>";
		
		((IBob)docStack.peek()).setContent( content );
    }
    
    
    // called immediately before the 'startElement' event 
    public void startPrefixMapping (String prefix, String uri) throws SAXException{
    	
    	super.startPrefixMapping(prefix,uri); 
    	
    	((HashMap)this._prefixMapping_stack.peek()).put(prefix, uri); 
    	
    } 
    
    // called immediately after the 'endElement' event 
    public void endPrefixMapping (String prefix) throws SAXException { 
    	
    	super.endPrefixMapping(prefix); 
		
    }
    
    
    public void startElement (String namespaceURI, String localName,
        String qName, Attributes atts) throws SAXException {
		
	    super.startElement(namespaceURI, localName, qName, atts);
		//logger.debug( "BobHandler.startElement CALLED  namespace["+namespaceURI+"] / localname["+localName+"] / attribs["+ atts.toString() +"]" );
		
		
		IBob created = Bob.make(namespaceURI,localName);
		//logger.debug( "created ["+ created +"]" );
		
		
		// set the attributes
		for(int i = 0; i < atts.getLength(); i++) {
		    
			String attValue = atts.getValue(i); 
			if(attValue != null) { 
				attValue = attValue.trim(); 
			}
			//logger.debug( "Atribute Value["+attValue+"]" );
			
		    String aname = atts.getLocalName(i); 
		    Class createdClass = created.getClass();
		    Class params[] = { String.class };
		    String inputs[] = { attValue };
		    Method setMethod = null;
		    
		    try { 
		    	
		    	setMethod = createdClass.getMethod( "set"+StringUtil.upperFirstLetter(aname), params);
				setMethod.invoke( created, inputs );
		    }
		    catch(NoSuchMethodException e) {
				e.printStackTrace();
		    }
		    catch(IllegalAccessException e) {
				e.printStackTrace();
		    }
		    catch(InvocationTargetException e) {
				
		    	//logger.debug("class > "+createlogger.debug / setter > set"+StringUtil.upperFirstLetter(aname)); 
		    	e.printStackTrace();
		    }
		}
		
		

		// set the namespace prefix mappings 
		HashMap prefixes = (HashMap)_prefixMapping_stack.peek(); 
		created.setPrefixMappings(prefixes); 
		
		
		// put into the stack 
		docStack.push(created); 
		
		
		
		//** prepare for the next set of prefix mappings 
		HashMap prefixMapping_map = new HashMap(); 
		_prefixMapping_stack.push(prefixMapping_map); 
		
		
    }
    public void endElement(String namespaceURI, String localName, String qName) 
    	throws SAXException {
		
		
		//logger.debug( "BobHandler:: endElement CALLED >> ["+namespaceURI+"] > ["+localName+"]" );
		
		// find the <tag/> in the list
		if( tagList.size() < 1 ) { 
			
			String emessage = "FATAL ERROR there are no definitions";
			logger.error( emessage ); 
			throw new SAXException( emessage ); 
		}
		
		Iterator iter = tagList.iterator();
		IBob tagDef = null;
		while(iter.hasNext()) {
			
		    tagDef = (IBob)iter.next();
		    String qnm = tagDef.getQName();
		    
		    // repurposing 'qName' for my own format
		    if(namespaceURI == "" || namespaceURI == null) {
				qName = localName;
		    }
		    else {
				qName = namespaceURI +":"+ localName;
		    }
		    
		    if(qnm.equalsIgnoreCase(qName)) {
				break;
		    }
		}
		
		// set the CHILDREN
		IBob created = (IBob)docStack.pop(); 
		
		//** Do this if we are at the top of the stack 
		//logger.debug( "docStack.size()["+docStack.size()+"]" ); 
		IBob parent = null; 
		if( docStack.size() > 0 ) {

		    parent = (IBob)docStack.peek();

			rootBob.removeAllChildren();
		    rootBob.addChild( created );
			//logger.debug( "BobHandler:: endElement rootBob kids["+rootBob.allChildren().size()+"] / rootBob["+ rootBob +"]" ); 
		}
		else {
		    
			//root = created;
			rootBob.removeAllChildren();
		    rootBob.addChild( created );
			//logger.debug( "BobHandler:: endElement rootBob kids["+rootBob.allChildren().size()+"] / rootBob["+ rootBob +"]" ); 
			return; 
		}
		
		
		// get best subclass - default of 'GClazz' is 'Clazz'
		String tname = created.getTagName();
		String ns = created.getNamespace();
		tname = StringUtil.upperFirstLetter(tname);
		ns = StringUtil.namespaceToPackage(ns);
		
		// set node's parent 
		created.setParent(parent); 
		
		
		//** CLASS CREATION 
		Class cclass = null;
		String cname = null; 
		try {
	
		    //logger.debug( ">> param ["+ ns +"] / ["+ "I" + tname +"]" );
		    if(ns.length() > 0 ) {
				cname = ns + "." + "I" + tname;
		    }
		    else {
		    	cname = "I" + tname; 
		    }
		    //cclass = Class.forName( ns + "." + "I" + tname );
		    cclass = Class.forName( cname ); 
		    
		}
		catch(ClassNotFoundException e) {
			throw new SAXException("Error creating the class ["+cname+"]", e); 
		}
		
		
		Class defParams[] = { cclass };
		IBob methParams[] = { created };
		
		Class pclass = parent.getClass();
		String methName = "add" + StringUtil.upperFirstLetter(tname);
		Method setMeth = null;
		
		try { 
			
		    setMeth = pclass.getMethod( methName, defParams );
		    //logger.debug("BobHandler:: executing > parent["+parent.getTagName()+"] > (class["+pclass+"] / method["+setMeth.getName()+"]) > param["+methParams+"]"); 
		    
		    setMeth.invoke( parent, methParams );
		}
		catch(NoSuchMethodException e) {
			
			//logger.warn("Error adding["+((IBob)methParams[0]).xpath(false)+"] to parent["+parent.xpath(false)+"]. Using generic Bob.addChild()"); 
			
			try { 
				Class cclass2 = Class.forName("com.interrupt.bob.base.IBob" ); 
				Class defParams2[] = { cclass2 };
				setMeth = pclass.getMethod( "addChild", defParams2 );
				setMeth.invoke( parent, methParams );
			}
			catch(ClassNotFoundException ee) { 
				throw new SAXException(ee); 
			}
			catch(NoSuchMethodException ee) { 
				throw new SAXException(ee); 
			}
			catch(IllegalAccessException ee) {
				throw new SAXException(ee);
			}
			catch(InvocationTargetException ee) {
				throw new SAXException(ee);
			}
			
		}
		catch(IllegalAccessException e) {
			
			//throw new SAXException("IllegalAccessException / message["+e.getMessage()+"]", e); 
			throw new SAXException(e); 
			
		}
		catch(InvocationTargetException e) {
			
			/*throw new SAXException("InvocationTargetException / \nparent["+parent.xpath(false)+
				"] / \nmethName["+methName+
				"] / \ndefParams["+defParams[0]+
				"] / \nmethParams["+((IBob)methParams[0]).xpath(false)+
				"] / \nmethParam NS["+((IBob)methParams[0]).getNamespace()+
				"] / \ntargetException["+e.getTargetException()+
				"] / \nmessage["+e.getMessage()+"]", e); 
			*/
			throw new SAXException(e); 
			
		}
		
		
		//** clear for the subsequent set of prefix mappings 
		_prefixMapping_stack.pop(); 
		
    }
    
    
    
}



