package com.interrupt.bob.handler;

public class HandlerField extends GHandlerField {
	
	
	private Object _value = null;
	
	public Object getValue(Object value) { 
		return this._value; 
	}
	public void setValue(Object value) { 
		
		this._value = value;
		super.setValue(value.toString());
	}
}

