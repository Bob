package com.interrupt.bob.handler;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Logger;

import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.interrupt.bob.Balloon;
import com.interrupt.bob.generator.IGenerator;
import com.interrupt.bob.generator.ClassGenerator;
import com.interrupt.bob.generator.InterfaceGenerator;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.core.IMemory;
import com.interrupt.bob.core.Memory;
import com.interrupt.bob.core.MemoryField;
import com.interrupt.bob.core.Queue;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.GDocumentHandler;
import com.interrupt.bob.util.Util;

public class XMLHandler extends DocumentHandler {
    
	
	
	private Logger logger = Logger.getLogger(XMLHandler.class); 
	
	private Stack docStack = null;
    public List tagList = null;
    
    
    public XMLHandler() {
    	
		super();
		docStack = new Stack();
		
    }
    
    public XMLHandler(List tl) {
		
		super();
	    docStack = new Stack();
		tagList = tl;
		
    }
    
    public void startDocument() throws SAXException{
        
		logger.debug("!!! START !!! XMLHandler.startDocument CALLED");
		super.startDocument();
		if(this.getMessage() instanceof List) {
		    tagList = (List)this.getMessage();
		}
    }
    
    public void endDocument() throws SAXException{
    	
    	logger.debug("!!! END !!! XMLHandler.endDocument CALLED");
        super.endDocument();
        
        if(this.getParent() != null) { 
        	
	        String memoryName = this.getOut(); 
	    	MemoryField memoryField = new MemoryField(); 
	    	memoryField.setName(memoryName); 
	    	memoryField.setValue(null); 
	    	
	    	Memory memory = (Memory)((Queue)this.getParent()).getMemory();
			memory.addMemoryField(memoryField);
        }
        
    }
    
    public void startElement (String namespaceURI, String localName,
        String qName, Attributes atts) throws SAXException {

        super.startElement(namespaceURI, localName, qName, atts);
	
	logger.debug("XMLHandler.startElement >> " + 
		namespaceURI + " >> " + 
		localName + " >> " + 
		qName
		/*atts.getURI(0) + " >> " +
	    atts.getLocalName(0) +  " >> " +
	    atts.getQName(0) +  " >> " +
	    atts.getType(0) +  " >> " +
	    atts.getValue(0)
	    */
	    );
	
	
        Balloon bigBalloon = new Balloon(); 
		bigBalloon.setChildrenBalloon(new StringBuilder()); 
		bigBalloon.setFindersBalloon(new StringBuilder()); 
        docStack.push(bigBalloon); 
        
    }

    public void endElement(String namespaceURI, String localName, String qName) 
		throws SAXException {
		
		logger.debug("XMLHandler.endElement > tag["+localName+"]" +
		  "qname["+qName+"] "+ ">>> tagList["+tagList+"]");
		
		// find the <tag/> in the list
		Iterator iter = tagList.iterator();
		IBob tagDef = null;
		while(iter.hasNext()) {
		    
			tagDef = (IBob)iter.next();
		    String qnm = tagDef.getQName();
		    
		    // repurposing 'qName' for my own format
		    if(namespaceURI == "" || namespaceURI == null) {
		    	qName = localName;
		    }
		    else {
		    	qName = namespaceURI +":"+ localName;
		    }
		    
		    if(qnm.equalsIgnoreCase(qName)) {
		    	break;
		    }
		}
		
        Balloon bigBalloon = (Balloon)docStack.pop();
        
        
		this.makeInterface(tagDef, bigBalloon, namespaceURI);
		bigBalloon.flush();
		this.makeClass(tagDef, bigBalloon, namespaceURI);
		
    }

    public void makeClass(IBob tagDef, Balloon bigBalloon, String namespaceURI) {
    	
        String theName = genClassNameFromTag(tagDef.getQName());
        
        // set the NAMESPACE
        bigBalloon.setNamespace(tagDef.getNamespace());
        
        // add the PACKAGENAME
        StringBuilder bb = new StringBuilder();
        (ClassGenerator.getInstance()).appendPackage(tagDef.getNamespace(), bb);
        bigBalloon.setPackageBalloon(bb);
	
        // add CLASSNAME based on tag name
        StringBuilder cdb = new StringBuilder();
        (ClassGenerator.getInstance()).appendClassName(tagDef, "public", "class", theName, cdb);
        bigBalloon.setClassDeclBalloon(cdb);
        
        // add FIELDS and ACCESSORS for the attributes
        StringBuilder sb = new StringBuilder();
        StringBuilder asb = new StringBuilder();
		
		Attributes attrs = tagDef.getAttributes();
	    int attSize = attrs.getLength();
        for(int i = 0; i < attSize; i++) {
        	
	            /*(ClassGenerator.getInstance())
			.appendAttribute(attrs.getLocalName(i), sb);
	            (ClassGenerator.getInstance())
			.appendAttributeAccessors(attrs.getLocalName(i), asb);
		    */
	            
		    /*(ClassGenerator.getInstance())
			.appendAttribute(((com.interrupt.bob.Attributes)attrs).getLocalNameJ(i), sb);
	            */
		    (ClassGenerator.getInstance())
			.appendAttributeAccessors(((com.interrupt.bob.Attributes)attrs).getLocalNameJ(i), asb);
        }
        bigBalloon.setAttributes(attrs);
        bigBalloon.setAttributesBalloon(sb);
        bigBalloon.setAccessorsBalloon(asb);
		
		// add all CHILDREN & FINDERS
		//List children = tagDef.allChildren();
		List children = tagDef.allGenChildren();
	        StringBuilder cbuf = new StringBuilder();
	
		StringBuilder findBuf = new StringBuilder();
		StringBuilder listBuf = new StringBuilder();
		StringBuilder addBuf = new StringBuilder();
		StringBuilder removeBuf = new StringBuilder();
		StringBuilder removeAllBuf = new StringBuilder();
		StringBuilder makeBuf = new StringBuilder();
		for(int i = 0; i < children.size() ; i++) {
	
		    IBob childDef = (IBob)children.get(i);
	    	
            // the children
            (ClassGenerator.getInstance()).appendChild(childDef.getTagName(), cbuf);
            
            // the finders
            (ClassGenerator.getInstance()).appendFinders(childDef, findBuf);
            
            // the listers
            (ClassGenerator.getInstance()).appendListers(childDef, listBuf);
            
            // the adders
            (ClassGenerator.getInstance()).appendAdders(childDef, addBuf);
            
            // the removers
            (ClassGenerator.getInstance()).appendRemovers(childDef, removeBuf);
            
            // remove all
            (ClassGenerator.getInstance()).appendRemoveAll(childDef, removeAllBuf);
            
		}
		
		bigBalloon.appendChildrenBalloon(cbuf);
	    bigBalloon.appendFindersBalloon(findBuf);
	    bigBalloon.appendListersBalloon(listBuf);
	    
	    bigBalloon.appendAddersBalloon(addBuf);
	    bigBalloon.appendRemoversBalloon(removeBuf);
	    
		// REMOVE ALL
		bigBalloon.appendRemoveAllBalloon(removeAllBuf);
	
		// ** 'make()' for gen classes
	    (ClassGenerator.getInstance()).appendMake(tagDef, makeBuf);
		bigBalloon.appendMakeBalloon(makeBuf);
		
	    // close off the class
	    StringBuilder buf = new StringBuilder();
	    (ClassGenerator.getInstance()).appendClose(buf);
	    bigBalloon.setClassCloseBalloon(buf);
	    
		//logger.debug("makeClass: appendMakeBalloon: " + bigBalloon.getMakeBalloon());
		
        String classString = new String();
        classString = bigBalloon.makeClass();

        // make a java name
        StringBuilder javaName = new StringBuilder();
        javaName.append(theName.trim());
        javaName.append(".java");
        
        // convert URI to file name 
        String pathURI = new String();
        if(namespaceURI.contains("/")) { 
        	pathURI = namespaceURI.replace('/', File.separatorChar); 
        }
        else { 
        	pathURI = namespaceURI; 
        }
        writeToFile(pathURI, javaName.toString(), classString); 
        
    }

    public void makeInterface(IBob tagDef, Balloon bigBalloon, String namespaceURI) {
    	
        String theName = genInterfaceNameFromTag(tagDef.getQName());
        
        // set the NAMESPACE
        bigBalloon.setNamespace(tagDef.getNamespace());

        // add the PACKAGENAME
        StringBuilder bb = new StringBuilder();
        (InterfaceGenerator.getInstance()).appendPackage(tagDef.getNamespace(), bb);
        bigBalloon.setPackageBalloon(bb);

        // add CLASSNAME based on tag name
        StringBuilder cdb = new StringBuilder();
        (InterfaceGenerator.getInstance()).appendClassName(tagDef, "public", "interface", theName, cdb);
        bigBalloon.setClassDeclBalloon(cdb);

        // add FIELDS and ACCESSORS for the attributes
        StringBuilder sb = new StringBuilder();
        StringBuilder asb = new StringBuilder();
        
        Attributes attrs = tagDef.getAttributes();
        int attSize = attrs.getLength();
        for(int i = 0; i < attSize; i++) {
        	
	            /*(InterfaceGenerator.getInstance())
			.appendAttribute(attrs.getLocalName(i), sb);
	            (InterfaceGenerator.getInstance())
			.appendAttributeAccessors(attrs.getLocalName(i), asb);
		    */
        	(InterfaceGenerator.getInstance())
				.appendAttribute(((com.interrupt.bob.Attributes)attrs).getLocalNameJ(i), sb);
	        (InterfaceGenerator.getInstance())
	        	.appendAttributeAccessors(((com.interrupt.bob.Attributes)attrs).getLocalNameJ(i), asb);
        }
        bigBalloon.setAttributes(attrs);
        bigBalloon.setAttributesBalloon(sb);
        bigBalloon.setAccessorsBalloon(asb);
		
		// add all CHILDREN & FINDERS
		//List children = tagDef.allChildren();
		List children = tagDef.allGenChildren();
		StringBuilder cbuf = new StringBuilder();
		
		StringBuilder findBuf = new StringBuilder();
		StringBuilder listBuf = new StringBuilder();
		StringBuilder addBuf = new StringBuilder();
		StringBuilder removeBuf = new StringBuilder();
		StringBuilder removeAllBuf = new StringBuilder();
		for(int i = 0; i < children.size() ; i++) {
		    
		    IBob childDef = (IBob)children.get(i);
		    
            // the children
            (InterfaceGenerator.getInstance()).appendChild(childDef.getTagName(), cbuf);
            
            // the finders
            (InterfaceGenerator.getInstance()).appendFinders(childDef, findBuf);
            
            // the listers 
            (InterfaceGenerator.getInstance()).appendListers(childDef, listBuf);
            
		    // the adders
	        (InterfaceGenerator.getInstance()).appendAdders(childDef, addBuf);
		    
		    // the removers
	        (InterfaceGenerator.getInstance()).appendRemovers(childDef, removeBuf);
	        
		    // remove all
	        (InterfaceGenerator.getInstance()).appendRemoveAll(childDef, removeAllBuf);
		}
		
		bigBalloon.appendChildrenBalloon(cbuf);
        bigBalloon.appendFindersBalloon(findBuf);
        bigBalloon.appendFindersBalloon(listBuf);
        bigBalloon.appendAddersBalloon(addBuf);
        bigBalloon.appendRemoversBalloon(removeBuf);
        
		// REMOVE ALL
		bigBalloon.appendRemoveAllBalloon(removeAllBuf);
		
        // close off the class
        StringBuilder buf = new StringBuilder();
        (InterfaceGenerator.getInstance()).appendClose(buf);
         bigBalloon.setClassCloseBalloon(buf);

        String classString = new String();
        classString = bigBalloon.makeClass();

        // make a java name
        StringBuilder javaName = new StringBuilder();
        javaName.append(theName.trim());
        javaName.append(".java");
        
        // convert URI to file name 
        String pathURI = new String();
        if(namespaceURI.contains("/")) { 
        	pathURI = namespaceURI.replace('/', File.separatorChar); 
        }
        else { 
        	pathURI = namespaceURI; 
        }
        writeToFile(pathURI, javaName.toString(), classString); 
        
    }

    public String genClassNameFromTag(String tagName) {
		
		tagName = tagName.trim();
		//logger.debug("GRRRR["+tagDef.getQName() == null +"]");
		//logger.debug("GRRRR["+ tagName.length() +"]");
		if(tagName == null || (tagName.length() < 1) ) {
		    //logger.debug("HERE");
		    return "";
		}
		
        String className = tagName.trim();
	
        // if there is a namespace prefix, strip it off
        if(className.indexOf(":") > 0) {
            className = className.substring(className.indexOf(":") + 1);
        }
        String firstLetter = className.substring(0,1);
        String restOf = className.substring(1);

        StringBuilder sb = new StringBuilder("G");
        sb.append(firstLetter.toUpperCase());
        sb.append(restOf);

        return sb.toString();
    }
    public String genInterfaceNameFromTag(String tagName) {
    	
		if(tagName == null || tagName == "") {
		    return "";
		}
		
        String className = tagName.trim();

        // if there is a namespace prefix, strip it off
        if(className.indexOf(":") > 0) {
            className = className.substring(className.indexOf(":") + 1);
        }
        String firstLetter = className.substring(0,1);
        String restOf = className.substring(1);

        StringBuilder sb = new StringBuilder("I");
        sb.append(firstLetter.toUpperCase());
        sb.append(restOf);

        return sb.toString();
    }

    public void writeToFile(String parentName, String javaName, String contents) {
    	
		String genDir = (System.getProperties()).getProperty(Util.GEN);
		String baseDir = (System.getProperties()).getProperty(Util.BASE);
		genDir = genDir.trim();
		baseDir = baseDir.trim();
		
		logger.debug("BASE dir > "+ baseDir); 
		logger.debug("GEN dir > "+ genDir); 
		logger.debug("parentName > "+ parentName); 
        
		// getting the 'base' dir
        StringBuilder jfsb = new StringBuilder(baseDir);
        if(jfsb.charAt(jfsb.length() - 1) != File.separator.charAt(0)) {
        	jfsb.append(File.separator); 
		}
		
		// adding the 'gen' dir
		jfsb.append(genDir);
	    if(jfsb.charAt(jfsb.length() - 1) != File.separator.charAt(0)) {
		    jfsb.append(File.separator); 
		}
        
	    //logger.debug("PARENT dir before expansion 1 > "+ jfsb); 
        
        // make directories
        File  parent = new File(jfsb.toString());
        if(!parent.exists()) {
        	parent.mkdir();
        }
        
        //logger.debug("PARENT dir before expansion 2 > "+ parent.getAbsolutePath()); 
        
        String next = null;
        StringTokenizer st = new StringTokenizer(parentName, File.separator);
        
        try {
        	
            while(st.hasMoreTokens()) {
                next = st.nextToken();
                jfsb.append(next);
                jfsb.append(File.separator);

                parent = new File(jfsb.toString());
                //logger.debug("PARENT dir > "+ parent.getAbsolutePath() + " > exists["+parent.exists()+"]"); 
                if(!parent.exists()) {
                    parent.mkdir();
                    //logger.debug("... creating parent dir > "+ parent.exists()); 
                }
            }
            
            File javaFile = new File(jfsb.toString(), javaName);
            logger.debug("writeToFile >> WRITING OUT ["+ javaFile.getAbsolutePath() +"]");
            
            FileWriter fw = null;
            javaFile.createNewFile();
            fw = new FileWriter(javaFile);
            fw.write(contents);
            fw.close();
            
            
        }
        catch(IOException e) {
            e.printStackTrace(System.out);
        }
        
    }
    
    
}


