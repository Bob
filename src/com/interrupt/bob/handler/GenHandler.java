package com.interrupt.bob.handler;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Logger;

import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import com.interrupt.bob.Balloon;
import com.interrupt.bob.generator.IGenerator;
import com.interrupt.bob.generator.ClassGenerator;
import com.interrupt.bob.generator.InterfaceGenerator;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.util.Util;
import com.interrupt.bob.util.StringUtil;
import com.interrupt.callback.CallbackEvent;

/**
 *  build a bob tree hierarchy from definitions and xml files
 */
public class GenHandler extends DocumentHandler {
    
    
	private Logger logger = Logger.getLogger(GenHandler.class); 
	
	private Stack docStack = null;
    private List tagList = null;
    private Bob systemList = null;
    
    
    public GenHandler() {
	
	super();
	systemList = new Bob();
	docStack = new Stack();
    }
    public GenHandler(List tl) {
	
	super();
        systemList = new Bob();
	docStack = new Stack();
	tagList = tl;
    }
    
    public void startDocument() throws SAXException{
	
	super.startDocument();
	if(this.getMessage() instanceof List) {
	    tagList = (List)this.getMessage();
	}
    }
    public void endDocument() throws SAXException{
	
        super.endDocument();
	CallbackEvent event = new DocumentEvent();
	//event.setMessage(systemList);
	//event.setName(BobSystem.SYSTEM);
	notifyListeners(event);
    }
    
    public void ignorableWhitespace(char[] ch, int start, int length) {
	//logger.debug( "ignorableWhitespace CALLED >> " + new String( ch, start, length ) );
    }
    public void characters(char[] ch, int start, int length) {
	
	String content = new String( ch, start, length );
	
	if( (content.trim()).length() > 0 ) {
	    IBob rr = (IBob)docStack.peek();
	    rr.setContent( content );
	    
	    logger.debug( "characters CALLED >> " + new String( ch, start, length ) + " >> size ["+docStack.size() +"] >> ["+ rr.getTagName() +"]" );
	}
	
	//((IBob)docStack.peek()).setContent( new String( ch, start, length ) );
	//logger.debug( "characters CALLED >> " + new String( ch, start, length ) + " >> size ["+docStack.size() +"] >> ["+ ((IBob)docStack.peek()).getTagName() +"]" );
	
    }
    
    public void startCDATA() {
	logger.debug( "[[[ startCDATA CALLED" );
    }
    public void endCDATA() {
	logger.debug( "]]] endCDATA CALLED" );
	IBob rr = (IBob)docStack.peek();
	String content = rr.getContent();
	content = "<![CDATA[" + content + "]]>";
	
	((IBob)docStack.peek()).setContent( content );
    }
    
    
    public void startElement (String namespaceURI, String localName,
        String qName, Attributes atts) throws SAXException {
	
        super.startElement(namespaceURI, localName, qName, atts);
	//logger.debug( "startElement CALLED >> ["+namespaceURI+"] > ["+localName+"]" );
	
	// set the attributes
	IBob created = Bob.make(namespaceURI,localName);
	for(int i = 0; i < atts.getLength(); i++) {
	    
	    String aname = atts.getLocalName(i);
	    Class createdClass = created.getClass();
	    Class params[] = { String.class };
	    String inputs[] = { atts.getValue(i) };
	    Method setMethod = null;
	    
	    try {
		setMethod = createdClass.getMethod( "set"+StringUtil.upperFirstLetter(aname), params);
		setMethod.invoke( created, inputs );
	    }
	    catch(NoSuchMethodException e) {
		e.printStackTrace();
	    }
	    catch(IllegalAccessException e) {
		e.printStackTrace();
	    }
	    catch(InvocationTargetException e) {
		e.printStackTrace();
	    }
	}
	docStack.push(created);
    }
    
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
    	
    	//logger.debug( "endElement CALLED >> ["+namespaceURI+"] > ["+localName+"]" );
		
		// find the <tag/> in the list
		Iterator iter = tagList.iterator();
		IBob tagDef = null;
		while(iter.hasNext()) {
		    tagDef = (IBob)iter.next();
		    String qnm = tagDef.getQName();
		    
		    // repurposing 'qName' for my own format
		    if(namespaceURI == "" || namespaceURI == null) {
		    	qName = localName;
		    }
		    else {
		    	qName = namespaceURI +":"+ localName;
		    }
		    
		    if(qnm.equalsIgnoreCase(qName)) {
		    	break;
		    }
		}
		
		// set the CHILDREN
		IBob created = (IBob)docStack.pop();
		IBob parent = null;
		if( docStack.size() > 0 ) {
		    parent = (IBob)docStack.peek();
		}
		else {
		    //root = created;
		    systemList.addChild( created );
		    return;
		}
		
		// get best subclass - default of 'GClazz' is 'Clazz'
		String tname = created.getTagName();
		String ns = created.getNamespace();
		tname = StringUtil.upperFirstLetter(tname);
		ns = StringUtil.namespaceToPackage(ns);
		
		Class cclass = null;
		try {
	
		    //logger.debug( ">> param ["+ ns +"] > ["+ "I" + tname +"]" );
		    if(ns.length() > 0 ) {
		    	cclass = Class.forName( ns + "." + "I" + tname ); 
		    }
		    else {
		    	cclass = Class.forName( "I" + tname );
		    }
		}
		catch(ClassNotFoundException e) {
		    
		    e.printStackTrace();
		}
		
		//String tname = created.getTagName();
		//Class defParams[] = { IBob.class };
		Class defParams[] = { cclass };
		IBob methParams[] = { created };
		
		try { 
			Util.evalReflectedAdd(parent, tname, defParams, methParams); 
		}
		catch(NoSuchMethodException e) {
    	    e.printStackTrace(); 
    	}
    	catch(IllegalAccessException e) {
    	    e.printStackTrace(); 
    	}
    	catch(InvocationTargetException e) {
    	    e.printStackTrace(); 
    	}
    	catch(Throwable e) {
    	    e.printStackTrace(); 
    	}
		/* //parent.add{child}(created);
		Class pclass = parent.getClass();
		String methName = "add" + StringUtil.upperFirstLetter(tname);
		Method setMeth = null;
		
		try {
		    setMeth = pclass.getMethod( methName, defParams );
		    //setMeth = parentClass.getMethod( methName, defParams );
		    setMeth.invoke( parent, methParams );
		}
		catch(NoSuchMethodException e) {
		    e.printStackTrace();
		}
		catch(IllegalAccessException e) {
		    e.printStackTrace();
		}
		catch(InvocationTargetException e) {
		    e.printStackTrace();
		}
		*/
		
    }

}



