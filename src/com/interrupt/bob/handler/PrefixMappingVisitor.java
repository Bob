package com.interrupt.bob.handler;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;


public class PrefixMappingVisitor implements IVisitor { 
	

	private Logger logger = Logger.getLogger(PrefixMappingVisitor.class); 
	
	public void visit(IBob bob) { 
		
		
		//logger.debug( "PrefixMappingVisitor VISITING["+bob.getTagName()+"]" );
		
		
		// A. find namespace in 'xmlns' attribute 
		boolean xmlns_exists = false; 
		HashMap prefMap = bob.getPrefixMappings(); 
		//logger.debug( "PrefixMappingVisitor Attributes["+atts+"]" );
		
		
		if(prefMap != null) { 
			
			
    		// set the attributes
    		String aname = null; 
    		String nextKey = null; 
		    Iterator kiter = prefMap.keySet().iterator(); 
		    while(kiter.hasNext()) { 
		    	
		    	
		    	nextKey = (String)kiter.next(); 
    		    aname = (String)prefMap.get(nextKey); 
    		    //logger.debug( "	Attribute Name["+aname+"] / Key["+nextKey+"]" );
        		
    		    // only empty prefix names are an 'xmlns' declaration 
    		    if((nextKey == null) || (nextKey.trim().length() < 1)) { 
    		    	
    		    	xmlns_exists = true; 
    		    	break; 
    		    } 
    		    
		    }
    		
    		//** MAKE DECISION. there is an 'xmlns' attribute 
    		if(xmlns_exists) { 
    			bob.setNSVisible(true); 
    			return; 
    		}
    		
		}
		
		
		// B. search ancestors for namespace prefix mapping 
		String NS_comparator = bob.getNamespace(); 
		IBob currentBob = bob; 
		boolean prefix_exists = false;
		while(currentBob != null) { 
			
    		HashMap bobPrefixes = currentBob.getPrefixMappings(); 
    		if(bobPrefixes == null) { 
    			break; 
    		}
    		//logger.debug( "PrefixMappingVisitor bobPrefixes["+bobPrefixes+"] / bob NS["+NS_comparator+"]" ); 
    		
    		Iterator keyIterator = bobPrefixes.keySet().iterator(); 
    		String nextName = null; 
    		String nextValue = null; 
    		while(keyIterator.hasNext()) { 
    			
    			nextName = (String)keyIterator.next(); 
    			nextValue = (String)bobPrefixes.get(nextName); 
    			if(nextValue.equals(NS_comparator)) { 
    				prefix_exists = true; 
    				bob.setNSPrefix(nextName); 
    				break; 
    			} 
    			
    		} 
    		
    		currentBob = currentBob.getParent(); 
    		
		}
		
		
		//** MAKE DECISION. if Bob or ancestor has a matching namespace prefix or...
		if(prefix_exists) { 
			bob.setNSVisible(true); 
			return; 
		}
		
		bob.setNSVisible(false); 
		
	}
	
}


