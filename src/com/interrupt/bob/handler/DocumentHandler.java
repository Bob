
package com.interrupt.bob.handler;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ext.DeclHandler;

import com.interrupt.bob.base.Bob;
import com.interrupt.callback.CallbackEvent;
import com.interrupt.callback.CallbackBroadcaster;


/* Abstract handler class for xml documents. Has common methods for adding,
 * removing and notifying listeners
 */
public abstract class DocumentHandler extends GDocumentHandler 
    implements ContentHandler, LexicalHandler, DeclHandler, CallbackBroadcaster {
	
	
	
	private Logger logger = Logger.getLogger(DocumentHandler.class); 
	
	private List listeners = null;
    protected Object _message = null;
    
    public DocumentHandler() {
    	
    	listeners = new ArrayList();
    }
    
    
    /* set/get a message
     */
    public void setMessage(Object msg) {
    	_message = msg;
    }
    public Object getMessage() {
    	return _message;
    }
    
    
    /* CallbackBroadcaster methods
     */
    public void addListener(com.interrupt.bob.DocumentHandler handler) {
    	listeners.add(handler);
    }

    public List allListener() { 
    	return new ArrayList(listeners); 
    }
    
    public void notifyListeners(CallbackEvent event) {
		
		Iterator iter = listeners.iterator();
		com.interrupt.bob.DocumentHandler next = null;
		while(iter.hasNext()) {
		    next = (com.interrupt.bob.DocumentHandler)iter.next();
		    next.endHandle(event);
		}
		
    }
    
    public Object removeListener(com.interrupt.bob.DocumentHandler handler) {
    	
		int index = listeners.indexOf(handler);
		return listeners.remove(index);
    }


    /* ContentHandler methods
     */
    public void setDocumentLocator (Locator locator){
    }

    public void startDocument() throws SAXException{
    	//logger.debug("!!! START !!! startDocument CALLED");
    }

    public void endDocument() throws SAXException{
		
		logger.debug("!!! END !!! endDocument CALLED");
		
		CallbackEvent event = new DocumentEvent();
		notifyListeners(event);
    }
    
    // called immediately before the 'startElement' event 
    public void startPrefixMapping (String prefix, String uri) throws SAXException{
    	//logger.debug("startPrefixMapping CALLED / ["+prefix+"/"+uri+"]");
    }
    
    // called immediately after the 'endElement' event 
    public void endPrefixMapping (String prefix) throws SAXException{
    	//logger.debug("endPrefixMapping CALLED / ["+prefix+"]");
    }
    
    public void startElement (String namespaceURI, String localName,
    String qName, Attributes atts) throws SAXException{
    }
    
    public void endElement (String namespaceURI, String localName, 
	    String qName) throws SAXException{
    }

    public void characters (char ch[], int start, int length) throws SAXException{
    }

    public void ignorableWhitespace (char ch[], int start, int length) throws SAXException{
    }

    public void processingInstruction (String target, String data) throws SAXException{
    }

    public void skippedEntity (String name) throws SAXException{
    }

    /*  Lexical handler methods
     */
     public void startDTD (String name, String publicId,
                           String systemId) throws SAXException{
     }

     public void endDTD () throws SAXException{
     }

     public void startEntity (String name) throws SAXException{
     }

     public void endEntity (String name) throws SAXException{
     }

     public void startCDATA() throws SAXException{
     }

     public void endCDATA () throws SAXException{
     }

     public void comment (char ch[], int start, int length) throws SAXException{
     }


    /* DeclHandler methods
     */
    public void elementDecl(String name, String model)
	throws SAXException {
    }

    public void attributeDecl(String eName, String aName, String type, 
	String valueDefault, String value) throws SAXException {
    }

    public void internalEntityDecl (String name, String value)
	throws SAXException {
    }

    public void externalEntityDecl (String name, String publicId,
	    String systemId) throws SAXException {
    }
}
