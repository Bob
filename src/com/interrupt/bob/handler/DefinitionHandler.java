
package com.interrupt.bob.handler;

import java.util.Stack;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
/*import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ext.DeclHandler;
*/
//import com.interrupt.bob.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.handler.DefinitionEvent;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bob.base.PrintVisitor;
import com.interrupt.bob.core.IMemory;
import com.interrupt.bob.core.Memory;
import com.interrupt.bob.core.MemoryField;
import com.interrupt.bob.core.Queue;
import com.interrupt.callback.CallbackBroadcaster;
import com.interrupt.callback.CallbackEvent;


public class DefinitionHandler extends DocumentHandler implements CallbackBroadcaster {
    
    
	
	private Logger logger = Logger.getLogger(DefinitionHandler.class); 
	
	private Stack docStack = null;
    private TagMerger tagMerger = null;
    private IBob currentDoc = null;
    
    public DefinitionHandler() {
		
		super();
		docStack = new Stack();
		tagMerger = new TagMerger();
    }
    
    
    // XMLHandler methods
    public void startDocument() throws SAXException {
    	logger.debug("!!! START !!! DefinitionHandler.startDocument CALLED");
    }
    
    public void endDocument() throws SAXException {
		
		//logger.debug("!!! END !!! DefinitionHandler.endDocument CALLED / Listener size["+this.allListener().size()+"]"); 
		//logger.debug("DefinitionHandler.endDocument:: definitions ["+ tagMerger.getTagList() +"]"); 
		
    	if(this.getParent() != null) { 
    		
	    	String memoryName = this.getOut(); 
	    	MemoryField memoryField = new MemoryField(); 
	    	memoryField.setName(memoryName); 
	    	memoryField.setValue(tagMerger.getTagList()); 
	    	
	    	// put this memory field into the parent Queue
	    	Memory memory = (Memory)((Queue)this.getParent()).getMemory();
			memory.addMemoryField(memoryField);
    	}
		
    	CallbackEvent event = new DocumentEvent();
		event.setMessage(tagMerger.getTagList());
		event.setName(BobSystem.DEFINITIONS);
		notifyListeners(event);
    }
    
    public void startElement (String namespaceURI, String localName, 
		String qName, Attributes atts) throws SAXException {
		
		//logger.debug( "DefnitionHandler:: startElement >> name["+localName+"] qname["+
		//  qName+"] namespace["+namespaceURI+"] attributes["+ atts +"]");
		
		//AttributesImpl attribs = new AttributesImpl();
		//attribs.setAttributes(atts);
		com.interrupt.bob.Attributes attribs = new com.interrupt.bob.Attributes();
		attribs.setAttributes(atts);
		
		//logger.debug("startElement >> "+ attribs);
		
		IBob tagDef = new Bob();
		tagDef.setNamespace(namespaceURI);
		tagDef.setTagName(localName);
		//tagDef.setQName(qName);
		tagDef.setAttributes(attribs);
			    
		docStack.push(tagDef);
    }
    
    public void endElement (String namespaceURI, String localName, String qName) 
		throws SAXException {
    	
		//logger.debug( "DefinitionHandler:: endElement >> name["+localName+"] qname["+
		//		qName+"] namespace["+namespaceURI+"]");
		
		IBob tagDef = (IBob)docStack.pop();
		tagDef = tagMerger.addTag(tagDef);
		//logger.debug( "DefinitionHandler.endElement:: added def["+tagDef.getTagName()+"] / tagList["+tagMerger.getTagList()+"]" ); 
		
		if(!docStack.empty()) {
		    //((IBob)docStack.peek()).addChild(tagDef);
		    ((IBob)docStack.peek()).addGenChild(tagDef);
		}
		
		//this.currentDoc = tagDef;
    }

}



