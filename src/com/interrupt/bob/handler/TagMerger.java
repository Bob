
package com.interrupt.bob.handler;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;

//import com.interrupt.bob.Attributes;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.Bob;

public class TagMerger {

    private ArrayList tagList = null;
    
    public TagMerger() {
	tagList = new ArrayList();
    }

    public ArrayList getTagList() {
	return tagList;
    }

    public IBob addTag(String tagName, String namespace) {
	IBob td = new Bob(namespace, tagName);
	return addTag(td);
    }

    public IBob addTag(IBob tag) {
	
	ListIterator listIterator = tagList.listIterator();
	IBob next = null;
	
	boolean merged = false;
	
	while(listIterator.hasNext()) {
	    next = (IBob)listIterator.next();
	    String qnm = next.getQName();
	    
	    if(qnm.equalsIgnoreCase(tag.getQName())) {
		
		mergeTag(next, tag);
		listIterator.set(next);
		tag = next;
		
		merged = true;
		break;
	    }
	}
	
	// if that tag did not exist before
	if(!merged) {
	    tagList.add(tag);
	}
	
	return tag;
    }

    /**
     * general contract for this method is that all
     * merged content chould end up in 'next'
     */
    public void mergeTag(IBob next, IBob tag) {
	 
	mergeAttributes(next, tag);
	mergeChildren(next, tag);	
    }
    public void mergeAttributes(IBob next, IBob tag) {

	AttributesImpl attribs = (AttributesImpl)tag.getAttributes();
	
	AttributesImpl nextAttributes = (AttributesImpl)next.getAttributes();
	String origAttr = null;
	String thisAttr = null;
	
	// make list of attribute names
	ArrayList nomatches = new ArrayList();
	for(int i = 0; i < attribs.getLength(); i++) {
	    nomatches.add(attribs.getQName(i));
	}

	for(int i = 0; i < nextAttributes.getLength(); i++) {
	    origAttr = nextAttributes.getQName(i);
	    for(int j = 0; j < attribs.getLength(); j++) {
		thisAttr = attribs.getQName(j);

		// remove attribute names that already exist
		if(origAttr.equalsIgnoreCase(thisAttr)) {
		    nomatches.remove(thisAttr);
		    break;
		}
	    }
	}

	// add the remaining attributes. nomatches. defs. 
	for(int i = 0; i < nomatches.size(); i++) {
	    
	    String qnm = (String)nomatches.get(i);

	    int idx = attribs.getIndex(qnm);
	    nextAttributes.addAttribute(attribs.getURI(idx), attribs.getLocalName(idx), 
		attribs.getQName(idx), attribs.getType(idx), attribs.getValue(idx));
	}
	next.setAttributes(nextAttributes);
    }
    public void mergeChildren(IBob next, IBob tag) {

	//List nextChildr = next.allChildren();
	//List tagChildr = tag.allChildren();	
	List nextChildr = next.allGenChildren();
	List tagChildr = tag.allGenChildren();	

	ListIterator nextIter = nextChildr.listIterator();
	ListIterator tagIter = tagChildr.listIterator();
	

	while(nextIter.hasNext()) {
	    IBob nextnext = (IBob)nextIter.next();
	    String qnNext = nextnext.getQName();

	    //tag.addChild(nextnext);
	    tag.addGenChild(nextnext);
	}
	while(tagIter.hasNext()) {
	    IBob nextend = (IBob)tagIter.next();
	    String qnEnd = nextend.getQName();

	    //next.addChild(nextend);
	    next.addGenChild(nextend);
	}
    }
}


