
package com.interrupt.bob;

import java.util.Iterator;
import java.util.Properties;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.GHandlerField;
import com.interrupt.bob.handler.HandlerField;
import com.interrupt.bob.handler.IHandlerField;
import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.util.Util;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.BobSystem;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.greenfabric.find.Find;


public class Main {
	
	
	private Logger logger = Logger.getLogger(Main.class); 
	
	public static void main(String args[]) {
	
	Options options = new Options();

	Option genOption = new Option("gen", true, "directory for generated java files");
	genOption.setArgs(1);
	genOption.setArgName("gen directory");

	Option baseOption = new Option("base", true, "base directory from which bob runs");
	baseOption.setArgs(1);
	baseOption.setArgName("base directory");

	Option endOption = new Option("end", true, "process files with given suffix");
	endOption.setArgs(1);
	endOption.setArgName("file suffix");

	Option defOption = new Option("def", true, "xml definitions that 'Bob' will use");
	defOption.setArgs(1);
	defOption.setArgName("xml definitions");

	Option sysOption = new Option("sys", true, "xml files that 'Bob' will process");
	sysOption.setArgs(1);
	sysOption.setArgName("xml system files");

	options.addOption(genOption);
	options.addOption(baseOption);
	options.addOption(endOption);
	options.addOption(defOption);
	options.addOption(sysOption);
	
	
	BasicParser parser = new BasicParser(); 
	
	// Parse command-line options
	CommandLine cl = null;
	try {
	    cl = parser.parse(options, args);
	}
	catch(ParseException e) {
	    e.printStackTrace(System.out);
	}

	// Set System properties
	Properties properties = System.getProperties();
	try {
		
	    properties.setProperty(Util.HOME, System.getProperty("user.dir"));
	    properties.setProperty(Util.BASE, cl.getOptionValue("base"));
	    properties.setProperty(Util.GEN, cl.getOptionValue("gen"));
	    properties.setProperty(Util.END, cl.getOptionValue("end"));
		
		
		String def_s = cl.getOptionValue("def");
		if( def_s == null || def_s.trim().length() < 1 ) { 
			//logger.debug("ERROR. -def <definition.files> not specified. Stopping."); 
			return;
		}
		
		String sys_s = cl.getOptionValue("sys");
		if( sys_s == null || sys_s.trim().length() < 1 ) { 
			//logger.debug("ERROR. -sys <system.files> not specified. Stopping."); 
			return;
		}
		
	    properties.setProperty(Util.DEF, def_s );
	    properties.setProperty(Util.SYS, sys_s );
		
	}
	catch(NullPointerException e) {
	    e.printStackTrace(System.out);
	}

	// process <xml/> and directory/ recursively
	FindFiles ffiles = new FindFiles();
	ffiles.setBase(properties.getProperty(Util.BASE));
	ffiles.setSuffix(properties.getProperty(Util.END));
	
	List fileList = ffiles.find(properties.getProperty(Util.DEF).split("\\s") );

	//logger.debug("MAIN > cl >> " + java.util.Arrays.asList(properties.getProperty(Util.DEF)).toString());
	//logger.debug("MAIN > files >> " + fileList.toString());
	
	
	StringBuilder fileString_b  = new StringBuilder();
	Iterator fileIter = fileList.iterator(); 
	while( fileIter.hasNext() ) { 
		
		fileString_b.append( " " ); 
		fileString_b.append( (String)fileIter.next() ); 
		
	}
	String fileString_s = fileString_b.toString(); 
	
	try {
		
		DefinitionHandler dhandler = new DefinitionHandler(); 
		dhandler.setFiles( fileString_s ); 
		dhandler.setOut( Util.DEF ); 
		
			IHandlerField hfield = new HandlerField(); 
			hfield.setName("tagList"); 
			hfield.setValue("${"+ Util.DEF +"}"); 
		
		XMLHandler xhandler = new XMLHandler(); 
		xhandler.setFiles( fileString_s ); 
		xhandler.addHandlerField(hfield); 
		
	    DocumentProcessor processor = new DocumentProcessor();
	    processor.addHandler(dhandler);
	    processor.addHandler(xhandler);
	    processor.process();
		
	}
	catch(ProcessorException e) {
	    e.printStackTrace(System.out);
	}

	// initialise 'bob' system
	//(BobSystem.getInstance()).initialise();
	
    }
	
}


