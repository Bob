package com.interrupt.bob.bootstrap;

import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Iterator;
import java.io.File;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import java.io.IOException;

import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.util.Util;
import com.interrupt.bob.FindFiles;
import com.interrupt.callback.CallbackEvent;
import com.interrupt.bob.handler.DefinitionEvent;
import com.interrupt.bob.bootstrap.Queue;

public class BootstrapProcessor {
    
    
    private XMLReader reader = null;
    private DocumentHandler handler = null;
    private List _handlers = null;
    private List _callbacks = null;
    private Iterator _handlerIter = null;
    private Queue queue = new Queue();

    public BootstrapProcessor() {
	
	// set the queue
	queue = new Queue();
    }
    
    
    /* handlers & callbacks
     */
    public void addHandler(DocumentHandler handler) {
	this.queue.addHandler(handler);
    }
    public List allHandler() {
	return new ArrayList(this.queue.allHandler());
    }
    public List allCallback() {
	return this.queue.allCallback();
    }
    
    /* return queue
     */
    public Queue getQueue() {
	return queue;
    }
    
    
    /* process list of files
     */
    public void process(List pathList) throws ProcessorException {
	queue.execute(pathList);
    }
    
}


