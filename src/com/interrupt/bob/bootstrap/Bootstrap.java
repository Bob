
package com.interrupt.bob.bootstrap;


import java.util.Iterator;
import java.util.Properties;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

import com.interrupt.bob.FindFiles;
import com.interrupt.bob.bootstrap.BootstrapProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bob.handler.DefinitionHandler;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.XMLHandler;
import com.interrupt.bob.util.Util;

import com.interrupt.bob.base.BobSystem;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.greenfabric.find.Find;


public class Bootstrap {
	
	private static Logger logger = Logger.getLogger(Bootstrap.class); 
	
    public static void main(String args[]) {

	Options options = new Options();

	Option genOption = new Option("gen", false, "directory for generated java files");
	genOption.setArgs(1);
	genOption.setArgName("gen directory");

	Option baseOption = new Option("base", false, "base directory from which bob runs");
	baseOption.setArgs(1);
	baseOption.setArgName("base directory");

	Option endOption = new Option("end", false, "process files with given suffix");
	endOption.setArgs(1);
	endOption.setArgName("file suffix");

	options.addOption(genOption);
	options.addOption(baseOption);
	options.addOption(endOption);


	BasicParser parser = new BasicParser();

	// Parse command-line options
	CommandLine cl = null;
	try {
	    cl = parser.parse(options, args);
	}
	catch(ParseException e) {
	    e.printStackTrace(System.out);
	}

	// Set System properties
	Properties properties = System.getProperties();
	try {
	    properties.setProperty(Util.HOME, System.getProperty("user.dir"));
	    properties.setProperty(Util.BASE, cl.getOptionValue("base"));
	    properties.setProperty(Util.GEN, cl.getOptionValue("gen"));
	    properties.setProperty(Util.END, cl.getOptionValue("end"));
	}
	catch(NullPointerException e) {
	    e.printStackTrace(System.out);
	}

	// process <xml/> and directory/ recursively
	FindFiles ffiles = new FindFiles();
	ffiles.setBase(properties.getProperty(Util.BASE));
	ffiles.setSuffix(properties.getProperty(Util.END));
	List fileList = ffiles.find(cl.getArgs());

	logger.debug("BOOTSTRAP > cl >> " + java.util.Arrays.asList(cl.getArgs()).toString());
	logger.debug("BOOTSTRAP > files >> " + fileList.toString());

	try {
	    BootstrapProcessor processor = new BootstrapProcessor();
	    processor.addHandler(new DefinitionHandler());
	    processor.addHandler(new XMLHandler());
	    processor.process(fileList);
	}
	catch(ProcessorException e) {
	    e.printStackTrace(System.out);
	}
    }
}


