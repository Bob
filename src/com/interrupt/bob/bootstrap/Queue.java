package com.interrupt.bob.bootstrap;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.interrupt.bob.FindFiles;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bob.handler.DocumentHandler;
import com.interrupt.bob.handler.QueueEvent;
import com.interrupt.bob.processor.DocumentProcessor;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.callback.CallbackEvent;

import org.apache.log4j.Logger;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.SAXException;

public class Queue extends DocumentHandler implements com.interrupt.bob.DocumentHandler {
    
    
    private Map		_memory = null;
    private List	_callbacks = null;
    private List	_handlerList = null;
    private List	_fileList = null;
    private Iterator	_handlersIter = null;
    private Iterator	_filelistIter = null;
    
    private XMLReader	_reader = null;
    private String	_nextFile = null;
    private Object	_lastMessage = null;
    
    private Logger logger = Logger.getLogger(Queue.class); 
	
    /**
     *
     * Workflow: nextHandler(); (nextHandler/endHandle) 
     *	    --  nextFile(); until all files are processed;
     * then call nextHandler again
     */
    public Queue() {
	
    	_memory = new HashMap();
    	_handlerList = new ArrayList();
    	_callbacks = new ArrayList();
		
		// set reader
		System.setProperty("org.xml.sax.driver", "com.bluecast.xml.Piccolo"); 
		try {
		    this._reader = XMLReaderFactory.createXMLReader();
		}
		catch(SAXException e) {
		    e.printStackTrace();
		}
    }
    
    
    /* set 'reader' and 'handler' list
     */
    public void setReader(XMLReader xreader) {
		_reader = xreader;
    }
    public XMLReader getReader() {
		return _reader;
    }
    public void addHandler(DocumentHandler handler) {
		handler.addListener(this);
		this._handlerList.add(handler);
    }
    public List allHandler() {
		return new ArrayList(this._handlerList);
    }
    
    
    public Object getLastMessage() {
		return _lastMessage;
    }
    
    
    /* 'com.interrupt.bob.DocumentHandler' methods
     */
    public void endHandle(CallbackEvent event) {
	
    	logger.debug("... Bootstrap Queue.endHandle()");
	
    	// SETUP 'BobSystem'
    	_callbacks.add(event);
    	_lastMessage = event.getMessage();
    	String mname = event.getName();
	
    	//(BobSystem.getInstance()).setMessage(mname,_lastMessage);
    	this._memory.put(mname,this._lastMessage);
    		
    	try {
			this.nextFile();
    	}
    	catch(ProcessorException e) {
			e.printStackTrace();
    	}
    }
    public List allCallback() { return new ArrayList(this._callbacks); }
    
    
    public Map getMessageHistory() {
    		return new HashMap(this._memory);
    }
    public void setMessageHistory(Map mhistory) {
    		this._memory = mhistory;
    }
    
    
    public void kickoff() throws ProcessorException {
		
		_handlersIter = _handlerList.iterator();
		this.nextHandler();
    }

    
    public void nextHandler() throws ProcessorException {
		
		if(_handlersIter.hasNext()) {
		
			_filelistIter = _fileList.iterator();
		    
		    // next handler
		    DocumentHandler handler = (DocumentHandler)_handlersIter.next();
		    handler.setMessage(_lastMessage);
			
	        logger.debug("");
		    logger.debug( "... Bootstrap Handler["+handler.getClass().getName()+"]" );
		    
		    try {
				_reader.setProperty("http://xml.org/sax/properties/lexical-handler", handler); 
				_reader.setProperty("http://xml.org/sax/properties/declaration-handler", handler); 
				_reader.setContentHandler(handler);
		    }
		    catch(SAXException e) {
				e.printStackTrace();
		    }
		    
		    this.nextFile();
		    
		}
		else {
		    
		    logger.debug(">>> Bootstrap endExecute");
		    QueueEvent qevent = new QueueEvent();
		    this.notifyListeners(qevent);
		}
		
    }
    
    public void nextFile() throws ProcessorException {
		
		// set the next file
		if(_filelistIter.hasNext()) {
		    _nextFile = (String)_filelistIter.next();
		    logger.debug( "... Bootstrap PROCESSING ["+ _nextFile +"]" );
	    
		    File fileToParse = new File(_nextFile);
	    
		    try {
				_reader.parse(fileToParse.getAbsolutePath());
		    }
		    catch(SAXException e) {
				throw new ProcessorException( e.getMessage(), e);
		    }
		    catch(IOException e) {
				throw new ProcessorException( e.getMessage(), e);
		    }
		}
		else {
		    this.nextHandler();
		}
		
    }
    
    
    /* ENTRY POINT
     */
    public void execute(List pl) throws ProcessorException {
		
		logger.debug( "... Bootstrap Queue.execute");
		
		// ensure file list is good
		try{
		    FindFiles.listCheck(pl);
		}
		catch(RuntimeException e) {
		    throw new ProcessorException(e.getMessage(), e);
		}
		
		// set the file list
		_fileList = pl;
		
		this.kickoff();
    }
    
}


