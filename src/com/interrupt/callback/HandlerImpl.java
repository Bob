package com.interrupt.callback;

import com.interrupt.callback.CallbackEvent;
import com.interrupt.bob.DocumentHandler;

public class HandlerImpl implements DocumentHandler {
    
    /* stub for testing purposes
     */
    public int getThing() { return 0; }
    
    /*private CallbackEvent event = null;

    public CallbackEvent getEvent() {
	return event;
    }
    
    public void onEvent( CallbackEvent message ) {
	
	event = message;
    }
    */

    public void endHandle(CallbackEvent ce) {}
}


