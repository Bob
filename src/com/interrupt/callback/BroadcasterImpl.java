package com.interrupt.callback;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import com.interrupt.bob.DocumentHandler;

public class BroadcasterImpl implements CallbackBroadcaster {
    
    private List listeners = null;
    
    public BroadcasterImpl() {
	listeners = new ArrayList();
    }
    
    /* CallbackBroadcaster methods
     */
    public void addListener(com.interrupt.bob.DocumentHandler handler) {
	listeners.add(handler);
    }

    public List allListener() { 
	return new ArrayList(listeners); 
    }

    public void notifyListeners(CallbackEvent event) {
	
	Iterator iter = listeners.iterator();
	com.interrupt.bob.DocumentHandler next = null;
	while(iter.hasNext()) {
	    next = (com.interrupt.bob.DocumentHandler)iter.next();
	    next.endHandle(event);
	}
	
    }

    public Object removeListener(com.interrupt.bob.DocumentHandler handler) {

	int index = listeners.indexOf(handler);
	return listeners.remove(index);
    }
}


