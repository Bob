
package com.interrupt.callback;


import java.util.List;
import com.interrupt.bob.DocumentHandler;

public interface CallbackBroadcaster {
    
    public void notifyListeners(CallbackEvent event);
    public void addListener(DocumentHandler handler);
    public Object removeListener(DocumentHandler handler);
    
    public List allListener();
}

