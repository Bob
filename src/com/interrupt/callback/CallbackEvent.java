
package com.interrupt.callback;

public abstract class CallbackEvent {
    
    private String _name = null;
    private Object _message = null;
    
    
    public CallbackEvent() {
	_name = "";
	_message = "";
    }
    
    
    public void setMessage(Object msg) {
	_message = msg;
    }
    public Object getMessage() {
	return _message;
    }

    public void setName(String name) {
	_name = name;
    }
    public String getName() {
	return _name;
    }
    
}

