
package com.interrupt.callback;

public interface CallbackHandler {

    /* a stub to compare interface instances
     */
    public int getThing();

    public CallbackEvent getEvent();
    
}


